package xiaoa.test.hbase;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CountDownLatch;

import org.apache.commons.io.FileUtils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.google.common.base.Charsets;

import xiaoa.java.utils.net.HttpUtils;
import xiaoa.java.utils.time.DateFormatUtils;
import xiaoa.java.utils.words.ExcelDome.L;

/**
 * 
 * @author xiaoa
 * @date 2017年3月27日 上午11:45:46
 * @version V1.0
 *
 */
public class HttpTest {
	
	/**
	 * 测试
	 * @Title: test
	 * @throws Throwable
	 * @author xiaoa
	 */
	public static boolean testRequest() throws Throwable{
		
		StringBuffer quey = new StringBuffer("?message=");
		
		for (int i = 0 ; i < 300 ; i ++){
			quey.append("y");
		}
	   
	    String body =  HttpUtils.get("http://localhost:8080/ActiveMQProducer/sendMessage/sendTest" + quey.toString());
	    
	    if (body == null || body.trim().equals("")){
	    	return false;
	    }

	    JSONObject  json = JSON.parseObject(body);
	    
	    return   json.getBoolean("resutSuccess");
	}
	
	static String message = null;
	
	static {
		
		
		try {
			File  file = new File("e://emailMessage.html");
			
			message = FileUtils.readFileToString(file, Charsets.UTF_8);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	
	static int transactionId = 0;
	
	
	
	public static synchronized int getTransactionId() {
		return transactionId ++ ;
	}

	/**
	 * 发送email 测试
	 * @Title: testEmailRequest
	 * @return
	 * @throws Throwable
	 * @author xiaoa
	 */
	public static boolean testEmailRequest() throws Throwable{
		

		Map<String,String> map = new HashMap<String , String>();
		
		int tid = getTransactionId();
		
		
		map.put("transactionId",tid +"");
		map.put("platform", "1");
		map.put("channel", "2");
		map.put("userTag", "user");
		map.put("version", "1");
		
		map.put("personal", "标题");
		map.put("subject", "测试主题");		
		map.put("text", message);
		map.put("html", "true");
		map.put("to",   tid + "@miduchi111na.com");
		
		String body = HttpUtils.doPost("http://localhost:8080/ActiveMQProducer/sendMessage/sendEmailMessage" , map);
		
		if (body == null || body.trim().equals("")){
			return false;
		}

	    JSONObject  json = JSON.parseObject(body);
	    
	    return   json.getBoolean("resutSuccess");
	}
	
	// 测试时间
	static final int seconds = 6000;
	
	static final long beginTime = System.currentTimeMillis();
	
	public static synchronized boolean checkEnd(){
		
		if (successNums > 1500){
			return false;
		}
		
		return System.currentTimeMillis() - beginTime  <= (seconds * 1000);
		
	} 
	
	
	static int errorNums = 0;
	
	static int successNums = 0;
	
	public static synchronized void addErrorNums(){
		errorNums ++ ;
	}

	public static synchronized void addSuccessNums(){
		successNums ++ ;
	}
	
	
	/**
	 * 运行
	 * @Title: testReqNum
	 * @param threads
	 * @throws Throwable
	 * @author xiaoa
	 */
	public static void testReqNum(int threads )throws Throwable{
		
		
		// 创建一个线程计数器
		final CountDownLatch   countDownLatch  =  new CountDownLatch(threads);
		
		// 线程
		Runnable  run  = new Runnable() {
			
			@Override
			public void run() {

				// 获取线程
				countDownLatch.getCount();
				
				while(checkEnd()){
					
					boolean success = false;
					
					try {
						success = testEmailRequest();
						L.info("=================== success = " + success );
					} catch (Throwable e) {
						e.printStackTrace();
					}
					
					if (success){
						
						// 增加成功数量
						addSuccessNums();
					}else{
						
						// 增加失败数量
						addErrorNums();
						
					}
					
				}
				
				// 线程完成
				countDownLatch.countDown();
				
			}
		};
		
		// 
		for (int i = 0; i < threads ; i ++){
			Thread thread = new Thread(run);
			thread.setName("thead[" + i + "]");
			thread.start();
		}
		
		countDownLatch.await();
		
	}
	
	
	
	
	public static void main(String[] args)throws Throwable {

		// 开始运行
		testReqNum(10);
		
		L.info("========================  seconds = " + seconds  + "  beginTime = " + DateFormatUtils.format_yyyyMMddhhssSSS(new Date(beginTime)) +" endTime = " + DateFormatUtils.format_yyyyMMddhhssSSS(new Date()));
		L.info("========================  message.length = " + message.length() + " + errorNums = " + errorNums + "   successNums = " + successNums);
	}
	

}
