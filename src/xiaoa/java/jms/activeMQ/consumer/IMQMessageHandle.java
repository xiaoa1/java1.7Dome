package xiaoa.java.jms.activeMQ.consumer;

import javax.jms.Message;

/**
 * 消费者头
 * @author xiaoa
 * @date 2017年3月19日 下午6:15:42
 * @version V1.0
 *
 */
public interface IMQMessageHandle {
	
	/**
	 * 当有消息的时候回调方法
	 * @Title: onMessage
	 * @param message
	 * @throws Throwable
	 * @author xiaoa
	 */
	public boolean onMessage(Message  message)throws Throwable;
	
	
	

}
