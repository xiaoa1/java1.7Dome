package xiaoa.java.jms.activeMQ;


import java.util.Date;





import javax.jms.Connection;


import javax.jms.DeliveryMode;
import javax.jms.Message;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;
import org.apache.activemq.ActiveMQConnectionFactory;

import xiaoa.java.log.L;
import xiaoa.java.utils.time.DateFormatUtils;
public class ClientTest {
	
	// 消息工厂
	private static ActiveMQConnectionFactory  jmsSendFactory  =  null;
	
	private static final String QUEUE_NAME = "xiaoaQueue";

	private final static String url = "failover:(tcp://192.168.218.138:61616?wireFormat.maxInactivityDuration=0,tcp://xiaoa1:61616?wireFormat.maxInactivityDuration=0)"
			+ "?initialReconnectDelay=1000&jms.prefetchPolicy.all=100?maxReconnectDelay=30000000&maxReconnectAttempts=-1&warnAfterReconnectAttempts=0";


	
	/**
	 * 初始化
	 * @Title: init
	 * @throws Throwable
	 * @author xiaoa
	 */
	public static void init()throws Throwable{
		
//		// 创建一个url工具类
//		ActiveUrlUtils  url = new ActiveUrlUtils(Protocol.NIO , "xiaoa1" , 61616);
//		
//		url.setJmsPrefetchPolicyAll(100);
		
		jmsSendFactory  = new ActiveMQConnectionFactory(url);
	}

	
	public static void testSend()throws Throwable{
		
		// 初始化系统
		init();
		
		// 获取一个链接
		Connection   connection =  jmsSendFactory.createConnection();
		
		// 创建一个会话
		Session  session     = connection.createSession(Boolean.FALSE,Session.AUTO_ACKNOWLEDGE);
		
		// 创建队列
		Queue   sendQueue    = session.createQueue(QUEUE_NAME);
		
		// 创建一个生产者
		MessageProducer  producer =   session.createProducer(sendQueue);
		
		// 设置是否持久化
		producer.setDeliveryMode(DeliveryMode.PERSISTENT);
		
		StringBuilder  buffer  = new StringBuilder();
		
		for (int i =0 ; i < 2000 ; i ++){
			buffer.append("你");
		}
		
		
		long startTime = System.currentTimeMillis();
		
		for (int i =0 ; i < Integer.MAX_VALUE ; i ++){
			
			// 创建一条信息
			Message  message = session.createMessage();
			
			message.setStringProperty("json", "生产者[" + i + "]。这里是json" + DateFormatUtils.format_yyyyMMddhhssSSS(new Date()) );
			
			L.info(message.getStringProperty("json"));
			
			// 发送一条消息
			producer.send(message);
			
		}
		
		L.info("====================  完成   耗时  ：" + (System.currentTimeMillis() - startTime));
		
	}
	
	
	public static void main(String[] args) throws Throwable{
		
		testSend();
	}
	
	
	
	
	

}
