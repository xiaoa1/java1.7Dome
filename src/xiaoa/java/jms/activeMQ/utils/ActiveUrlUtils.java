package xiaoa.java.jms.activeMQ.utils;

import java.util.ArrayList;
import java.util.List;

/**
 * url Utils
 * @author xiaoa
 * @date 2017年3月19日 下午9:23:52
 * @version V1.0
 *
 */
public class ActiveUrlUtils {


	/**
	 * 一次获取消息数量
	 */
	private  Integer jmsPrefetchPolicyAll = 1000;
	
	
	
	/**
	 * url
	 */
	private StringBuilder   urlBuff  = new StringBuilder("");
	
	/**
	 * 网络
	 */
//	private StringBuilder   hostsBuff  = new StringBuilder("");
	
	
	/**
	 * 所有urlList
	 */
	private List<URL>  urlList  = new ArrayList<URL>();

	
	public void addUrlUtils(URL url){
		
		if (url == null){
			throw new RuntimeException("参数有误");
		}

		urlList.add(url);
	}
	
	
	/**
	 * 获取最终url
	 * @Title: createUrl
	 * @return
	 * @author xiaoa
	 */
	public String createUrl(){
		
		// 追加参数
		urlBuff.append("?");
		urlBuff.append("jms.prefetchPolicy.all").append("=").append(jmsPrefetchPolicyAll);
		
		return urlBuff.toString();
	}
	
	
	/**
	 * 连接模式
	 * @author xiaoa
	 * @date 2017年3月21日 下午8:14:37
	 * @version V1.0
	 *
	 */
	public static enum Syntax {failover};
	
	
	
	public static class URL{
		
		/**
		 * 协议
		 * @author xiaoa
		 * @date 2017年3月19日 下午9:29:59
		 * @version V1.0
		 *
		 */
		public static enum Protocol {TCP , VM , NIO}
		
		
		// 协议
		private Protocol protocol =  Protocol.NIO;
		
		// 主机名
		private String   host     =  null;
		
		// 端口
		private int      port     =  0;
		
		
		/**
		 * 参数前缀  参考  http://activemq.apache.org/configuring-wire-formats.html
		 */
		private String  PARAM_PREFIX = "wireFormat";
		
		// 配置参数
		

		/**
		 * Should commonly repeated values be cached so that less marshaling occurs?
		 * 一般应重复的值被缓存以减少编组发生？
		 */
		private boolean cacheEnabled         = true;
		
		/**
		 * When cacheEnabled=true then this parameter is used to specify the number of values to be cached.
		 * 当cacheenabled =true，这个参数是用来指定要缓存的值的数目。
		 */
		private int cacheSize                = 1024;
		
		/**
		 *
			The maximum inactivity duration (before which the socket is considered dead) in milliseconds. On some platforms it can take a long time for a socket to die. Therefore allow the broker to kill connections when they have been inactive for the configured period of time.
			Used by some transports to enable a keep alive heart beat feature.
			Inactivity monitoring is disabled when set to a value <= 0.
			
			以毫秒为单位的最大不活动时间（即套接字被认为是死的）。在某些平台上，套接字可能需要很长时间才能死去。因此，允许代理在配置时间段处于非活动状态时杀死连接。
			一些运输工具用来使一个保持跳动的心跳功能。
			当设置为值< 0时禁用活动监视）。
		 */
		private int    maxInactivityDuration = 30000;
		
		
		/**
		 * 	
			The initial delay before starting inactivity checks.
			Yes, the word 'Inital' is supposed to be misspelled like that.

			开始活动检查前的初始延迟。
			是的，“Inital”应该是错误的。
		 */
		private int maxInactivityDurationInitalDelay = 10000;
		
		
		/**
		 * Maximum allowed frame size. Can help help prevent OOM DOS attacks.
		 * 
			最大允许网络帧大小。可以帮助防止OOM的DoS攻击。
		 * 
		 */
		private long maxFrameSize = Long.MAX_VALUE;
		
		/**
		 * Should the size of the packet be prefixed before each packet is marshaled?
		 *  
                    该数据包的大小是前缀在每包封送？
		 */
		private boolean prefixPacketSize  = true;
		
		/**
		 * Should the stack trace of exception that occur on the broker be sent to the client?
		 * 是否发生在代理上的异常的堆栈跟踪被发送到客户端？
		 */
		private boolean stackTraceEnabled = true;
		
		
		/**
		 * Does not affect the wire format, but provides a hint to the peer that TCP nodelay should be enabled on the communications Socket.
		 * 不影响线的格式，但提供了一个暗示，对等TCP刻不容缓应该对socket通信功能
		 */
		private boolean tcpNoDelayEnabled = true;
		
		
		/**
		 * Should wire size be optimized over CPU usage?
		 * 
                      是否应该优化CPU的大小？
		 */
		private boolean tightEncodingEnabled = true;
		
		/**
		 * 获取url参数
		 * @Title: getQuery
		 * @return
		 * @author xiaoa
		 */
		public String  getQuery(){
			
			StringBuilder    queryBuff = new StringBuilder();
			
			queryBuff.append("?");
			queryBuff.append(PARAM_PREFIX).append(".").append("cacheEnabled").append("=").append(cacheEnabled);
			queryBuff.append("&").append(PARAM_PREFIX).append(".").append("cacheSize").append("=").append(cacheSize);
			queryBuff.append("&").append(PARAM_PREFIX).append(".").append("maxInactivityDuration").append("=").append(maxInactivityDuration);
			queryBuff.append("&").append(PARAM_PREFIX).append(".").append("maxInactivityDurationInitalDelay").append("=").append(maxInactivityDurationInitalDelay);
			queryBuff.append("&").append(PARAM_PREFIX).append(".").append("maxFrameSize").append("=").append(maxFrameSize);
			queryBuff.append("&").append(PARAM_PREFIX).append(".").append("prefixPacketSize").append("=").append(prefixPacketSize);
			queryBuff.append("&").append(PARAM_PREFIX).append(".").append("stackTraceEnabled").append("=").append(stackTraceEnabled);
			queryBuff.append("&").append(PARAM_PREFIX).append(".").append("tcpNoDelayEnabled").append("=").append(tcpNoDelayEnabled);
			queryBuff.append("&").append(PARAM_PREFIX).append(".").append("tightEncodingEnabled").append("=").append(tightEncodingEnabled);

			return queryBuff.toString();
		}
		
		
		/**
		 * 转为url字符串
		 * @Title: toUrlStr
		 * @return
		 * @author xiaoa
		 */
		public String toUrlStr(){
			
			StringBuilder  urlBuff = new StringBuilder();
			
			urlBuff.append(protocol.toString().toLowerCase()).append("://").append(host).append(":").append(port);
			urlBuff.append(getQuery());
			
			return urlBuff.toString();
		}
		
		
		
		
		
		public Protocol getProtocol() {
			return protocol;
		}

		public void setProtocol(Protocol protocol) {
			this.protocol = protocol;
		}

		public String getHost() {
			return host;
		}

		public void setHost(String host) {
			this.host = host;
		}


		public int getPort() {
			return port;
		}

		public void setPort(int port) {
			this.port = port;
		}

		public String getPARAM_PREFIX() {
			return PARAM_PREFIX;
		}


		public void setPARAM_PREFIX(String pARAM_PREFIX) {
			PARAM_PREFIX = pARAM_PREFIX;
		}

		public boolean isCacheEnabled() {
			return cacheEnabled;
		}

		public void setCacheEnabled(boolean cacheEnabled) {
			this.cacheEnabled = cacheEnabled;
		}


		public int getCacheSize() {
			return cacheSize;
		}


		public void setCacheSize(int cacheSize) {
			this.cacheSize = cacheSize;
		}


		public int getMaxInactivityDuration() {
			return maxInactivityDuration;
		}


		public void setMaxInactivityDuration(int maxInactivityDuration) {
			this.maxInactivityDuration = maxInactivityDuration;
		}

		public int getMaxInactivityDurationInitalDelay() {
			return maxInactivityDurationInitalDelay;
		}

		public void setMaxInactivityDurationInitalDelay(int maxInactivityDurationInitalDelay) {
			this.maxInactivityDurationInitalDelay = maxInactivityDurationInitalDelay;
		}

		public long getMaxFrameSize() {
			return maxFrameSize;
		}

		public void setMaxFrameSize(long maxFrameSize) {
			this.maxFrameSize = maxFrameSize;
		}

		public boolean isPrefixPacketSize() {
			return prefixPacketSize;
		}

		public void setPrefixPacketSize(boolean prefixPacketSize) {
			this.prefixPacketSize = prefixPacketSize;
		}

		public boolean isStackTraceEnabled() {
			return stackTraceEnabled;
		}


		public void setStackTraceEnabled(boolean stackTraceEnabled) {
			this.stackTraceEnabled = stackTraceEnabled;
		}


		public boolean isTcpNoDelayEnabled() {
			return tcpNoDelayEnabled;
		}

		public void setTcpNoDelayEnabled(boolean tcpNoDelayEnabled) {
			this.tcpNoDelayEnabled = tcpNoDelayEnabled;
		}


		public boolean isTightEncodingEnabled() {
			return tightEncodingEnabled;
		}

		public void setTightEncodingEnabled(boolean tightEncodingEnabled) {
			this.tightEncodingEnabled = tightEncodingEnabled;
		}

		public URL(Protocol protocol , String host , int port) {
			this.protocol = protocol;
			this.host     = host;
			this.port     = port;
		}
		
		
	}
	

	
	
	/**
	 * 返回一次获取消息数量
	 * @Title: getJmsPrefetchPolicyAll
	 * @return
	 * @author xiaoa
	 */
	public int getJmsPrefetchPolicyAll() {
		return jmsPrefetchPolicyAll;
	}

	/**
	 * 设置 一次获取消息数量
	 * @Title: setJmsPrefetchPolicyAll
	 * @param jmsPrefetchPolicyAll
	 * @author xiaoa
	 */
	public void setJmsPrefetchPolicyAll(int jmsPrefetchPolicyAll) {
		this.jmsPrefetchPolicyAll = jmsPrefetchPolicyAll;
	}
	
	
	@Override
	public String toString() {
		return createUrl();
	}
	
	
	
}
