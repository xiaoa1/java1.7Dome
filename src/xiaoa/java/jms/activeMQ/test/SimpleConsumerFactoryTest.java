package xiaoa.java.jms.activeMQ.test;

import javax.jms.Message;

import xiaoa.java.jms.activeMQ.consumer.IMQMessageHandle;
import xiaoa.java.jms.activeMQ.consumer.SimpleConsumerFactory;
import xiaoa.java.log.L;

/**
 * 测试类
 * @author xiaoa
 * @date 2017年3月22日 上午9:37:00
 * @version V1.0
 *
 */
public class SimpleConsumerFactoryTest {
	
	

	// 连接url
	private final static String url = "failover:(tcp://192.168.218.138:61616?wireFormat.maxInactivityDuration=0,tcp://192.168.218.1:61616?wireFormat.maxInactivityDuration=0)"
			+ "?initialReconnectDelay=1000&jms.prefetchPolicy.all=100?maxReconnectDelay=3000&maxReconnectAttempts=-1&warnAfterReconnectAttempts=0";

	
	
    
	/**
	 * 消费者1
	 * @author xiaoa
	 * @date 2017年3月22日 上午9:42:10
	 * @version V1.0
	 *
	 */
	public static class IMQMessageHandleTest1 implements IMQMessageHandle{

		@Override
		public boolean onMessage(Message message) throws Throwable {
			
			L.info("=========== 消费者 （1）  message =  " + message );
			
			return true;
		}
		
	}
	
	
	/**
	 * 消费者2
	 * @author xiaoa
	 * @date 2017年3月22日 上午9:42:10
	 * @version V1.0
	 *
	 */
	public static class IMQMessageHandleTest2 implements IMQMessageHandle{

		@Override
		public boolean onMessage(Message message) throws Throwable {
			
			L.info("=========== 消费者  （2） message =  " + message );
			
			return true;
		}
		
	}
	
	
	/**
	 * 消费者3
	 * @author xiaoa
	 * @date 2017年3月22日 上午9:42:10
	 * @version V1.0
	 *
	 */
	public static class IMQMessageHandleTest3 implements IMQMessageHandle{

		@Override
		public boolean onMessage(Message message) throws Throwable {
			
			L.info("=========== 消费者  （3） message =  " + message );
			
			return true;
		}
		
	}
	
	
	/**
	 * 初始化服务
	 * @Title: init
	 * @throws Throwable
	 * @author xiaoa
	 */
	public  static void init() throws Throwable{
		
		// 创建一个连接工厂
		SimpleConsumerFactory    consumerFactory   = new SimpleConsumerFactory(url);
		
//		// 使用默认session 
//		consumerFactory.addConsumer(new IMQMessageHandleTest1(), "test1", 0);
//		
//		// 使用默认session 
//		consumerFactory.addConsumer(new IMQMessageHandleTest2(), "test1_1", 0);
//		
//		// 使用默认session 
//		consumerFactory.addConsumer(new IMQMessageHandleTest3(), "test2", 0);
		
		// 使用单独session
		consumerFactory.addConsumer(new IMQMessageHandleTest2(), "test1", 3);
		
		// 使用单独session  3 个·消费者
		consumerFactory.addConsumer(new IMQMessageHandleTest3(), "test1_1", 2);
		
		L.info("=============== 初始化成功");
		
	}
	
	public static void main(String[] args)throws Throwable {
		init();
	}
	
	

}
