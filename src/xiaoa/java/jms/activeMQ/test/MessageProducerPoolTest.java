package xiaoa.java.jms.activeMQ.test;

import java.util.concurrent.CountDownLatch;

import javax.jms.Message;

import xiaoa.java.jms.activeMQ.Message.MessageProducerPool;
import xiaoa.java.utils.words.ExcelDome.L;

/**
 * 发送者Test
 * @author xiaoa
 * @date 2017年3月22日 上午10:39:40
 * @version V1.0
 *
 */
public class MessageProducerPoolTest {


	// 连接url
	private final static String url = "failover:(tcp://localhost:61616?wireFormat.maxInactivityDuration=0,tcp://192.168.218.1:61616?wireFormat.maxInactivityDuration=0)"
			+ "?initialReconnectDelay=1000&jms.prefetchPolicy.all=100?maxReconnectDelay=3000&maxReconnectAttempts=-1&warnAfterReconnectAttempts=0";

	// 发送者连接池
	private static MessageProducerPool messageProducerPool  = null;
	
	/**
	 * 初始化
	 * @Title: init
	 * @throws Throwable
	 * @author xiaoa
	 */
	public  static void init()throws Throwable{
		
		messageProducerPool  = new MessageProducerPool(url);
		
	} 
	
	
	static  int   num      = 0;
	
	static  int  duration  = 60 * 1000;
	
	static final long beginTime  =  System.currentTimeMillis();

	
	/**
	 * 检查是否结束
	 * @Title: checkEnd
	 * @return
	 * @author xiaoa
	 */
	public static synchronized boolean checkEnd(){
		if (System.currentTimeMillis() - beginTime >= duration){
			return true;
		}
		return false;
	}
	
	/**
	 * 增加数量
	 * @Title: addNum
	 * @author xiaoa
	 */
	public static synchronized void addNum(){
		num ++;
	}
	
	public static void main(String[] args)throws Throwable {
		
		// 初始化
		init();
		
		
		final StringBuilder  buffer  = new StringBuilder();
		
		for (int i =0 ; i < 2000 ; i ++){
			buffer.append("你");
		}
		
		final CountDownLatch  countDownLatch  = new CountDownLatch(120);
		
		// 运行函数
		Runnable  run  = new Runnable() {
			
			
			@Override
			public void run() {
				
				countDownLatch.getCount();
				
				try {
					
					
					while(true){
						if (checkEnd()){
							L.info("======== 退出 ");
							break ;
						}
						
						L.info("======== 等待成功 ");
						
						// 创建一个消息
						Message  message  = messageProducerPool.createMessage();
						
						message.setStringProperty("json", buffer.toString());
						
						messageProducerPool.sendMessage("test1", message);
						
						L.info("======== 发送成功 ");
						
						addNum();
						
					}
				
				} catch (Throwable e) {
					e.printStackTrace();
				}
				
				countDownLatch.countDown();
				
			}
		};
		
		
		
		for (int i = 0 ; i < 120 ; i ++){
			
			Thread  thread  = new Thread(run);
			
			thread.start();
			
		}
		
		countDownLatch.await();
		
		L.info("============================  num = " + num);
		
		
	}
	
	
	
	
	

}
