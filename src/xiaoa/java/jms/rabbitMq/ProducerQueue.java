package xiaoa.java.jms.rabbitMq;

import java.util.HashMap;

import java.util.Map;

import xiaoa.java.jms.rabbitMq.LinkQueueMgr.LinkNode;


/**
 * 生产者
 * @author xiaoa
 * @date 2017年10月19日 下午10:27:56
 * @version V1.0
 *
 */
public class ProducerQueue extends QueueBase {
	
	
	String zkKey = null;
	
	public ProducerQueue(String exchange ,String path ,String queueName) throws Throwable{
		super(exchange ,  path,queueName);
	}
	
	
	
	/**
	 * 
	 * 构造器
	 * <p>Title: </p>
	 * <p>Description: </p>
	 * @author xiaoa
	 * @param zkKey 在zk中的key
	 */
	public ProducerQueue(String zkKey)throws Throwable{
		
		this.zkKey = zkKey;
		
		LinkNode  node = LinkQueueMgr.getNode(zkKey);
		
		if (node == null){
			throw new RuntimeException("参数有误");
		}
		
		init(node.getExchange() , node.getRoutingKey(),node.getRoutingKey());
		
	}
	
	
	/**
	 * 发送信息
	 * @Title: doSendMessage
	 * @param obj
	 * @throws Throwable
	 * @author xiaoa
	 */
	public void doSendMessage(Object obj)throws Throwable{
		
		// 如果是用zk
		if (zkKey != null){
			LinkNode  node = LinkQueueMgr.getNode(zkKey);
			
			if (node == null){
				throw new RuntimeException("参数有误");
			}
			
			Map<String, Object>  headers = new HashMap<>();
			
			// 填充下一个节点信息
			if (node.getNextKey() != null && !node.getNextKey().trim().isEmpty()){
				
				LinkNode nextNode = LinkQueueMgr.getNode(node.getNextKey());
				
				if(nextNode != null){
					LinkQueueMgr.doFillNodeToMsg(headers, nextNode);
				}
			}
			
		    super.doSendMessage(node.getExchange(), node.getRoutingKey(), obj, headers);
			
		}else {
			
			// 其他方式发送
			doSendMessage(exchange, path, obj);
		}
		
	}


	
	
	

}
