package xiaoa.java.jms.rabbitMq;

/**
 * 链表常量
 * @author xiaoa
 * @date 2017年10月22日 下午8:26:36
 * @version V1.0
 *
 */
public class LinkConstant {
	
	
	/**
	 * 路由器
	 */
	public static final String NEXT_EXCHANGE = "NEXT_EXCHANGE";
	
	/**
	 * 绑定key
	 */
	public static final String NEXT_ROUTING_KEY = "NEX_ROUTING_KEY";
	
	/**
	 * 下个节点key
	 */
	public static final String NEXT_NEXT_KEY        = "NEXT_NEXT_KEY" ;
}
