package xiaoa.java.jms.rabbitMq;

import com.rabbitmq.client.ConnectionFactory;

/**
 * 连接工厂
 * @author xiaoa
 * @date 2017年10月19日 下午10:42:21
 * @version V1.0
 *
 */
public class Factory {
	
	/**
	 * 连接工厂
	 */
	private static ConnectionFactory  factory;
	
	public static void init(String host , int port){
		 factory = new ConnectionFactory();  
	     factory.setHost(host);
	     factory.setPort(port);
	     factory.setUsername("xiaoa");
	     factory.setPassword("xiaoa");
	}

	public static ConnectionFactory getFactory() {
		return factory;
	}
	

}
