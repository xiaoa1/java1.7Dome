package xiaoa.java.jms.rabbitMq;

import java.util.Map;


import com.alibaba.fastjson.JSON;

import xiaoa.java.zk.ZookeeperMgr;

/**
 * 链表节点工具类
 * @author xiaoa
 * @date 2017年10月22日 上午11:43:26
 * @version V1.0
 *
 */
public class LinkQueueMgr {
	
	
	/**
	 * 根目录
	 */
	private static String basePath = "/";
	
	/**
	 * 初始化
	 * @Title: init
	 * @param zkConnList
	 * @param basePath
	 * @author xiaoa
	 */
	public static void init(String basePath){
		
		if (basePath == null || basePath.trim().equals("")){
			basePath = "/";
		}else if (!basePath.endsWith("/")){
			basePath = basePath + "/";
		}
		
		if (!basePath.startsWith("/")){
			basePath = "/" + basePath;
		}
		
		LinkQueueMgr.basePath = basePath;	
		
	}
	
	
	
	
	/**
     * 发送到下一步
     * @Title: doPushNext
     * @param headers
     * @param obj
     * @author xiaoa
     */
    public static void doPushNext( Map<String,Object>  headers , Object obj , QueueBase queue)throws Throwable{
    	
    	if (headers == null || !headers.containsKey(LinkConstant.NEXT_EXCHANGE)
    			|| !headers.containsKey(LinkConstant.NEXT_ROUTING_KEY)){
    		return ;
    	}
    	
    	// 下一个目标节点
    	String nextExchange  = headers.get(LinkConstant.NEXT_EXCHANGE).toString();
    	
    	// 下一个目标节点
    	String netxRoutingKey  = headers.get(LinkConstant.NEXT_ROUTING_KEY).toString();
    	
    	// 下下一个目标节点
    	String nextNextKey = headers.get(LinkConstant.NEXT_NEXT_KEY) != null ? headers.get(LinkConstant.NEXT_NEXT_KEY).toString() : null;
    	
    	// 清除节点信息
    	doClearNode(headers);
    	
    	if (nextNextKey != null && !nextNextKey.trim().equals("")){
    
    		LinkNode  node = getNode(nextNextKey);
    		
    		doFillNodeToMsg(headers, node);
        	
    	}
    	
    	// 发送到下个节点
    	queue.doSendMessage(nextExchange, netxRoutingKey, obj, headers);
    	
    	
    }
    
    /**
     * 清除节点信息
     * @Title: doClearNode
     * @param headers
     * @author xiaoa
     */
    public  static void doClearNode( Map<String,Object>  headers ){
    	
    	if (headers == null){
    		return ;
    	}
    	
    	if (headers.containsKey(LinkConstant.NEXT_EXCHANGE)){
    		headers.remove(LinkConstant.NEXT_EXCHANGE);
    	}
    	
    	if (headers.containsKey(LinkConstant.NEXT_ROUTING_KEY)){
    		headers.remove(LinkConstant.NEXT_ROUTING_KEY);
    	}
    	
    	if (headers.containsKey(LinkConstant.NEXT_NEXT_KEY)){
    		headers.remove(LinkConstant.NEXT_NEXT_KEY);
    	}
    	
    } 
    
    /**
     * 填充节点信息到node中
     * @Title: doFillNodeToMsg
     * @param headers
     * @param node
     * @author xiaoa
     */
    public static void doFillNodeToMsg(Map<String,Object>  headers , LinkNode node ){
    	
    	if (headers == null || node == null){
    		return ;
    	}
    	
    	// 清除节点信息
    	doClearNode(headers);
    	
    	headers.put(LinkConstant.NEXT_EXCHANGE, node.getExchange());
    	headers.put(LinkConstant.NEXT_ROUTING_KEY, node.getRoutingKey());

    	// 下一个节点
    	if (node.getNextKey() != null && !node.getNextKey().isEmpty()){
        	headers.put(LinkConstant.NEXT_NEXT_KEY, node.getNextKey());
    	}
    	
    }
    
    
    /**
     * 获取节点
     * @Title: getNode
     * @param key
     * @return
     * @author xiaoa
     */
    public static LinkNode  getNode(String key)throws Throwable{
    	
		// 获取下一个节点
    	String nextLinkNodeJson = ZookeeperMgr.get(getPath(key));
    	
    	if (nextLinkNodeJson == null || nextLinkNodeJson.trim().isEmpty()){
    		return null;
    	}
    	
    	LinkNode node = JSON.parseObject(nextLinkNodeJson, LinkNode.class);
    	
    	return node;
    	
    }
    
    
    /**
     * 
     * @Title: getPath
     * @param path
     * @return
     * @author xiaoa
     */
    private static String getPath(String path){
    	
    	if (path == null || path.equals("")){
    		return null;
    	}
    	
    	if (path.startsWith("/")){
    		path = path.substring(1, path.length());
    	}
    	
    	return basePath + path;
    }
	
    
    /**
     * 初始化节点
     * @Title: initNode
     * @param key
     * @param node
     * @throws Throwable
     * @author xiaoa
     */
    public static void initNode(String key,LinkNode node)throws Throwable{
    	
    	if (node == null){
    		throw new NullPointerException(" node ");
    	}
    	
    	if (!ZookeeperMgr.exists(getPath(key))){
    	     ZookeeperMgr.insetOrupdate(getPath(key), JSON.toJSONString(node));
    	}
    	
    }
    
    
    /**
     * 初始化或者修改
     * @Title: insetOrupdate
     * @param key
     * @param node
     * @throws Throwable
     * @author xiaoa
     */
    public static void insetOrupdate(String key,LinkNode node)throws Throwable{
    	
    	if (node == null){
    		throw new NullPointerException(" node ");
    	}
    	
    	if (!ZookeeperMgr.exists(getPath(key))){
    	     ZookeeperMgr.insetOrupdate(getPath(key), JSON.toJSONString(node));
    	}
    	
    }
    
    
    /**
     * 节点对象
     * @author xiaoa
     * @date 2017年10月22日 下午8:17:11
     * @version V1.0
     *
     */
    public static class LinkNode{
    	
    	/**
    	 * 交换机
    	 */
    	private String exchange;
    	
    	/**
    	 * routingKey 路由key
    	 */
    	private String routingKey;
    	
    	/**
    	 * 队列名字
    	 */
    	private String queueName;
    	
    	/**
    	 * 下一个节点key
    	 */
    	private String nextKey;
    	
    	

		public String getQueueName() {
			return queueName;
		}

		public void setQueueName(String queueName) {
			this.queueName = queueName;
		}

		public String getExchange() {
			return exchange;
		}

		public void setExchange(String exchange) {
			this.exchange = exchange;
		}

		public String getRoutingKey() {
			return routingKey;
		}

		public void setRoutingKey(String routingKey) {
			this.routingKey = routingKey;
		}

		public String getNextKey() {
			return nextKey;
		}

		public void setNextKey(String nextKey) {
			this.nextKey = nextKey;
		}
    }

}
