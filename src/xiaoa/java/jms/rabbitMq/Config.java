package xiaoa.java.jms.rabbitMq;

/**
 * 配置
 * @author xiaoa
 * @date 2017年10月22日 上午11:15:25
 * @version V1.0
 *
 */
public class Config {
	
	private static Config config = null;
	
	
	public static Config newInstance(){
		if (config == null){
			config = new Config();
		}
		return config;
	}
	
	
	/**
	 * 路由器
	 */
	private String errorExchange = "error";
	
	/**
	 * 绑定key
	 */
	private String errorRoutingKey = "errorR";

	public String getErrorExchange() {
		return errorExchange;
	}

	
	public String getErrorRoutingKey() {
		return errorRoutingKey;
	}


}
