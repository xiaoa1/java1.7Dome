package xiaoa.java.jms.rabbitMq.test;

import xiaoa.java.jms.rabbitMq.ProducerQueue;

/**
 * 生产者测试
 * @author xiaoa
 * @date 2017年10月21日 上午11:45:02
 * @version V1.0
 *
 */
public class ProducerQueueTest {
	

	public static void main(String[] args)throws Throwable {
	
		ServiceMgrTest.initService();

		System.out.println("初始连接化成功");
		
		ProducerQueue queue = new ProducerQueue("sina_spider_start");
		
		ProducerQueue error = new ProducerQueue("errorQueue");
			
	    queue.doSendMessage("哈哈，，不买不是中国人 error");
		
	    error.doSendMessage("想不到吧。。。我是个错误");
		
		System.out.println("发送成功");
		
		System.exit(1);
		
	}
	

}
