package xiaoa.java.jms.rabbitMq.test;

import java.util.Map;



import com.rabbitmq.client.QueueingConsumer.Delivery;

import xiaoa.java.jms.rabbitMq.ConsumerQueue;
import xiaoa.java.jms.rabbitMq.ConsumerQueue.Listen;

/**
 * 消费者测试
 * @author xiaoa
 * @date 2017年10月21日 上午11:45:13
 * @version V1.0
 *
 */
public class ConsumerQueueTest {
	

	public static void main(String[] args)throws Throwable {
		
		ServiceMgrTest.initService();
		
		System.out.println("初始连接化成功");
		
		ConsumerQueue queue = new ConsumerQueue("sina_spider_start");
		
		// 监听
	    Listen  listen = new Listen(){

		@Override
		public Object doWord(Delivery delivery, Map<String, String> logMap) throws Throwable {
			String json = new String(delivery.getBody());
			
			System.out.println(json); 	
			
			if (json.contains("error")){
				throw new RuntimeException("error");
			}
			
			return null;
			
		}};
		
		queue.addListen("listen1", listen);
		
		
		System.out.println("监听成功");
		
	}
	

}
