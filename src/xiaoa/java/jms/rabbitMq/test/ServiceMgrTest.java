package xiaoa.java.jms.rabbitMq.test;

import xiaoa.java.jms.rabbitMq.Factory;

import xiaoa.java.jms.rabbitMq.LinkQueueMgr;
import xiaoa.java.jms.rabbitMq.LinkQueueMgr.LinkNode;
import xiaoa.java.log.L;
import xiaoa.java.zk.ZookeeperMgr;

public class ServiceMgrTest {

	
	/**
	 * 初始化服务
	 * @Title: initService
	 * @author xiaoa
	 */
	public static void initService()throws Throwable{
		
		
		/**
		 * 初始化mq服务
		 */
		Factory.init("xiaoa", 5672);
		L.info("初始化队列成功");
		
		/**
		 * 初始化 zk
		 */
		ZookeeperMgr.init("xiaoa1:2181");
		L.info("初始化 Zookeeper 成功");
		
		/**
		 * 初始化 链表服务
		 */
		LinkQueueMgr.init("base_queue");
		L.info("初始化 LinkQueueMgr 成功");

		
	    LinkNode  errorTest = new LinkNode();
	    errorTest.setExchange("error");
	    errorTest.setRoutingKey("errorR");
		LinkQueueMgr.initNode("errorQueue", errorTest);
		
	}
	
	
}
