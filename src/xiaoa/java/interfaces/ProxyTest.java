package xiaoa.java.interfaces;

/**
 * 动态代理测试接口
 * @author Administrator
 */
public interface ProxyTest {
	
	/**
	 * 测试类
	 * @param t1
	 * @param t2
	 * @return
	 */
	public  String  test(Integer  t1  , String  t2 )throws Throwable;
	

}
