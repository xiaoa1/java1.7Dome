package xiaoa.java.utils.time;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 日期格式化工具
 * @author xiaoa
 * @date 2017年3月17日 下午11:21:19
 * @version V1.0
 *
 */
public class DateFormatUtils {
	
	private DateFormatUtils (){}
	
	// 格式化
	public final static String yyyy_MM_dd_hh_ss_SSS = "yyyy-MM-dd HH:mm:ss:SSS"; 
	
	/**
	 * 格式化时间
	 * @Title: format
	 * @param pattern
	 * @param date
	 * @return
	 * @author xiaoa
	 */
	public static String format(String pattern , Date date){
		
		// 创建一个格式化时间
	    SimpleDateFormat   format = new SimpleDateFormat(pattern);
	    
	    return format.format(date);
	}
	
	
	/**
	 * 格式化
	 * @Title: parse
	 * @param pattern
	 * @param date
	 * @return
	 * @author xiaoa
	 * @throws ParseException 
	 */
	public static Date parse(String pattern , String date) throws ParseException {
		
		// 创建一个格式化时间
	    SimpleDateFormat   format = new SimpleDateFormat(pattern);
	    
	    return format.parse(date);
	}
	
	/**
	 * 格式化为   yyyy-mm-dd hh:ss:SSS
	 * @Title: format_yyyyMMddhhssSSS
	 * @param date
	 * @return
	 * @author xiaoa
	 */
	public static String format_yyyyMMddhhssSSS(Date date){
		return format(yyyy_MM_dd_hh_ss_SSS, date);
	}
	

}
