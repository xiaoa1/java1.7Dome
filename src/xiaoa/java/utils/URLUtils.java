package xiaoa.java.utils;

import java.net.HttpURLConnection;
import java.net.URL;


/**
 * url工具类
 * @author xiaoa
 * @date 2017年3月10日 上午10:42:24
 * @version V1.0
 *
 */
public class URLUtils {
	
	
	/**
	 * 判断url是否可以访问  true 为可以
	 * @Title: isNotCall
	 * @return
	 * @throws Throwable
	 * @author xiaoa
	 */
	public static boolean isNotCall(String urlStr)throws Throwable{
		
	    URL  url;  
	    
	    HttpURLConnection con;  
		try {
			 url = new URL(urlStr);  
		     con = (HttpURLConnection) url.openConnection();  
		     int  state = con.getResponseCode();  
		     if (state == 200) {  
		    	 // 设置为可用
		    	 return true;
		     }

		} catch (Exception e) {
			
		}
		
		
		return false;
		
	}
	


}
