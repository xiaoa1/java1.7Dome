package xiaoa.java.utils.net;

import java.io.InputStream;


import java.net.URL;
import java.util.Map;
import java.util.zip.GZIPInputStream;

import org.apache.commons.httpclient.DefaultHttpMethodRetryHandler;
import org.apache.commons.httpclient.Header;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.cookie.CookiePolicy;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.apache.commons.httpclient.params.HttpMethodParams;
import org.apache.commons.io.IOUtils;
import org.apache.http.client.params.ClientPNames;

import xiaoa.java.log.L;

import org.apache.commons.httpclient.HttpClient;


// 网络请求
@SuppressWarnings("deprecation")
public class HttpUtils {

	static ThreadLocal<HttpClient>  local  = new  ThreadLocal<HttpClient>();
	
	
	public  static String get(String url,String cookie,String charset,String proxyIPAddress,String proxyIPAddressPort,String refer , String agent)throws Exception{
//		connectionManager.setMaxTotalConnections(15);
		if(url == null){
			throw new Exception("参数有误");
		}
		
		if(agent == null || agent.trim().equals("")){
			agent = "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:39.0) Gecko/20100101 Firefox/39.0";
		}
		
		if(charset == null || charset.trim().equals("")){
			charset = "UTF-8";
		}
		
		String result=null;
		HttpClient client = local.get();
		
		if (client == null ){
			client = new HttpClient();
			local.set(client);
		}
		
		HttpMethod method =  new GetMethod(url);
		try {
			client.getParams().setContentCharset(charset);
			client.getParams().setParameter(ClientPNames.ALLOW_CIRCULAR_REDIRECTS, true); 
			client.getParams().setParameter(HttpMethodParams.HTTP_CONTENT_CHARSET, charset);
			client.getParams().setParameter(HttpMethodParams.RETRY_HANDLER, new DefaultHttpMethodRetryHandler(200, true));
			client.getParams().setCookiePolicy(CookiePolicy.IGNORE_COOKIES);
			client.getParams().setIntParameter("http.socket.timeout", 100000); 
			client.getHttpConnectionManager().getParams().setParameter("http.socket.timeout", 30000);
 			
			method.setRequestHeader("Host",getHost(url));
			method.setRequestHeader("Accept-Language", "zh-cn,zh;q=0.8,en-us;q=0.5,en;q=0.3");
			method.setRequestHeader("User-Agent", "360Spider");
			method.setRequestHeader("Connection", " Keep-Alive");
			method.setRequestHeader("Content-Type", "application/x-javascript;charset="+charset);
			method.setRequestHeader("Accept", "*/*");
			method.getParams().setParameter(HttpMethodParams.RETRY_HANDLER,new DefaultHttpMethodRetryHandler());
 			if(cookie != null && cookie.trim().equals("")){
 				method.setRequestHeader("Cookie", cookie);
 			}
 			if(refer != null && refer.trim().equals("")){
 				method.setRequestHeader("Referer", refer);
 			}
			int stats =client.executeMethod(method);
            if (stats == HttpStatus.SC_OK) {
                Header header= method.getResponseHeader("Content-Encoding");
                if(header!=null&&header.getValue() != null &&header.getValue().contains("gzip")){
                	InputStream input=new GZIPInputStream(method.getResponseBodyAsStream());
                    result = IOUtils.toString(input,charset);
                    IOUtils.closeQuietly(input);
                } else {
                    result = method.getResponseBodyAsString();
                }
            }
		} catch (Exception e)  
        {  
			method.abort();  
            e.printStackTrace ();  
            return null;  
        } finally {
        	method.abort(); 
        }
		return result;
	}
	
	/**
	 * get请求
	 * @Title: get
	 * @param url
	 * @return
	 * @throws Throwable
	 * @author xiaoa
	 */
	public static String get(String url)throws Throwable{
		if(url == null || url.trim().equals("")){
			return null;
		}
		
		return get(url, null, null, null, null, null, null);
		
	}
	
	/**
	 * get请求
	 * @Title: get
	 * @param url
	 * @return
	 * @throws Throwable
	 * @author xiaoa
	 */
	public static String get(URL url)throws Throwable{
		if(url == null ){
			return null;
		}
		return get(url.toString());
		
	}
	
	/**
	 *获取网络 host
	 * @param url
	 * @return
	 */
	public static String getHost(String url){
		if(url.indexOf("https://")>=0){
			url=url.replace("http://", "");
		}else if(url.indexOf("http://")>=0){
			url=url.substring(7);
		}
		// 截取
		if (-1 < url.indexOf("i.taobao.com")) { 
			url = url.replaceAll("i.taobao.com", "my.taobao.com");
		}
		return url.substring(0,url.indexOf("/")==-1?url.length():url.indexOf("/"));
	}	

	/**
	 * 发送一个post请求
	 * @Title: doPost
	 * @param url
	 * @param paramsMap
	 * @return
	 * @throws Throwable
	 * @author xiaoa
	 */
	public  static String doPost(String url , Map<String, String> paramsMap)throws Throwable{
		
		if (url == null || url.trim().equals("")){
			throw new RuntimeException("参数有误");
		}
		
		// 创建一个请求对象
		HttpClient  client = new HttpClient();
		
		// 创建一个请求方法
		PostMethod   method = new PostMethod(url);
		
		client.getParams().setContentCharset("UTF-8");
		client.getParams().setParameter(HttpMethodParams.HTTP_CONTENT_CHARSET, "utf-8");
		client.getParams().setParameter(HttpMethodParams.RETRY_HANDLER, new DefaultHttpMethodRetryHandler(200, true));
		client.getParams().setCookiePolicy(CookiePolicy.IGNORE_COOKIES);
		client.getParams().setIntParameter("http.socket.timeout", 30000); 
		client.getHttpConnectionManager().getParams().setParameter("http.socket.timeout", 30000);
			
		method.setRequestHeader("Host",getHost(url));
		method.setRequestHeader("Accept-Language", "zh-CN,zh;q=0.8");
		method.setRequestHeader("Accept-Encoding", "gzip, deflate, sdch");
		method.setRequestHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36");
		method.setRequestHeader("Connection", "close");
		method.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		method.setRequestHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
		
		if (paramsMap != null && paramsMap.size() > 0){
			for (String key : paramsMap.keySet()){
				method.setParameter(key, paramsMap.get(key));
			}
		}
		
	    // 发送请求
		int status =  client.executeMethod(method);
		
		L.info("==============  status = " + status);
		
		if (status != 200){
			return "";
		}
		
		String body = method.getResponseBodyAsString();
		
		method.abort();
		
		return body;
	}
	
	
	 /**
	    * 发起http请求
	    * 
	    */
	   public  static String doPostText(String url ,String text )throws Throwable{
			
			if (url == null || url.trim().equals("")){
				throw new RuntimeException("参数有误");
			}
			
			
			// 创建一个请求对象
			HttpClient  client = new HttpClient();
			
			// 创建一个请求方法
			PostMethod   method = new PostMethod(url);

			String  reponseBody = null;
			
			try {
				
				client.getParams().setContentCharset("UTF-8");
				client.getParams().setParameter(HttpMethodParams.HTTP_CONTENT_CHARSET, "utf-8");
				client.getParams().setParameter(HttpMethodParams.RETRY_HANDLER, new DefaultHttpMethodRetryHandler(200, true));
				client.getParams().setCookiePolicy(CookiePolicy.IGNORE_COOKIES);
				client.getParams().setIntParameter("http.connection.timeout",5 * 1000);
				
					
				method.setRequestHeader("Host",getHost(url));
				method.setRequestHeader("http.socket.timeout", "60000");
				method.setRequestHeader("Accept-Language", "zh-CN,zh;q=0.8");
				method.setRequestHeader("Accept-Encoding", "deflate, sdch");
				method.setRequestHeader("User-Agent", "new_warning_search_engine");
				method.setRequestHeader("Connection", "close");
				method.setRequestHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
				
				if (text != null){
					method.setRequestEntity(new StringRequestEntity(text ,"text/plain" , "GBK"));
				}
				
			    // 发送请求
				int status =  client.executeMethod(method);
				
				System.out.println("status = " + status);
				
				// 获取返回字符串
				reponseBody = method.getResponseBodyAsString();
				
			}finally {
				if (method != null){
					method.abort();
				}
			}
			
			return reponseBody;
		}



	
	public static void main(String[] args) throws Throwable {	
		
		String time = Long.toString(System.currentTimeMillis());

	    String resp = get("https://www.qiushibaike.com/article/119625205");
		
		
	    System.out.println(resp);
		}
	
	

}
