package xiaoa.java.utils.net;

import java.io.InputStream;

import java.net.URI;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;

import xiaoa.java.log.L;
import xiaoa.java.spider.parse.ParseHtml;

/**
 * http 工具类
 * @author xiaoa
 * @date 2016年11月5日 下午4:03:59
 * @version V1.0
 *
 */
public class MyHttpUtils {
	
	/**
	 * 创建get请求
	 * @Title: doGet
	 * @param uri
	 * @return
	 * @throws Throwable
	 * @author xiaoa
	 */
	public static Body doGet(URI  uri )throws Throwable{
		
		if(uri == null){
			throw new RuntimeException("参数有误！");
		}

		new sun.net.www.protocol.https.Handler();

		// 创建url
		URL url = uri.toURL();
		
		// 打开一个url链接
		URLConnection   conn  = null;
		
		// 获取输入流
		InputStream   input   = null;
		
		// 定义一个字符流
		StringBuilder  buffer = new StringBuilder();
		
		// 定义返回body
		Body  body = new Body();
		body.code  = 400;
				
	    try {
	    	
	    	// 打开连接
	    	conn   = url.openConnection();

			conn.setDoOutput(true);

			conn.getOutputStream();

			// 打开输入流
			input  = conn.getInputStream();



			// 一次读取1k
	    	byte[] bs = new byte[1024];
	    	
	        while(input.read(bs) > -1 ){
	        	buffer.append(new String(bs));
	        }
	    	
			body.code  = 200;

		}catch ( Exception e ){
			e.printStackTrace();
			L.info("------------网络链接错误");
			
		} finally {
			if(input != null){
				input.close();
			}
		}
	    
	    // 将内容填充到body中
	    body.body = buffer.toString();
		
	    return body;
	} 
	
	
	
	public static void main(String[] args)throws Throwable {

		// 设置为debug模式
//		System.setProperty("javax.net.debug" , "all");


		String context = doGet(URI.create("https://www.baidu.com/s?wd=shanghai&ie=UTF-8")).getBody();
		System.out.println(context);
		
//		List<String>  urlList =  parseHtml.getUrls(context);
//		
//		for(String url : urlList){
//			System.out.println("==============" + url);
//		}
		
	}
	
	

}
