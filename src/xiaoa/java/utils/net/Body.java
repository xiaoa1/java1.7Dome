package xiaoa.java.utils.net;

public class Body {
	
	// 状态码
	public int     code;
	
	// 内容
	public String  body;

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	
	

}
