package xiaoa.java.utils;

import java.util.List;

/**
 * obj 工具类
 * @author xiaoa
 * @date 2017年1月9日 下午4:39:18
 * @version V1.0
 *
 */
public class ObjectUtis {
	
	/**
	 * 合并
	 * @Title: toSp
	 * @param list
	 * @param sp
	 * @return
	 * @author xiaoa
	 */
	public static <T> String toMerge (List<T>  list , String sp){
		
		if(list == null || list.size() == 0 || sp == null){
			return null;
		}
		
		StringBuilder  sb  = new StringBuilder("");
		
		
		for(T t : list){
			if(sb.length() == 0){
				sb.append(t);
			}else{
				sb.append(sp);
				sb.append(t);
			}
		}
		
		return sb.toString();
		
	}
	
	/**
	 * 合并
	 * @Title: toMerge
	 * @param sp
	 * @param objs
	 * @return
	 * @author xiaoa
	 */
    public static String toMerge ( String sp , Object... objs ){
		
		if(objs == null || objs.length == 0 || sp == null){
			return null;
		}
		
		StringBuilder  sb  = new StringBuilder("");
		
		for(Object obj : objs){
			if(sb.length() == 0){
				sb.append(obj);
			}else{
				sb.append(obj);
				sb.append(obj);
			}
		}
		
		return sb.toString();
	}
	
    /**
     * 合并
     * @Title: toMerge
     * @param sp
     * @param objs
     * @return
     * @author xiaoa
     */
    public static String toMerge( String sp , String... objs ){
		
		if(objs == null || objs.length == 0 || sp == null){
			return null;
		}
		
		StringBuilder  sb  = new StringBuilder("");
		
		for(Object obj : objs){
			if(sb.length() == 0){
				sb.append(obj);
			}else{
				sb.append(obj);
				sb.append(obj);
			}
		}
		
		return sb.toString();
	}
    
  /**
   * long类型转成byte数组 
   * @Title: longToByte
   * @param number
   * @return
   * @author xiaoa
   */
    public static byte[] longToByte(long number) { 
          long temp = number; 
          byte[] b = new byte[8]; 
          for (int i = 0; i < b.length; i++) { 
              b[i] = new Long(temp & 0xff).byteValue();// 将最低位保存在最低位 
              temp = temp >> 8; // 向右移8位 
          } 
          return b; 
      } 
      
      /**
       * byte数组转成long 
       * @Title: byteToLong
       * @param b
       * @return
       * @author xiaoa
       */
      public static long byteToLong(byte[] b) { 
          long s = 0; 
          long s0 = b[0] & 0xff;// 最低位 
          long s1 = b[1] & 0xff; 
          long s2 = b[2] & 0xff; 
          long s3 = b[3] & 0xff; 
          long s4 = b[4] & 0xff;// 最低位 
          long s5 = b[5] & 0xff; 
          long s6 = b[6] & 0xff; 
          long s7 = b[7] & 0xff; 
   
          // s0不变 
          s1 <<= 8; 
          s2 <<= 16; 
          s3 <<= 24; 
          s4 <<= 8 * 4; 
          s5 <<= 8 * 5; 
          s6 <<= 8 * 6; 
          s7 <<= 8 * 7; 
          s = s0 | s1 | s2 | s3 | s4 | s5 | s6 | s7; 
          return s; 
      } 
   
   
      /**
       *  int 转 byte
       * @Title: intToByte
       * @param number
       * @return
       * @author xiaoa
       */
      public static byte[] intToByte(int number) { 
          int temp = number; 
          byte[] b = new byte[4]; 
          for (int i = 0; i < b.length; i++) { 
              b[i] = new Integer(temp & 0xff).byteValue();// 将最低位保存在最低位 
              temp = temp >> 8; // 向右移8位 
          } 
          return b; 
      } 
   
       /**
        * byte转int
        * @Title: byteToInt
        * @param b
        * @return
        * @author xiaoa
        */
      public static int byteToInt(byte[] b) { 
          int s = 0; 
          int s0 = b[0] & 0xff;// 最低位 
          int s1 = b[1] & 0xff; 
          int s2 = b[2] & 0xff; 
          int s3 = b[3] & 0xff; 
          s3 <<= 24; 
          s2 <<= 16; 
          s1 <<= 8; 
          s = s0 | s1 | s2 | s3; 
          return s; 
      } 
   
      /**
       * shortToByte
       * @Title: shortToByte
       * @param number
       * @return
       * @author xiaoa
       */
      public static byte[] shortToByte(short number) { 
          int temp = number; 
          byte[] b = new byte[2]; 
          for (int i = 0; i < b.length; i++) { 
              b[i] = new Integer(temp & 0xff).byteValue();// 将最低位保存在最低位 
              temp = temp >> 8; // 向右移8位 
          } 
          return b; 
      } 
   
       
      /**
       * byteToShort
       * @Title: byteToShort
       * @param b
       * @return
       * @author xiaoa
       */
      public static short byteToShort(byte[] b) { 
          short s = 0; 
          short s0 = (short) (b[0] & 0xff);// 最低位 
          short s1 = (short) (b[1] & 0xff); 
          s1 <<= 8; 
          s = (short) (s0 | s1); 
          return s; 
      } 
      
      
      /**
       * 去掉前后空格
       * @Title: trim
       * @param value
       * @return
       * @author xiaoa
       */
      public static String trim(String value) {
    	  
          int len = value.length();
          int st = 0;
          char[] val = value.toCharArray();    /* avoid getfield opcode */
          
          while ((st < len) && (val[st] <= ' ' || val[st] == 65279 )) {
              st++;
          }
          while ((st < len) && (val[len - 1] <= ' '  || val[st] == 65279 )) {
              len--;
          }
          return ((st > 0) || (len < value.length())) ? value.substring(st, len) : value;
      }
	
	

}
