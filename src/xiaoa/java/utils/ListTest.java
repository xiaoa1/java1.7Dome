package xiaoa.java.utils;

import java.util.AbstractSequentialList;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

/**
 * list 测试
 * @author lkc
 *
 */
public class ListTest {
	
	
	//  创建一个list集合
	public static final  List<String>   list   =  new  ArrayList<String>();
	
	
	public static void main1(String[] args) throws Throwable{
		
		
		
		 //  获取系统参数
		Map<String, String >   env  =   System.getenv();
		
		for(String key :  env.keySet()){
			
			System.out.println("key  =   "+ key + "     value = " + env.get(key) );
		}
		
		
		
		System.out.println(6 >> 1);
		
		System.out.println(Integer.toBinaryString(50));
		
		System.out.println(Integer.toBinaryString(50 >> 1));

		
		System.out.println( 50 >> 1 );
		
		//  创建一个 Runnable 对象
		
		Runnable   run  =  new  Runnable() {
			
			@Override
			public void run() {
				
				try {
					//  创建一个死循环向list中添加元素
					Long   count  =  0l;
					while(true){

						String  value =   "线程name ： "+Thread.currentThread().getName() +"   count = "+count;
					
						System.out.println("添加 "+value);
						//  向list中添加值
						list.add(value);
					
						count++;
						// 线程休眠
						Thread.sleep(100);
						
					}
				
				} catch (Exception e) {
					e.printStackTrace();
				};
				
				
			}
		};
		
		list.subList(2, 3);
		
		
		// 创建线程
		for(int index = 1 ; index < 1 ; index ++){
			
			Thread   thread   =  new Thread(run , String.valueOf(index));
			
			// 开启线程
			thread.start();
			
		}
		
		//  主线程休眠
		Thread.sleep(1000);
		
		while(true){
			
			//  获取迭代器
		    Iterator<String>   its =  	list.iterator();
			

		    while(its.hasNext()){
		    	
		    	String  value =  its.next();
		    	
		    	System.out.println(" 打印数据   ：  "+value);
		    	
		    }
		    
		    //  主线程休眠
		    Thread.sleep(500);
		    
			
		}
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	}
	
	
	
	
	/**
	 * 实现list接口类
	 * @Title: listIm
	 * @author lkc
	 */
	public static void listIm (){
		
		AbstractSequentialList<String>     sequentialList   =  new   AbstractSequentialList<String>() {
			
			@Override
			public int size() {
				// TODO Auto-generated method stub
				return 0;
			}
			
			@Override
			public ListIterator<String> listIterator(int index) {
				// TODO Auto-generated method stub
				return null;
			}
		};
		
		sequentialList.iterator();
		
		
	}
	
	
	
	
	

}
