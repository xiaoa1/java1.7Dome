package xiaoa.java.utils.words;

import java.math.BigDecimal;
import java.util.List;

/**
 * 表格对象
 * @author lkc
 * @date 2017年6月22日 下午4:56:19
 * @version V1.0
 *
 */
public class FromComplexBean {
	
	
	
	public static final String NAME_WX = "微信";
	public static final String NAME_WB = "新浪微博";
	public static final String NAME_YC = "优创+";
	public static final String NAME_TT = "头条号";
	
	
	/**
	 * 品牌
	 */
	private String name  = "" ;
	
	/**
	 * 方案id
	 */
	private Integer keywordId;
	
	/**
	 * 总数量
	 */
	private Long num  = 0L;

	/**
	 * 总敏感百分比
	 */
	private BigDecimal sensitivePercent = new BigDecimal(0);
	
	/**
	 * 转发数
	 */
	private Integer forwardNum  = 0;
	
	/**
	 * 评论数
	 */
	private Integer commentNum  = 0;
	
	/**
	 * 点赞数
	 */
	private Integer  laudNum  = 0;
	
	
	/**
	 * 阅读量
	 */
	private Integer readNum  = 0;
	
	/**
	 * 链接
	 */
	private String  hrefUri = "";
	

	/**
	 * 词云
	 */
	private List<String> wordList; 
	
	
	/**
	 * 媒体list
	 */
	private List<Media>  list;
	
	
	
	public List<String> getWordList() {
		return wordList;
	}



	public void setWordList(List<String> wordList) {
		this.wordList = wordList;
	}


	public Long getNum() {
		return num;
	}



	public void setNum(Long num) {
		this.num = num;
	}



	public BigDecimal getSensitivePercent() {
		return sensitivePercent;
	}





	public void setSensitivePercent(BigDecimal sensitivePercent) {
		this.sensitivePercent = sensitivePercent;
	}





	public Integer getReadNum() {
		return readNum;
	}





	public void setReadNum(Integer readNum) {
		this.readNum = readNum;
	}



	public Integer getKeywordId() {
		return keywordId;
	}



	public void setKeywordId(Integer keywordId) {
		this.keywordId = keywordId;
	}



	/**
	 * 媒体
	 * @author lkc
	 * @date 2017年6月22日 下午4:59:22
	 * @version V1.0
	 *
	 */
	public static class Media{
		
		/**
		 * 来源名字
		 */
		private String sourceName  = "" ;
		
		
		/**
		 * 数量
		 */
		private Long num = 0L;
		
		/**
		 * 数量
		 */
		private Integer nomalNum = 0;
		
		/**
		 * 敏感数量
		 */
		private Integer sensitiveNum  = 0;
		
		
		/**
		 * 敏感百分比
		 */
		private BigDecimal sensitivePercent = new BigDecimal(0);
		
		/**
		 * 转发数
		 */
		private Integer forwardNum  = 0;
		
		/**
		 * 评论数
		 */
		private Integer commentNum  = 0;
		
		/**
		 * 点赞数
		 */
		private Integer  laudNum  = 0;
		
		
		/**
		 * 阅读量
		 */
		private Integer readNum  = 0;
		
		
		
		/**
		 * 链接
		 */
		private String  hrefUri = "" ;


		
		
		
		
		public Integer getSensitiveNum() {
			return sensitiveNum;
		}

		public void setSensitiveNum(Integer sensitiveNum) {
			this.sensitiveNum = sensitiveNum;
		}

		public Integer getLaudNum() {
			return laudNum;
		}

		

		public String getSourceName() {
			return sourceName;
		}

		public void setSourceName(String sourceName) {
			this.sourceName = sourceName;
		}

		public Integer getNomalNum() {
			return nomalNum;
		}

		public void setNomalNum(Integer nomalNum) {
			this.nomalNum = nomalNum;
		}

		public BigDecimal getSensitivePercent() {
			return sensitivePercent;
		}

		public void setSensitivePercent(BigDecimal sensitivePercent) {
			this.sensitivePercent = sensitivePercent;
		}

		public Integer getForwardNum() {
			return forwardNum;
		}

		public void setForwardNum(Integer forwardNum) {
			this.forwardNum = forwardNum;
		}

		public Integer getCommentNum() {
			return commentNum;
		}

		public void setCommentNum(Integer commentNum) {
			this.commentNum = commentNum;
		}


		public void setLaudNum(Integer laud) {
			this.laudNum = laud;
		}

		

		public String getHrefUri() {
			return hrefUri;
		}

		public void setHrefUri(String hrefUri) {
			this.hrefUri = hrefUri;
		}

		public Integer getReadNum() {
			return readNum;
		}

		public void setReadNum(Integer readNum) {
			this.readNum = readNum;
		}

		public Long getNum() {
			return num;
		}

		public void setNum(Long num) {
			this.num = num;
		}

		
		
		
		
	}



	public Integer getForwardNum() {
		return forwardNum;
	}



	public void setForwardNum(Integer forwardNum) {
		this.forwardNum = forwardNum;
	}



	public Integer getCommentNum() {
		return commentNum;
	}



	public void setCommentNum(Integer commentNum) {
		this.commentNum = commentNum;
	}



	public Integer getLaudNum() {
		return laudNum;
	}



	public void setLaudNum(Integer laudNum) {
		this.laudNum = laudNum;
	}



	public String getName() {
		return name;
	}





	public void setName(String name) {
		this.name = name;
	}





	public List<Media> getList() {
		return list;
	}





	public void setList(List<Media> list) {
		this.list = list;
	}



	public String getHrefUri() {
		return hrefUri;
	}



	public void setHrefUri(String hrefUri) {
		this.hrefUri = hrefUri;
	}
	
	
	
	
	
	

}
