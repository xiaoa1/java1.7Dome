package xiaoa.java.utils.words;

import java.io.File;



import java.io.FileInputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;



public class ExcelDome {
	
	
	// 退出退出写入标识
	private static  final UrlBean breakWriteBean = new UrlBean();
	
	// 最大值
	private static final Long  currentMaxKey = 0L;
	
	// 写入线程池
	private static  WirteToExcelQueueThreadPool  wirteToExcelQueueThreadPool ;
	
	/**
	 * 获取url
	 * @Title: filter
	 * @param filePath
	 * @return
	 * @throws Throwable
	 * @author xiaoa
	 */
	public static List<UrlBean>  readUrls(String filePath)throws Throwable{
		
		if(filePath == null || filePath.trim().equals("")){
			throw new RuntimeException("参数有误");
		}
		
		// 存储所有url
		List<UrlBean>   urlList  = new  LinkedList<>();
		
		// 读取文件
		FileInputStream fileInput = new FileInputStream(filePath);
		
		// 创建对Excel工作簿文件的引用
		HSSFWorkbook workbook = new HSSFWorkbook(fileInput);
		
		try {
			
			// 获取工作蒲数量
			int  sheetCount       =  workbook.getNumberOfSheets();
			
			// 循环工作蒲
			for(int i = 0 ; i < sheetCount ; i ++ ){
				
				// 获取工作表
				HSSFSheet  sheet  =  workbook.getSheetAt(i);
				
				// 如果没有改工作蒲 或 工作蒲中没有行
				if(sheet == null || sheet.getPhysicalNumberOfRows() == 0 ){
					continue;
				}
				
				// 所有行数量
				int  rows  =  sheet.getPhysicalNumberOfRows();
				
				for(int r_i = 1 ; r_i < rows ; r_i ++){
					
					// 获取行操作对象
					HSSFRow  row = sheet.getRow(r_i);
					
					if(row == null ){
						 continue;
					}
					
					// 创建一个url对象
					UrlBean  bean = new UrlBean();
					
					// 当前单元格
					HSSFCell  currentCell = null ;
					
					currentCell = row.getCell(0);
					
					// 从行中取出 key
					String key = getCellValue(currentCell);
					
					// 如果没有key  不处理
					if (key == null || key.trim().equals("")){
						continue;
					}
				
					// 转为long
				    bean.key = Double.valueOf(key).longValue();
					
					// 从行中取出 webname
					currentCell  = row.getCell(1);
					String webname = currentCell == null ? null :currentCell.getStringCellValue();
					if(webname != null && !webname.trim().equals("webname")){
						bean.webname  = webname;
					}
					
					// 从行中取出 url
					currentCell  = row.getCell(2);
					if (currentCell == null){
						continue;
					}
					
					String url = currentCell == null ?  null : currentCell.getStringCellValue();
					
					if(url != null && !url.trim().equals("")){
						bean.url  = url;
					}
					
					// 从行中取出 province
					currentCell   = row.getCell(3);

					String province = currentCell == null ? null :  currentCell.getStringCellValue();
					
					if(province != null && !province.trim().equals("")){
						bean.province = province;
					}
					
					// 将bean添加到集合中
					urlList.add(bean);
					
				}
				
			}
			
		} catch (Exception e) {
			throw e;
		}finally {
			
			// 关闭流
			workbook.close();
		}
		
		return urlList;
	}
	
	
	/**
	 * url模型
	 */
	public static class UrlBean{
		
		// urlKey
		public Long key;
		
		// webname
		public String webname;
		
		// 链接地址
		public String url;
		
		// 省份
		public String province;
		
	}

	/**
	 * 写入队列bean
	 * @author xiaoa
	 * @date 2017年3月13日 上午10:56:15
	 * @version V1.0
	 *
	 */
	public static class WirteToExcelQueueBean{
		
		// 退出标志
		public static final WirteToExcelQueueBean BREAK_BEAN = new WirteToExcelQueueBean("退出" , "退出");
		
		// 文件名字
		public String fileName;
		
		// 文件名字
		public String selectName;
		
		// 队列待写入队列
		public BlockingQueue<UrlBean>  urlBeanQueue;
		
		public WirteToExcelQueueBean(String fileName , String selectName) {
			urlBeanQueue  = new LinkedBlockingQueue<UrlBean>();
			this.fileName = fileName;
			this.selectName = selectName;
		}
		
	}
	
	/**
	 * 写入队列
	 * @author xiaoa
	 * @date 2017年3月13日 上午11:07:02
	 * @version V1.0
	 *
	 */
	public static  class WirteToExcelQueueThreadPool {
		
		// 所有write队列
		final Map<String , WirteToExcelQueueBean>   wirteToExcelQueueThreadMap  = new  Hashtable<String , WirteToExcelQueueBean>();
		
		// 队列
		final BlockingQueue<WirteToExcelQueueBean>  queue  = new LinkedBlockingQueue<WirteToExcelQueueBean>();
		
		// 输出路径
		public String wirtePath;
		
		public WirteToExcelQueueThreadPool(String wirtePath) {
			
			if (!wirtePath.endsWith("/")){
				wirtePath = wirtePath + "/";
			}
			
			this.wirtePath = wirtePath;
		}
		
		// 开始队列
		public void  startThread(){
			
			Runnable  startRun = new Runnable() {
				
				@Override
				public void run() {
					
					while (true){
						try {
						
						   final  WirteToExcelQueueBean bean = queue.take();
						
							if (bean == WirteToExcelQueueBean.BREAK_BEAN){
								L.info("===============  创建输出线程退出");
								break;
							}
							
							L.info("============== 创建写入线程  " + bean.fileName);
							
							// 创建写入线程
							Thread  writeThread = new Thread(new Runnable() {
								
								@Override
								public void run() {

									try {
										
										// 写入
										writeToFile(bean.urlBeanQueue, bean.selectName, wirtePath + bean.selectName );
										
									} catch (Throwable e) {
										e.printStackTrace();
									}
									
								}
							});
							
							writeThread.start();
						
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
						
					}
					
				}
			};
			
			
			new Thread(startRun , "写入线程").start();
			
		}
		
		/**
		 * 包含
		 * @Title: contains
		 * @param fileName
		 * @return
		 * @author xiaoa
		 */
		public synchronized boolean contains(String fileName){
			
             for (String key : wirteToExcelQueueThreadMap.keySet()){
				
            	 if (key.equals(fileName)){
            		 return true;
            	 }
			 }
             
            return false;
			
		}
		
		/**
		 * 获取bean
		 * @Title: getWirteToExcelQueueBean
		 * @param fileName
		 * @return
		 * @author xiaoa
		 */
		public WirteToExcelQueueBean getWirteToExcelQueueBean(String fileName){
			return wirteToExcelQueueThreadMap.get(fileName);
		}
		
		// 关闭
		public  synchronized void close (){
			
			// 退出创建程序
			queue.add(WirteToExcelQueueBean.BREAK_BEAN);
			
			for (String key : wirteToExcelQueueThreadMap.keySet()){
				
				// 获取bean
				WirteToExcelQueueBean bean = wirteToExcelQueueThreadMap.get(key);
				
				// 添加到队列，标识队列要退出
				bean.urlBeanQueue.add(breakWriteBean);
			}
		}

		
		// 添加
		public boolean add(WirteToExcelQueueBean e) {
			
			if (e != WirteToExcelQueueBean.BREAK_BEAN){
				wirteToExcelQueueThreadMap.put(e.fileName, e);
			}
			
			return queue.add(e);
		}
	}
	
	
	/**
	 * 写入到文件
	 * @Title: writeToFile
	 * @param queue
	 * @param selectName
	 * @param outfilePath
	 * @throws Throwable
	 * @author xiaoa
	 */
	public static void writeToFile(BlockingQueue<UrlBean>  queue , String selectName , String outfilePath )throws Throwable{
		
		// 创建对Excel工作簿文件的引用
		HSSFWorkbook workbook = new HSSFWorkbook();
		
		try {
			
			// 创建工作蒲
			HSSFSheet  notCallSelect = 	workbook.createSheet(selectName);
			// 当前行
			int currentRows = 0;
			
			// 设置头
			setCellValue(notCallSelect, currentRows, 0, "id" );
			setCellValue(notCallSelect, currentRows, 1, "webname" );
			setCellValue(notCallSelect, currentRows, 2, "url" );
			setCellValue(notCallSelect, currentRows, 3, "province" );
			currentRows ++ ;
			
			while (true){
				
				if (queue.isEmpty() || currentRows % 1000 == 0){
					
					workbook.write(new File(outfilePath));
				}
				
				// 备份
                if (currentRows >= 10000 && currentRows % 10000 == 0){
					
					workbook.write(new File(outfilePath + "_" + currentRows / 10000 + "bak"));
					L.info("写入备份  == " + outfilePath + "_" + currentRows / 10000 + "bak");
				}
				
				// 获取一个
				UrlBean  bean  = queue.take();  
				
				// 如果是退出标志
				if (bean == breakWriteBean){
					workbook.write(new File(outfilePath));
					L.info("========== 写入退出");
					break;
				}
				
				setCellValue(notCallSelect, currentRows, 0, bean.key != null ? bean.key.toString() : "" );
				setCellValue(notCallSelect, currentRows, 1, bean.webname);
				setCellValue(notCallSelect, currentRows, 2, bean.url );
				setCellValue(notCallSelect, currentRows, 3, bean.province );
				currentRows ++ ;
			}
			
			
			
		} catch (Exception e) {
			throw e;
		}finally {
			
			// 关闭流
			workbook.close();
		}
		
		
	}
	
   
    // 设置表格值
	private static void setCellValue(HSSFSheet sheet, int iRow, int iCol,
			String val) {
		HSSFRow row = sheet.getRow(iRow);
		
		if (row == null ){
			row = sheet.createRow(iRow);
		}
		
		HSSFCell cell = row.createCell(iCol);
		cell.setCellValue(val);
	}
	
	// 获取表格数值
	@SuppressWarnings({  "deprecation" })
	private static String getCellValue(HSSFCell currentCell){
		
		switch (currentCell.getCellType()) {
		case  Cell.CELL_TYPE_NUMERIC:
			
			Double  doube =  currentCell.getNumericCellValue();
			
			return doube.toString();
			
       case  Cell.CELL_TYPE_STRING:
			
			return currentCell.getStringCellValue();
		default:
			return "";
		}
		
	}

	
	/**
	 * 过滤出已经抓取的
	 * @Title: filterComplete
	 * @param list
	 * @throws Throwable
	 * @author xiaoa
	 */
	public  static void filterComplete(List<UrlBean>  list , BlockingQueue<UrlBean> notCallQueue , BlockingQueue<UrlBean> normalQueue , String notCallFilePath , String normalFilePath )throws Throwable{
		
		
		// 完成url集合
		List<UrlBean>  completeBeanList  =  new ArrayList<UrlBean>();
		
		
		if (new File(notCallFilePath).exists()){
			
			List<UrlBean>  notCalleBeanList  = readUrls(notCallFilePath);
			if (notCalleBeanList != null && notCalleBeanList.size() > 0){
				completeBeanList.addAll(notCalleBeanList);
			}
			// 添加到  队列
		    notCallQueue.addAll(notCalleBeanList);
		}
		
		
		if (new File(normalFilePath).exists()){
			
			List<UrlBean>  normalCompleteBeanList  =  readUrls(normalFilePath);
			
			if (normalCompleteBeanList != null && normalCompleteBeanList.size() > 0){
				completeBeanList.addAll(normalCompleteBeanList);
				normalQueue.addAll(normalCompleteBeanList);
			}
		}
	
		if (completeBeanList.size() ==0 ){
			return ;
		}
		
		Map<String, UrlBean>  completeMap = new HashMap<String, UrlBean>();
		
		for (UrlBean bean : completeBeanList){
			completeMap.put(bean.url, bean);
		}
		
		// 获取总的列表迭代器
		Iterator<UrlBean>  its = list.iterator();
		
		while(its.hasNext()){
			
			UrlBean bean = its.next();
			
			// 如果已经处理过  删除掉
			if (completeMap.get(bean.url) != null){
				its.remove();
			}
		}
		
		
	}

	/**
	 * 过滤出 删除  网页  返回不可访问网页
	 * @param queue
	 * @param writePool
	 * @param threads
	 * @throws Throwable
     */
	public static void  filterNotCall(final Queue<UrlBean>  queue ,final WirteToExcelQueueThreadPool writePool , int threads)throws Throwable{
		
		// 设置网络超时时间
		System.setProperty("sun.NET.client.defaultConnectTimeout", String.valueOf(10 * 1000));
		
		
		// 定义一个线程计数器
		final CountDownLatch  countDownLatch = new CountDownLatch(threads);
		
		
		Runnable  run  = new Runnable() {
			
			@Override
			public void run() {

				// 开始一个线程
				countDownLatch.getCount();
				try {
					
					// 如果队列为空退出循环
					if(queue == null || queue.size() == 0){
						return ;
					}
					
					while(true){
						
						
						// 如果队列为空 退出循环
						if (queue.isEmpty()){
							break;
						}
						
						// 取出url
						UrlBean urBean = queue.poll();
						
						String urlStr = urBean.url;
						
						if (urlStr == null){
							break;
						}
						
						// 如果当前key小于已经测试的key退出
						if (urBean.key != null && urBean.key < currentMaxKey){
							break;
						}
						
						if(urlStr != null && !urlStr.trim().equals("")){
							

							// 判断是否可用
							int urlState = 404;
						     
						    URL  url;  
						
						    HttpURLConnection con;  
							try {
								 url = new URL(urlStr);  
							     con = (HttpURLConnection) url.openConnection();  
							    
							     // 设置超时时间
							     con.setReadTimeout(1000 * 10);
							     
							     int  state = con.getResponseCode();  
							     
							     urlState = state;
							     
							} catch (Exception e) {
								//L.info(" queue.size = " + queue.size() + "=============  不可用链接 " + urlStr);
							}
							
							 L.info(" queue.size = " + queue.size() + "=============   " + urlStr + " state = " + urlState);

							// 添加到队列
							String fileName  = "URL_Code_" + urlState + ".xls"; 
							
							WirteToExcelQueueBean  bean = null;
							
							if (!writePool.contains(fileName)){
								
								// 获取操作bean
							    bean = new WirteToExcelQueueBean(fileName, fileName);
								
								writePool.add(bean);
							}
							
							if (bean == null){
								bean  = writePool.getWirteToExcelQueueBean(fileName);
							}
							
							// 添加到处理队列中
							bean.urlBeanQueue.add(urBean);
							
							
						}
						
					}
				} catch (Exception e) {
					e.printStackTrace();
				}finally{
					
					// 结束一个线程
					countDownLatch.countDown();
				}
				
			}
		};
		
		
		// 开始创建线程
		for (int i = 0 ; i < threads ; i ++){
			
			Thread  thread  = new Thread(run);
			
			thread.setName("网络线程(" + i + ")" );
			thread.start();
		}
		
		// 等待线程执行结束
		countDownLatch.await();
		
		L.info("=================== 完成");
		
	}
	
	public static void main(String[] args) throws Throwable{
		
		
		if (args == null || args.length < 2){
			throw new RuntimeException("args[0]  = 输入源     args[1] =  输出目录  args[2] = 线程数 （默认300）");
		}
		
		L.info("输入源   = " + args[0] + "  输出目录 = " + args[1] + "   线程数 （默认300）");
		
		// 开始时间
		Long  startTime = System.currentTimeMillis();
		
		Integer  threads   = 300 ;
		
		if (args.length  == 3){
			threads = Integer.valueOf(args[2]);
		}
		
		String inFile  =  args[0];
		
		String outFilePath = args[1];
		
		if (!outFilePath.endsWith("/")){
			outFilePath = outFilePath +"/";
		}
		
		// 初始化写入线程池
		wirteToExcelQueueThreadPool = new WirteToExcelQueueThreadPool(outFilePath);
		
		// 开启
		wirteToExcelQueueThreadPool.startThread();
		
		// 开始读取
		List<UrlBean>   urlList  = readUrls(inFile);
		
		L.info("=========== 读取");
		
		
		// 创建过滤队列
		Queue<UrlBean>  urlQueue  = new LinkedBlockingDeque<UrlBean>();
		
		
		// 将bean添加到队列中
		urlQueue.addAll(urlList);
		
		L.info("=========== 添加到过滤队列   urlQueue.size() = " + urlQueue.size());
		
		Thread.sleep(10000);
		
		
		// 过滤不可用url
		filterNotCall(urlQueue, wirteToExcelQueueThreadPool , threads );
		
		L.info("=========== 过滤不可用");
		wirteToExcelQueueThreadPool.close();

		L.info("【xiaoa】 =============== 完成  耗时 =  " + (System.currentTimeMillis() - startTime ));
	
	}
	
	/**
	 * 日志类
	 * @author xiaoa
	 * @date 2017年3月16日 下午6:18:17
	 * @version V1.0
	 *
	 */
	public static class L{
		
		public static  LEvent  event;
		
		public static void info(Object ... objs){
			
			// 获取线程名字
			String threadName = Thread.currentThread().getName();
			
			SimpleDateFormat   format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss:SSS");
			
			String  info = ( format.format(new Date()) + " : threadName = [" + threadName+ "] " + ":" + merge(objs));
			
			if (event != null){
				event.info(info);
			}else{
				System.out.println(info);
			}
		}
		
		
		/**
		 * 将对象合并为字符串
		 * @Title: merge
		 * @param os
		 * @return
		 * @author xiaoa
		 */
		private static String merge(Object...  os){
			
			if(os == null ){
				return null;
			}
			
			// 创建字节缓存流
			StringBuilder  buff  =   new StringBuilder("");
			
			// 将日志对象拼接为字符串
			for(Object  o  :  os){
				buff.append(o.toString()+"  ");
			}
			
			return buff.toString();
		} 
		
		
		
		public static interface LEvent{
			public  void  info(String info);
			
		} 
		
		
	}
	
	

}
