package xiaoa.java.utils.words;

import javax.swing.JFrame;

import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JFileChooser;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;

import xiaoa.java.utils.words.ExcelDome.L.LEvent;
import javax.swing.JMenuBar;
import java.awt.Color;

/**
 * Created by lkc on 2017/3/16.
 */
public class SwingDome  extends JFrame implements ActionListener , WindowListener {

	private static final long serialVersionUID = 1L;
	
    JTextField textField      = null;
    JTextField inPathText     = null;;
	JButton beginButton       = null;
	JButton selectFileButton  = null;
    JButton inButton          = null;
    JButton outButton         = null;
	JPanel   panel            = null;
	final JTextArea logViewArea = new JTextArea();
	
	// 线程是否启动
	static transient boolean  isThreadRun = false;
	
	
	// 打印日志读写锁
	ReadWriteLock   logBufferReadWriteLock  = new ReentrantReadWriteLock();
	StringBuffer  logBuffer = new StringBuffer(); 
	
	JTextField outPathText;
	final JScrollPane scrollPane = new JScrollPane(logViewArea);
	
	public SwingDome() {
		logViewArea.setForeground(Color.GREEN);
		logViewArea.setBackground(Color.BLACK);
		setTitle("dome");
	    panel = new JPanel();
	 	getContentPane().add(panel);
	 	panel.add(scrollPane);

		panel.setLayout(null);
	    this.setBounds(400, 200, 1043, 655);  
	    this.setVisible(true);  
	    this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		
		beginButton = new JButton("开始");
		panel.add(beginButton);
		beginButton.setBounds(21, 553, 182, 27);
		beginButton.addActionListener(this);
		
		inButton = new JButton("选择处理文件");
		panel.add(inButton);
		inButton.setBounds(585, 553, 167, 27);
		inButton.addActionListener(this);
		
		inPathText = new JTextField();
		panel.add(inPathText);
		inPathText.setBounds(788, 554, 210, 24);
		inPathText.setColumns(10);
		
	
	  
	  outButton = new JButton("输出目录");
	  panel.add(outButton);
	  outButton.setBounds(231, 553, 113, 27);
	  outButton.addActionListener(this);

	  
	  outPathText = new JTextField();
	  panel.add(outPathText);
	  outPathText.setColumns(10);
	  outPathText.setBounds(361, 554, 210, 24);
	  
	
      /// 添加关闭
      addWindowListener(this);
 
 	  
 	  //logViewArea.setBounds(11, 23, 987, 517);
 	 // panel.add(logViewArea);
 	  
 	  JMenuBar menuBar = new JMenuBar();
 	  panel.add(menuBar);
 	  
 	 //分别设置水平和垂直滚动条总是出现 
 	 scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS); 
 	 scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS); 
 	  
 	  scrollPane.setBounds(21, 13, 980, 517);
 	  
 	  repaint();
      
      
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		
		// 如果是输入按钮
		if (e.getActionCommand() == inButton.getText()){
			
			//实例化一个文件选择器
			JFileChooser chooser = new JFileChooser();
			chooser.setApproveButtonText("确定");  
			chooser.showDialog(new JPanel(), "选择");
			
			File file         = chooser.getSelectedFile();  
			
			if (file == null ){
				inPathText.setText("未选中文件");
				return;
			}
			
			String fielaPath  = file.getPath();
			
			inPathText.setText(fielaPath);
			
		}
		
	    // 如果是输出按钮
		else if (e.getActionCommand() == outButton.getText()){
			
			//实例化一个文件选择器
			JFileChooser chooser = new JFileChooser();
			chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);		
			chooser.setApproveButtonText("确定");  
			chooser.showDialog(new JPanel(), "选择");
			
			File file         = chooser.getSelectedFile();  
			
			if (file == null ){
				outPathText.setText("未选中文件");
				return;
			}
			
			String fielaPath  = file.getPath();
			
			outPathText.setText(fielaPath);
			
		}
       
       // 如果是确定
		else if (e.getActionCommand() == beginButton.getText()){
			
			
			final String inputPath = inPathText.getText();
			
			final String outPath   = outPathText.getText();
			
			if (inputPath == null || inputPath.trim().equals("") || outPath == null || outPath.trim().equals("")){
				JOptionPane.showMessageDialog(new JPanel() , "请选择输入文件，以及输出目录", "错误",JOptionPane.WARNING_MESSAGE);  
				return ;
			}
			
			if (isThreadRun){
				JOptionPane.showMessageDialog(new JPanel() , "已经开始处理", "错误",JOptionPane.WARNING_MESSAGE);  
			}
			
			// 设置为已经开始处理
			isThreadRun = true;
			
			logViewArea.setText("开始处理");
			
			// 输出日志
			ExcelDome.L.LEvent   lEvent  = new LEvent(){
			
				public  void info(String info) {
					
					
				    try {
				    	
				    	// 获取一个读锁
					    logBufferReadWriteLock.readLock().lock();
				    	
				    	logBuffer.append("\n").append(info);
					} catch (Exception e2) {
						e2.printStackTrace();
					}finally{
					    // 释放锁
					    logBufferReadWriteLock.readLock().unlock();;
					}
					
				}
			};
			
			ExcelDome.L.event  = lEvent;
			
			// 开启一个线程处理，不然页面队列会卡
			Thread  thread = new Thread(new Runnable() {
				
				public void run() {
					try {
						ExcelDome.L.info("开始");
						ExcelDome.main(new String[]{inputPath , outPath , "300"});
						
					} catch (Throwable e1) {
						e1.printStackTrace();
						ExcelDome.L.info(e1.getMessage());
					}
				};
			
			});
		    thread.start();
		    
		 // 开启一个日志线程
		Thread  logThread = new Thread(){
			public void run() {	
				
				   // 休眠半秒
			    try {
					// 开一个循环来输出日志
					while (true){
						
					    try {
	
						 	// 获取一个读锁
						    logBufferReadWriteLock.writeLock().lock();
						    
						    if (logBuffer != null && logBuffer.length() > 0){
						    	logViewArea.append(logBuffer.toString());
						    	// 定位到最后一行
						    	logViewArea.setCaretPosition(logViewArea.getText().length());  
						    	
						    	// 清除缓冲区
						    	logBuffer  = new StringBuffer();
						    }
						    
						} catch (Exception e2) {
							e2.printStackTrace();
						}finally{
							logBufferReadWriteLock.writeLock().unlock();
	
						}
					 
					    // 线程休眠半秒钟
					    Thread.sleep(500);
					}
					
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				
			};
		};
		logThread.start();
		
		// 禁用按钮
		beginButton.setEnabled(false);
		}else {
			logViewArea.setText("选择错误");
		}
		
		System.out.println("================ 选中 ：" + e.toString());
		
		
	}
	
	
	public static void main(String[] args) {
		 new SwingDome();
	}

	@Override
	public void windowOpened(WindowEvent e) {
		
	}

	@Override
	public void windowClosing(WindowEvent e) {
	}

	@Override
	public void windowClosed(WindowEvent e) {
		System.exit(1);
	}

	@Override
	public void windowIconified(WindowEvent e) {
		
	}

	@Override
	public void windowDeiconified(WindowEvent e) {
		
	}

	@Override
	public void windowActivated(WindowEvent e) {
		
	}

	@Override
	public void windowDeactivated(WindowEvent e) {
		
	}
}
