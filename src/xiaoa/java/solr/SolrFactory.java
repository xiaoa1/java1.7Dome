package xiaoa.java.solr;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.apache.solr.client.solrj.impl.HttpSolrServer;
import org.apache.solr.client.solrj.response.UpdateResponse;
import org.apache.solr.common.SolrInputDocument;

public class SolrFactory {
	
	
	/**
	 * solr 服务
	 */
	public static HttpSolrServer solrServer = null;
	
	
	/**
	 * 初始化
	 * @Title: init
	 * @param url
	 * @author xiaoa
	 */
	public static void init(String url){
		
		solrServer = new HttpSolrServer(url);
		solrServer.setConnectionTimeout(120 * 1000);
		solrServer.setSoTimeout(120 * 1000);
		solrServer.setMaxTotalConnections(20);
		solrServer.setDefaultMaxConnectionsPerHost(2);
		solrServer.setMaxRetries(3);
		solrServer.setAllowCompression(true);
	}
	
	
	public static void main(String[] args)throws Exception {
		
		 init("http://search.solr.51wyq.cn/gov/solr/#/collection_2_tracyAA");
		 
		 solrServer.setBaseURL("http://search.solr.51wyq.cn/gov/solr/#/collection_2_tracyAA");
		 
		 Collection<SolrInputDocument> docList = new ArrayList<>();
		 
		 SolrInputDocument solrInput = new SolrInputDocument();
		 {
			 Map<String, Object>  map = new HashMap<>();
			 map.put("set", 12);
			 solrInput.setField("CommentsCount", map);
		 }
		 
		 {
			 Map<String, Object>  map = new HashMap<>();
			 map.put("set", 16);
			 solrInput.setField("AttitudesCount", map);
		 }
		 
//		 {
//            HashMap<String, Object> mapVersion = new HashMap<String, Object>();
//			mapVersion.put("set", 2);
//			solrInput.addField("_version", mapVersion);
//		 }
//		 
		 solrInput.setField("ID", "31511870997565495882463");
		 
		 docList.add(solrInput);
		 
		 UpdateResponse response =  solrServer.add(docList);
		 solrServer.commit();
		 
		 
		 System.out.println(response);
		
		
		
	}
	
	
	

}
