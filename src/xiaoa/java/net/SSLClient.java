package xiaoa.java.net;

import java.lang.management.ManagementFactory;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;

import javax.net.ssl.SSLSocketFactory;

public class SSLClient {
	public static void main(String[] args)throws Throwable{
		
		// 设置为debug模式
//	    System.setProperty("javax.net.debug" , "all");
		
		   // 设置加载
	    // 设置密匙存储库
	    System.setProperty("javax.net.ssl.keyStore", "E:\\Users\\Administrator\\git\\java1.7Dome\\resources\\cert\\sslclientkeys");
	    System.setProperty("javax.net.ssl.keyStorePassword", "123456");
	    
	    // 设置信任密匙库
	    System.setProperty("javax.net.ssl.trustStore", "E:\\Users\\Administrator\\git\\java1.7Dome\\resources\\cert\\sllclienttrust");
	    System.setProperty("javax.net.ssl.trustStorePassword", "123456");
		
//	    Socket  socket = SSLSocketFactory.getDefault().createSocket(new InetSocketAddress("www.baidu.com" , 4143).getAddress() , 443 );

	    Socket  socket = SSLSocketFactory.getDefault().createSocket(new InetSocketAddress("localhost" , 8012).getAddress() , 8012 );



		String name = ManagementFactory.getRuntimeMXBean().getName();  
		System.out.println(name);  
		String pid = name.split("@")[0];  
		
		System.out.println("启动成功 ： pid = " + pid  );
		
		ClientHandler.doHandler(socket);
		
	}

}
