package xiaoa.java.net;

import java.lang.management.ManagementFactory;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class ClientTest {
	
	
    static List<Socket>  socketList = new ArrayList<>();
 	
	public static void main(String[] args) throws Throwable{
	
		
//		long startTime = System.currentTimeMillis();
//		
//		 final CountDownLatch  threadCount = new CountDownLatch(1);
//		
//		 for (int i = 0 ; i < 1 ; i ++ ){
//			 
//			 Runnable  run =  new Runnable() {
//				
//				@Override
//				public void run() {
//					
//					threadCount.getCount();
//
//						try {
//							
//							int i = 0;
//							
//							while(i < 1000){
//								i ++;
//							    ClientTest.run();
//							}
//							
//							System.out.println("完成1");
//							
//						} catch (Throwable e) {
//							e.printStackTrace();
//						}
//					
//				   threadCount.countDown();
//					
//				}
//			  };
//			 
//			Thread thread = new Thread(run);
//			thread.start();
//			 
//		 }
//		 
//		 threadCount.await();
//		 
//		 System.out.println("use = " + (System.currentTimeMillis() - startTime));
		
		run();
		 
	}

	
	public static void run ()throws Throwable{
		
		// 设置为debug模式
	    System.setProperty("javax.net.debug" , "all");
		
		Socket  socket = new Socket("localhost" , 8111);
		
		socketList.add(socket);


		String name = ManagementFactory.getRuntimeMXBean().getName();  
		System.out.println(name);  
		String pid = name.split("@")[0];  
		
		System.out.println("启动成功 ： pid = " + pid  );
		
		ClientHandler.doHandler(socket);
		
		
	}
	
	
}
