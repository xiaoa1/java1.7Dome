package xiaoa.java.net;

import java.lang.management.ManagementFactory;

import xiaoa.java.utils.net.HttpUtils;


/**
 * http 连接池     
 * @author xiaoa
 * @date 2017年6月30日 下午11:20:21
 * @version V1.0
 *
 */
public class HttpTest2 {
	
	
	/**
	 * 测试
	 * @Title: main
	 * @param args
	 * @throws Throwable
	 * @author xiaoa
	 */
	public static void main(String[] args)throws Throwable {
		
		final long maxTime = Integer.MAX_VALUE ;
		final long start = System.currentTimeMillis() ;
		
		// 初始化
		String name = ManagementFactory.getRuntimeMXBean().getName();  
		System.out.println(name);  
		String pid = name.split("@")[0];  
		
		System.out.println("启动成功 ： pid = " + pid  );
		
	    try {
	    	
	         final int[]  count = new int[]{0};
		   	   
	    	for ( int i = 0 ; i < 300 ; i ++){
				
	    		
				Runnable run = new Runnable() {
					
					@Override
					public void run() {
						
						while(true){
						try {
							
							String name = Thread.currentThread().getName();
							
							int threadCount = 0;
							
								
								threadCount++;
							
								int countTemp = count[0] = count[0] + 1;
								
								long startTime = System.currentTimeMillis();
								
								String body = HttpUtils.doPost("http://xiaoa0/SpringDome/activemq/testSync", null);
								
								System.out.println( name + "  threadCount = " + threadCount + "count = " + countTemp + "  bodyLength = " + (body.length() < 30  ? body  : body.length() ) + "  use = " + (System.currentTimeMillis() - startTime));
								
								if (System.currentTimeMillis() - start >=  maxTime ){
									break;
								}
							
						} catch (Throwable e) {
							e.printStackTrace();
						}
						}
						
						
					}
				};
				
				// 处理线程
			    Thread  thread = new Thread(run);
			    thread.setName("线程" + i);
			    thread.start();
			    
			}
	    	
		} catch (Exception e) {
			e.printStackTrace();
		}
	    
	    System.out.println("结束");
		
		
	}
	
	

}
