package xiaoa.java.net;
import java.lang.management.ManagementFactory;
import java.net.ServerSocket;

/**
 * socket服务类测试
 * @author xiaoa
 * @date 2017年6月24日 下午5:04:54
 * @version V1.0
 *
 */
public class SocketTest {
	
	public static void main(String[] args) throws Throwable{
		
		ServerSocket   server = new ServerSocket(8012);
		
		String name = ManagementFactory.getRuntimeMXBean().getName();  
		System.out.println(name);  
		String pid = name.split("@")[0];  
		
		// 设置为debug模式
	    System.setProperty("javax.net.debug" , "all");
		
		System.out.println("启动成功 ： pid = " + pid  );
		
		ServerHander.doHandle(server);
		
		
	}
	
	
	

}
