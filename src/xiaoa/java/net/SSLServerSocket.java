package xiaoa.java.net;

import java.lang.management.ManagementFactory;
import java.net.ServerSocket;

import javax.net.ServerSocketFactory;
import javax.net.ssl.SSLServerSocketFactory;

/**
 * Created by Administrator on 2017/8/10.
 */
public class SSLServerSocket {
	
	
	public static void main(String[] args) throws Throwable{
		
		

		// 设置为debug模式
//	    System.setProperty("javax.net.debug" , "all");
		
	    
	    // 设置加载
	    // 设置密匙存储库
	    System.setProperty("javax.net.ssl.keyStore", "E:\\Users\\Administrator\\git\\java1.7Dome\\resources\\cert\\sslserverkeys");
	    System.setProperty("javax.net.ssl.keyStorePassword", "123456");
	    
	    // 设置信任密匙库
	    System.setProperty("javax.net.ssl.trustStore", "E:\\Users\\Administrator\\git\\java1.7Dome\\resources\\cert\\sllclienttrust");
	    System.setProperty("javax.net.ssl.trustStorePassword", "123456");
	    
	    
		// 获取默认工厂类
		ServerSocketFactory  socketServerFactory =	SSLServerSocketFactory.getDefault();
		
		// 创建一个服务
		ServerSocket  server =  socketServerFactory.createServerSocket(8012);
		
		String name = ManagementFactory.getRuntimeMXBean().getName();  
		System.out.println(name);  
		String pid = name.split("@")[0];  
		
		
		System.out.println("启动成功 ： pid = " + pid  );
		
		
		ServerHander.doHandle(server);

		
		
	}
	
    


}
