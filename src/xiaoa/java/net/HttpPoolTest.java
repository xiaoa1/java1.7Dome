package xiaoa.java.net;

import java.lang.management.ManagementFactory;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.net.ssl.SSLContext;

import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.LayeredConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import xiaoa.java.utils.net.HttpUtils;


/**
 * http 连接池     
 * @author xiaoa
 * @date 2017年6月30日 下午11:20:21
 * @version V1.0
 *
 */
public class HttpPoolTest {
	
	
	/**
	 * 数据库连接池
	 */
    private	static PoolingHttpClientConnectionManager   pool = null;
	
	
    /**
     * 初始化
     * @Title: init
     * @author xiaoa
     */
	public static void init()throws Throwable{
		
	    LayeredConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(SSLContext.getDefault());
       
	    Registry<ConnectionSocketFactory> socketFactoryRegistry = RegistryBuilder.<ConnectionSocketFactory> create()
		             .register("https", sslsf)
		             .register("http", new PlainConnectionSocketFactory())
		             .build();
		
		pool = new PoolingHttpClientConnectionManager(socketFactoryRegistry);
		pool.setMaxTotal(500);
		pool.setDefaultMaxPerRoute(75);
		
	}
	
	
	/**
	 * 获取http客户端
	 * @Title: getHttpClient
	 * @return
	 * @author xiaoa
	 */
	public  static  CloseableHttpClient  getHttpClient()throws Throwable{
		
		
	  	CloseableHttpClient httpClient = HttpClients.custom()
						                .setConnectionManager(pool)
						                .build();  
		
		return httpClient;
		
	}
	
	
	
	/**
	 * post请求
	 * @Title: doPost
	 * @param url
	 * @param parmas
	 * @return
	 * @throws Throwable
	 * @author xiaoa
	 */
	public static String doPost(String url , Map<String, String>  parmasMap)throws Throwable{
		
		// 请求客户端
		CloseableHttpClient  client      = null;
		CloseableHttpResponse  response   = null;
		HttpPost  post                   = null;
		try {
			
			 post = new HttpPost(url);
			 
		     List<NameValuePair> parList = new ArrayList<>();
			 
		     if (parmasMap != null){
		    	 
		    	 for (String key : parmasMap.keySet()){
					 
					 String value = parmasMap.get(key);
					 
					 if (value != null){
						 parList.add(new BasicNameValuePair(key , value));

					 }
					 
				 }
		     }
			
			 
		    post.setEntity(new UrlEncodedFormEntity(parList, "utf-8"));
			
		    // 获取客户端
			client    = getHttpClient();
			response = client.execute(post);
			 int statusCode = response.getStatusLine().getStatusCode();      //返回的结果码
			 
	         if (statusCode != 200) {
	        	 post.abort();
	        	 
	        	 return "statusCode = " + statusCode;
	         }			
	         
	         
	         HttpEntity httpEntity = response.getEntity();
	         String result = null;
	         if (httpEntity == null) {
	            
	             return null;
	         } else {
	             result = EntityUtils.toString(httpEntity, "utf-8");
	         }
	 
	         // 释放输入流
	         EntityUtils.consume(httpEntity);        //按照官方文档的说法：二者都释放了才可以正常的释放链接
	         return result;
	         
		} finally {
			
			if (response != null){
				response.close();
			}
			
		}
		
	}
	
	public static void main(String[] args)throws Throwable {
		
		
		
		// 初始化
		init();
		
//		final long maxTime = 60 * 1000 ;
		final long start = System.currentTimeMillis() ;
		
		String name = ManagementFactory.getRuntimeMXBean().getName();  
		System.out.println(name);  
		String pid = name.split("@")[0];  
		
		System.out.println("启动成功 ： pid = " + pid  );
		
	    try {
	    	
	         final int[]  count = new int[]{0};
		   	   
	    	for (int i = 0 ; i < 75 ; i ++){
				
				Runnable run = new Runnable() {
					
					
					@Override
					public void run() {
						
						long maxUse = 0;
						long minUse = 0;
						double avg  = 0;
						double threadCount = 0;
						long sum = 0;
						while(true){
							try {
								
		                        String name = Thread.currentThread().getName();
									
									threadCount ++;
								
									int countTemp = count[0] = count[0] + 1;
									
									long startTime = System.currentTimeMillis();
									
//									String body = doPost("http://xiaoa0", null);
									String body = HttpUtils.doPost("http://xiaoa0", null);

									
									long use = (System.currentTimeMillis() - startTime);
									
									if (minUse == 0 || use < minUse){
										minUse = use;
									}
									
									if (use > maxUse){
										maxUse = use;
									}
									
									sum = sum + use;
									try {
										avg = sum / threadCount;
										
										avg =  (int)avg;
										
									} catch (Throwable e) {
										// TODO: handle exception
									}
									
									System.out.println( name + "  threadCount = " + threadCount + "count = " + countTemp + "  bodyLength = " + (body.length() < 30  ? body  : body.length() ) 
											+ "  use = " + use + "   maxUse = " + maxUse  + "   minUse = " + minUse + "  avg = " + avg) ;
									
	//								if (System.currentTimeMillis() - start >=  maxTime ){
	//									break;
	//								}
									
								
							} catch (Throwable e) {
								e.printStackTrace();
							}
						
						}
						
						
					}
				};
				
				// 处理线程
			    Thread  thread = new Thread(run);
			    thread.start();
			    
			}
	    	
		} catch (Exception e) {
			e.printStackTrace();
		}
	    
	    System.out.println("结束");
		
		
	}
	
	

}
