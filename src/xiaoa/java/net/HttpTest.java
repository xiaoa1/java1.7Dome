package xiaoa.java.net;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import xiaoa.java.log.L;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.Map;

/**
 * http 测试类
 * @author lkc
 *
 */
public class HttpTest {
	
	public static void urlTest()throws Throwable{
		
		
		
		// 连接对象
		HttpURLConnection    con  = null ;
	 
		// url   输入流
		InputStream   inPut     = null;
		
		// 定义表单提交输出流
		OutputStream  postOut   =  null ;
		
		// 定义输出流
		OutputStream   outPut   = null;
		
		// 定义一个文件输入流用于输出文件
		InputStream   fileIn  =  null;
		
		
	  try {
		  
		  

			// 创建一个资源定位url对象
		//	URL   url  = new URL("http://img2.3lian.com/2014/f6/192/d/108.jpg");
			
			URL   url  = new URL("http://192.168.1.1:80");

			L.info("===================  getAuthority       =  " + url.getAuthority());
			L.info("===================  getDefaultPort     =  " + url.getDefaultPort());
			L.info("===================  getFile            =  " + url.getFile());
			L.info("===================  getHost            =  " + url.getHost());
			L.info("===================  getPath            =  " + url.getPath());
			L.info("===================  getPort            =  " + url.getPort());
			L.info("===================  getQuery           =  " + url.getQuery());
			L.info("===================  getRef()           =  " + url.getRef());
			L.info("===================  getUserInfo        =  " + url.getUserInfo());
			
		   // 创建连接
		       con=  	(HttpURLConnection) url.openConnection();
		    con.setRequestProperty("User-Agent", "xioaa");
		    
		    // 设置请求方式 为 post
		    con.setRequestMethod("POST");
		    
		    // 设置连接时间
		    con.setRequestProperty("Connection", "keep-alive");
		    
		    // 设置文件格式
		    con.setRequestProperty("Accept-Charset", "utf-8");
		    con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
		    con.setRequestProperty("Authorization", "Basic " + "YWRtaW46YWRtaW4=");
		    
		    // 设置允许上传文件
		    con.setConnectTimeout(5000);  
		    con.setReadTimeout(30000);  
		    con.setDoOutput(true);  
		    con.setDoInput(true);  
		    con.setUseCaches(false);  
		    
		    //  打开连接
		    con.connect();
		    
		    // 获取一个输出流  上传到服务器
		    postOut  =  new DataOutputStream( con.getOutputStream() );
		    
		    // 创建一个file输出流   用于上传文件
		    fileIn   =  new  FileInputStream( new File("e://xiaoa.jpg"));
		    
		    // 将文件写入上传输出流
		    for( byte[] bs = new byte[1] ; fileIn.read(bs) > -1 ;  ){
		    	
		    	// 将数据读取到上传输出流中
		    	postOut.write(bs);
		    	
		    	postOut.flush();
		    	
		    }
		    
		    // 关闭输出流
		    fileIn.close();
		    
		    
		    // 获取所有响应头
		    Map<String, List<String>>   headMap   =   con.getHeaderFields();
		    
		    for(String key :  headMap.keySet()){
		    	
		    	List<String> value  = headMap.get(key);
		    	L.info("==========  key = " + key + "    value = "  + value );
		    }
		    
		    //  body
		    // 获取一个输入流   必须先输出才能输入
		    inPut    =  con.getInputStream();
		    
		    
		    // 创建输出流
		    outPut   =  new  FileOutputStream("e://xiaoa.txt");
		    
		    // 创建一个输出流
		    int b  =  -1;
		    
		    while((b = inPut.read()) > -1){
		    	
		    	//  写入到输出流
		    	outPut.write(b);
		    	
		    	// 刷新缓存
		    	outPut.flush();
		    	
		    }
		    
		  
		    System.out.println("完成");
		    	
		
	} catch (Exception e) {
		// 将异常抛到外层
		 throw e;
	} finally {
		
		// 关闭上传输出流
		if( postOut != null){
			postOut.close();
		}
		
		// 如果有文件输出流  关闭输出流
		if(fileIn != null ){
			fileIn.close();
		}
		
		// 如果有输出流
		if(outPut != null  ){
			outPut.close();
		}
		
//		// 如果有输入流
//		if(inPut != null ){
//			inPut.close();
//		}
//		
		
		
		
		
	}
		
		
	}
	
	
    public static void getUrlBody(String urlStr)throws Throwable{ 
    	Connection.Response res = Jsoup.connect("https://www.baidu.com/link?url=30Lh6s_hQxte7oQWR-iDDUukQTwLzYP2hAUovPel48hvBwD8jIJ0F6gZZwy70UFO")
    			.timeout(60000)
    			.method(Connection.Method.GET)
    			.followRedirects(false)
    			.execute();
	 String str= res.header("Location");
	 
	 System.out.println(str);	 }
	
	
	public static void main(String[] args) throws Throwable{
		
	//	urlTest();

		getUrlBody("https://localhost:8112/link?url=30Lh6s_hQxte7oQWR-iDDUukQTwLzYP2hAUovPel48hvBwD8jIJ0F6gZZwy70UFO");
		
		
	}
	
	
	

}
