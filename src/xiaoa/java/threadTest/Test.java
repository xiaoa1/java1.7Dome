package xiaoa.java.threadTest;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import xiaoa.java.utils.time.DateFormatUtils;

public class Test {

	// 执行线程池
		private static ThreadPoolExecutor solrThreadPool = new ThreadPoolExecutor(200,
				200, 1L, TimeUnit.MINUTES, new LinkedBlockingQueue<Runnable>());
		
		
		public static void main(String[] args)throws Throwable {
			
			
			List<FutureTask<Boolean>>  taskList = new ArrayList<>();
			
			for (int i =0 ; i < 20 ; i ++){
				
				final int i_c = i;
				
				Callable<Boolean> callable = new Callable<Boolean>() {

					@Override
					public Boolean call() throws Exception {

					
						System.out.println("startTime " + i_c + " = " + DateFormatUtils.format_yyyyMMddhhssSSS(new Date()) );
						
						Thread.sleep(60 * 1000);
						
						
						System.out.println("endTime " + i_c + " = " + DateFormatUtils.format_yyyyMMddhhssSSS(new Date()) );

						
						return true;

					}
				};

				FutureTask<Boolean> task = new FutureTask<>(callable);
				
				solrThreadPool.execute(task);
				taskList.add(task);
				
			}
			
			
			// 等待执行完毕
			for (FutureTask<Boolean> task : taskList) {

				if (task == null) {
					continue;
				}

				// 最多等待20分钟
				task.get(20, TimeUnit.MINUTES);

			}
			
			
			
		}
		
		
		

	
}
