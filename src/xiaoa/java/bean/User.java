package xiaoa.java.bean;

public class User  extends Bean{
	
	private static final long serialVersionUID = 1L;

	//  用户名
	public String  name;
	
	//  性别
	public Integer sex;
	
	//   年龄
	public Integer age;
	
	//   描述
	public String  describe;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getSex() {
		return sex;
	}

	public void setSex(Integer sex) {
		this.sex = sex;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public String getDescribe() {
		return describe;
	}

	public void setDescribe(String describe) {
		this.describe = describe;
	}
	
	
	
	

}
