package xiaoa.java.bean;

import java.util.List;

public class IGroupResult {
	
	/**
	 * value
	 */
	private String groupValue = "";

	/**
	 * 数量
	 */
	private long total;// 整数值，原始数据

	private List<IGroupResult> subGroups;

	public String getGroupValue() {
		return groupValue;
	}

	public void setGroupValue(String groupValue) {
		this.groupValue = groupValue;
	}


	public long getTotal() {
		return total;
	}

	public void setTotal(long total) {
		this.total = total;
	}

	public List<IGroupResult> getSubGroups() {
		return subGroups;
	}

	public void setSubGroups(List<IGroupResult> subGroups) {
		this.subGroups = subGroups;
	}
	
	

}
