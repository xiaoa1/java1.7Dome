package xiaoa.java.bean;

import java.util.List;

/**
 * 分页工具
 * @author xiaoa
 * @date 2017年12月10日 下午12:45:48
 * @version V1.0
 *
 * @param <T>
 */
public class PaginationSupport<T> {
	
	/**
	 * 总数
	 */
	private long total;
	
	/**
	 * list
	 */
	private List<T> list;
	
	
	public PaginationSupport(List<T> list , long total){
		this.list = list;
		this.total = total;
	}
	

	public long getTotal() {
		return total;
	}

	public void setTotal(long total) {
		this.total = total;
	}

	public List<T> getList() {
		return list;
	}

	public void setList(List<T> list) {
		this.list = list;
	}
	
	

}
