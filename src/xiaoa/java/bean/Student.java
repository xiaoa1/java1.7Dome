package xiaoa.java.bean;

import java.util.Date;

import xiaoa.java.Hbase.annotations.HBaseColumn;
import xiaoa.java.Hbase.annotations.HBaseTable;


/**
 * 学生
 * @author xiaoa
 * @date 2017年1月8日 下午3:19:49
 * @version V1.0
 *
 */
@HBaseTable
public class Student extends User {

	private static final long serialVersionUID = 1L;
	
	//   老师名字
	@HBaseColumn
	public  String  teacherName;
	
	//   最后一次上课时间
	public   Date   lastTime;
	
	//   教师评价
	public   String  teacherEvaluation;
	
	//  语文
	public  Chinese  chinese;

	
	public Chinese getChinese() {
		return chinese;
	}

	public void setChinese(Chinese chinese) {
		this.chinese = chinese;
	}

	public String getTeacherName() {
		return teacherName;
	}

	public void setTeacherName(String teacherName) {
		this.teacherName = teacherName;
	}

	public Date getLastTime() {
		return lastTime;
	}

	public void setLastTime(Date lastTime) {
		this.lastTime = lastTime;
	}

	public String getTeacherEvaluation() {
		return teacherEvaluation;
	}

	public void setTeacherEvaluation(String teacherEvaluation) {
		this.teacherEvaluation = teacherEvaluation;
	}
	
	
	

}
