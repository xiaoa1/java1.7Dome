package xiaoa.java.bean;

import java.io.ByteArrayInputStream;

import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.lang.reflect.Field;

import xiaoa.java.log.L;

/**
 * 
 * @author lkc
 * 实现  Cloneable  为了实现浅层复制
 */
public class Bean implements Cloneable , Serializable {

	//  在反序列化的时候判断版本是否一致，，如果不一致抛出序列化版本不一致异常
	private static final long serialVersionUID = 0l;

	//  将对象转为字符串
	public  String toString(){
		StringBuffer   buff  =   new  StringBuffer("");
		
		try {
			
			Field[]   fs   =   	this.getClass().getFields();
			
			for(Field   f  :  fs){
				
				//  设置可以取private类型变量
				f.setAccessible(true);
				
				//  拼接到缓存流中
			    buff.append("     ").append(f.getName()).append(":").append(f.get(this));
				
			}
		
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return buff.toString();
		
		
	}

/**
 * 实现浅层复制   
 */
public Object clone() throws CloneNotSupportedException {
	
	//  调用  cloneable   接口进行对象clone   缺点  无法进行深层clone  
	return super.clone();
}


/**
 * @return
 * @throws Throwable
 */
//  忽略异常
 @SuppressWarnings("unchecked")  
public <T> T   cloneSerialize()throws Throwable{
	   
	   //  创建一个字节输出流
	   ByteArrayOutputStream   objOut          =  new ByteArrayOutputStream();
	   
	   //  创建一个对象输出流
	   ObjectOutputStream     objOutStream     =  new ObjectOutputStream(objOut);
	   
	   //  将对象写入输出流
	   objOutStream.writeObject(this);
	   
	   //  读取输出流的字节到输入流
	   ByteArrayInputStream     in             =  new ByteArrayInputStream(objOut.toByteArray());
	   
	   //  将字节流转换为对象流
	   ObjectInputStream        inStream       =  new  ObjectInputStream(in);
	   
	   //  反序列化为对象
	   T   coloneObj                           =   (T)inStream.readObject();
	   
	   //  返回对象
	   return   coloneObj;
	   
   }

 /**
  * 垃圾回收
  */
@Override
protected void finalize() throws Throwable {
	
	L.info(":gc ===========" + this.getClass().getName() +"  被回收  ");
	super.finalize();
}
	
	
}
