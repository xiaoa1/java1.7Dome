package xiaoa.java.bean;
public class Chinese extends Bean{
	private static final long serialVersionUID = 1L;

	//  成绩
	public  Float  good;
	
	//  批注
	public  String comment;

	
	public Float getGood() {
		return good;
	}

	public void setGood(Float good) {
		this.good = good;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}
	
	

}
