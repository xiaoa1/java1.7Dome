package xiaoa.java.http;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.net.ssl.SSLContext;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.LayeredConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import com.alibaba.fastjson.JSON;


/**
 * http 连接池工具
 * @author lkc
 * @date 2017年7月2日 上午10:13:04
 * @version V1.0
 *
 */
public class PoolHttpUtils {
	

	/**
	 * 数据库连接池
	 */
    private	static PoolingHttpClientConnectionManager   pool = null;
	
	
   /**
    * 初始化连接池
    * @Title: init
    * @param maxTotal   连接池中最大链接数量
    * @param defaultMaxPerRoute  一条线路中最大链接数
    * @throws Throwable
    * @author lkc
    */
	public static void init(int maxTotal ,int defaultMaxPerRoute)throws Throwable{
		
	    LayeredConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(SSLContext.getDefault());
       
	    Registry<ConnectionSocketFactory> socketFactoryRegistry = RegistryBuilder.<ConnectionSocketFactory> create()
													             .register("https", sslsf)
													             .register("http", new PlainConnectionSocketFactory())
													             .build();
		
		pool = new PoolingHttpClientConnectionManager(socketFactoryRegistry);
		pool.setMaxTotal(maxTotal);
		pool.setDefaultMaxPerRoute(defaultMaxPerRoute);
		
	}
	
	
	/**
	 * 获取http客户端
	 * @Title: getHttpClient
	 * @return
	 * @author xiaoa
	 */
	public  static  CloseableHttpClient  getHttpClient()throws Throwable{
		
		
		//  默认初始化
		if (pool == null){
			init(100, 50);
		}
		
	  	CloseableHttpClient httpClient = HttpClients.custom()
						                .setConnectionManager(pool)
						                .build();  
		
		return httpClient;
		
	}
	
	

	
	/**
	 * post请求
	 * @Title: doPost
	 * @param url
	 * @param parmasMap
	 * @param timeOut
	 * @param returnCla
	 * @return
	 * @throws Throwable
	 * @author lkc
	 */
	public static <T> T doPost(String url , Map<String, Object>  parmasMap  , long timeOut , Class<T> returnCla)throws Throwable{
		
		Header[] headers = new Header[1];
		
		headers[0] = new BasicHeader("http.connection.timeout", timeOut + "");
		
		ResponseBody  body =  doPost(url, parmasMap, headers);
		
		if(body != null && body.code == 200 && body.getBody() != null && !body.getBody().isEmpty()){
			return JSON.parseObject(body.getBody(), returnCla);
		}
		
		return null;
	}
	
	
	/**
	 * post请求
	 * @Title: doPost
	 * @param url
	 * @param parmas
	 * @return
	 * @throws Throwable
	 * @author xiaoa
	 */
	public static ResponseBody doPost(String url , Map<String, Object>  parmasMap , final Header[] headers)throws Throwable{
		
		// 请求客户端
		CloseableHttpClient  client      = null;
		CloseableHttpResponse  response  = null;
		HttpPost  post                   = null;
		
		ResponseBody  body = new ResponseBody();
		
		try {
			
			 post = new HttpPost(url);
			 
			 // 设置请求头
			 if (headers != null && headers.length > 0){
				 post.setHeaders(headers);
			 }
			 
		     List<NameValuePair> parList = new ArrayList<>();
			 
		     if (parmasMap != null){
		    	 
		    	 for (String key : parmasMap.keySet()){
					 
		    		 Object value = parmasMap.get(key);
					 
					 if (value != null){
						 parList.add(new BasicNameValuePair(key , value.toString()));
					 }
				 }
		     }
			 
		     post.setEntity(new UrlEncodedFormEntity(parList, "GBK"));
		 	
		     // 获取客户端
			 client     = getHttpClient();
			 
			 response   = client.execute(post);
			 
			 int statusCode = response.getStatusLine().getStatusCode();      //返回的结果码
  	         String result = null;

	         if (statusCode != 200) {
	        	 post.abort();
	         }else{
	        	
	        	 HttpEntity httpEntity = response.getEntity();
	  	         if (httpEntity == null) {
	  	             return null;
	  	         } else {
	  	             result = EntityUtils.toString(httpEntity, "GBK");
	  	         }
	  	 
	  	         // 释放输入流
	  	         EntityUtils.consume(httpEntity);        //按照官方文档的说法：二者都释放了才可以正常的释放链接
	         }			
	         
	         body.setCode(statusCode);;
	         body.setBody(result);
	         
	         return body;
	         
		} finally {
			
			if (response != null){
				response.close();
			}
			
		}
		
	}
	

	/**
	 * 返回内容·
	 * @author lkc
	 * @date 2017年7月2日 上午10:31:42
	 * @version V1.0
	 *
	 */
	public static class ResponseBody{
		
		/**
		 *返回状态
		 */
		private int code ;
		
		/**
		 * 返回json
		 */
		private String body;

		public int getCode() {
			return code;
		}

		public void setCode(int code) {
			this.code = code;
		}

		public String getBody() {
			return body;
		}

		public void setBody(String body) {
			this.body = body;
		}
		
		
	}
	
	
	public static void main(String[] args)throws Throwable {
		
		ResponseBody resp =  doPost("http://xiaoa0", null, null);
		
		System.out.println(resp.body);
		
	}
	
	

}
