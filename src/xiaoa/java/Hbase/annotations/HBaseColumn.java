package xiaoa.java.Hbase.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/***
 * Hbase 列 描述类
 * @author xiaoa
 * @date 2017年1月7日 上午1:23:59
 * @version V1.0
 *
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.FIELD })
public @interface HBaseColumn {
	
	/**
	 * 族
	 * @Title: family
	 * @return
	 * @author xiaoa
	 */
	String family() default "";
	
	/**
	 * 列名
	 * @Title: name
	 * @return
	 * @author xiaoa
	 */
	String name() default "";
	
	/**
	 * 默认值
	 * @Title: defaultValue
	 * @return
	 * @author xiaoa
	 */
	String defaultValue() default "";

}
