package xiaoa.java.Hbase.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Hbase 表描述类
 * @author xiaoa
 * @date 2017年1月7日 上午1:24:33
 * @version V1.0
 *
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.TYPE })
public @interface HBaseTable {
	
	/**
	 * 表名
	 * @Title: name
	 * @return
	 * @author xiaoa
	 */
	String tableName() default "";
	
}
