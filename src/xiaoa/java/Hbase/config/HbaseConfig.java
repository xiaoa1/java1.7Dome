package xiaoa.java.Hbase.config;

import java.util.Properties;

/**
 * hbase 配置
 * @author xiaoa
 * @date 2017年1月8日 下午2:44:37
 * @version V1.0
 */
public class HbaseConfig {
	
	// 默认簇名字
	public static String DEF_FAMILY_NAME                     = "df";
	
	//  -------------------
	//  hbase 配置
	public static String hbase_defaults_for_version_skip     = "true";
	
	// zookeeper  默认端口
	public static String hbase_zookeeper_property_clientPort = "2181";

	// hbase zookeeper 集群
	public static String hbase_zookeeper_quorum              = "192.168.20.130";
	
	// 是否是分布式
	public static String hbase_cluster_distributed           = "true";
	
	// dfs 是否支持追加
	public static String dfs_support_append                  = "true";
	
	

	/**
	 * 获取配置
	 * @Title: getProperties
	 * @return
	 * @author xiaoa
	 */
	public  static Properties   getProperties(){
		
		// 创建一个保存属性
		Properties   proper  =  new Properties();
		
		proper.setProperty("hbase.defaults.for.version.skip", hbase_defaults_for_version_skip);
		proper.setProperty("hbase.zookeeper.property.clientPort", hbase_zookeeper_property_clientPort);
		proper.setProperty("hbase.zookeeper.quorum", hbase_zookeeper_quorum);
		proper.setProperty("hbase.cluster.distributed", hbase_cluster_distributed);
		proper.setProperty("dfs.support.append", dfs_support_append);
		
		return proper;
	}  
	
	
	
	

}
