package xiaoa.java.Hbase.mgr;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

import xiaoa.java.Hbase.annotations.HBaseColumn;
import xiaoa.java.Hbase.annotations.HBaseTable;
import xiaoa.java.Hbase.bean.HbaseBean;
import xiaoa.java.Hbase.config.HbaseConfig;

/**
 * HBase voUtils
 * @author xiaoa
 * @date 2017年1月7日 上午10:48:05
 * @version V1.0
 *
 */
public class VoUtils {
	
	/**
	 * 获取列簇名字
	 * @Title: getFamily
	 * @return
	 * @author xiaoa
	 */
	public static String  getFamily(HBaseColumn  column){
		if(column == null ){
			throw new RuntimeException("VoUtils.getFamily column is null ");
		}
		
		// 获取列簇
	    String familyName =  column.family();
	    
	    return  familyName == null || familyName.trim().equals("") ? HbaseConfig.DEF_FAMILY_NAME : familyName ;
	}
	
	/**
	 * 获取列名
	 * @Title: getFamily
	 * @param column
	 * @return
	 * @author xiaoa
	 */
	public static String  getFamily(Field  column){
		if(column == null ){
			throw new RuntimeException("VoUtils.getFamily column is null ");
		}
		
		return getFamily(column.getAnnotation(HBaseColumn.class));
	}
	
	
	/**
	 * 获取表名
	 * @Title: getTableName
	 * @param table
	 * @param clas
	 * @return
	 * @author xiaoa
	 */
	public static String getTableName(HBaseTable  table , Class<?> clas){
		
		if(table == null ){
			throw new RuntimeException("VoUtils.getTableName column is null ");
		}
		
		if(clas == null ){
			throw new RuntimeException("VoUtils.getTableName clas is null ");
		}
		
		// 获取注解中的表名
		String tableName = table.tableName();
		
		if(tableName == null || tableName.trim().equals("")){
			
			// 以class 名作为 表名
			tableName  =  "t_" + clas.getSimpleName().toLowerCase();
		}
		
		return tableName;
	}
	
	
	
	/**
	 * 获取表名
	 * @Title: getTableName
	 * @param clas
	 * @return
	 * @author xiaoa
	 */
	public static <TT extends HbaseBean> String getTableName( Class<TT> clas){
		
		if(clas == null ){
			throw new RuntimeException("VoUtils.getTableName clas is null ");
		}
		return getTableName(clas.getAnnotation(HBaseTable.class) , clas);
	}

	/**
	 * 获取列名  如果不是列返回 null
	 * @Title: getColumnName
	 * @param f
	 * @return
	 * @author xiaoa
	 */
	public static String getColumnName(Field  f){
		if( f == null ){
			throw new RuntimeException("VoUtils.getColumnName  f is null ");
		}
		
		// 判断是否有列注解
		if(f.isAnnotationPresent(HBaseColumn.class)){
			
			String  name  =  f.getAnnotation(HBaseColumn.class).name();
			
			if(name == null || name.trim().equals("")){
				name = f.getName();
			}
			return name;
		}
		
		return  null;
		
	}
	
	

	/**
	 *  获取对象可用列value
	 * @Title: getColumnValueList
	 * @param data
	 * @return
	 * @throws Throwable
	 * @author xiaoa
	 */
	public static <TT extends HbaseBean> Map<String, Map<String, String>>  getColumnValueList(TT data)throws Throwable{
		if(data == null ){
		     throw new RuntimeException("  data  is null");
	    }
		
		// 创建族map
		Map<String, Map<String, String>> familyMap  = new HashMap<String,  Map<String, String>>();
		
		Field[]  fs  =  data.getClass().getFields();
		
		for(Field f : fs){
			
			// 如果没有列注解   
			if(!f.isAnnotationPresent(HBaseColumn.class)){
				continue;
			}
			
			// 获取列值
			Object value = f.get(data);
			if(value != null ){
				
			    //  获取族名
				String familyName  =  getFamily(f);
				
				// 获取列名
				String cloumnName =   VoUtils.getColumnName(f);
				
				// 获取列值
				Map<String, String>   valueMap  = familyMap.get(familyName);
				
				if(valueMap == null){
					valueMap  = new HashMap<String, String>();
					familyMap.put(familyName, valueMap);
				}
				
				// 添加到列值  value 中
				valueMap.put(cloumnName, value.toString());
			}
		}
		
		return familyMap;
		
	}
	
	
}
