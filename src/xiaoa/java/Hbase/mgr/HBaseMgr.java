package xiaoa.java.Hbase.mgr;


import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.client.HBaseAdmin;
import org.apache.hadoop.hbase.client.HConnection;
import org.apache.hadoop.hbase.client.HConnectionManager;
import org.apache.hadoop.hbase.client.HTableInterface;
import org.apache.hadoop.hbase.client.Put;

import xiaoa.java.Hbase.annotations.HBaseColumn;
import xiaoa.java.Hbase.annotations.HBaseTable;
import xiaoa.java.Hbase.bean.HbaseBean;
import xiaoa.java.Hbase.config.HbaseConfig;
import xiaoa.java.log.L;
import xiaoa.test.hbase.Student;

/**
 * HBase管理器
 * @author lkc
 *
 */
public  class HBaseMgr  {
	
	
	// 配置文件
	private static Configuration   conf          = null;
	
	// Hbase 管理工具
	private static HBaseAdmin  admin             = null;
	
	// HBase tablepool
	private static HConnection  tableMgr         = null;
	
	// threadlocal
	private static   ThreadLocal<Map<String, Object>>  threadLocal        =   new ThreadLocal<Map<String, Object>>();
	
	
	public static Map<String, Object>  getMap(){
		
		// 获取本地map
		Map<String, Object>   map  = threadLocal.get();
		
		if(map == null ){
			map = new HashMap<String, Object>();
			threadLocal.set(map);
		}
		
		return map;
	}
	
	/**
	 * 获取线程表
	 * @Title: getMap
	 * @return
	 * @author xiaoa
	 */
	@SuppressWarnings("unchecked")
	public static Map<String, HTableInterface> getTableMap(){
		
		Object  obj  = getMap().get("TABLE_MAP");
		
		Map<String, HTableInterface>   tableMap  = null ;
		
		if(obj != null)
		{
			tableMap= (Map<String, HTableInterface>)obj;
		}
		
		if(tableMap == null ){
			tableMap  = new HashMap<String, HTableInterface>();
			getMap().put("TABLE_MAP", tableMap);
		}
		
		return tableMap;
	}
	
	
	
	/**
	 * 初始化 mgr
	 */
	public  static void init()throws Throwable{

		// 创建配置文件
	     conf  =  HBaseConfiguration.create(getDefaultConf());
	     
	     // 初始化 admin
	     admin = new HBaseAdmin(conf);
	     L.info("============= HBaseMgr.init 初始化 admin  成功");
	     
	     // 初始化 tablemgr
	     tableMgr  = HConnectionManager.createConnection(conf);
	     L.info("============= HBaseMgr.init 初始化 tableMgr  成功");
	}
	
	/**
	 * 添加表
	 * @Title: addTable
	 * @param cla
	 * @throws Throwable
	 * @author xiaoa
	 */
	public static <TT extends HbaseBean> void  addTable(Class<TT> cla)throws Throwable{
		
		if(cla == null ){
			throw new RuntimeException(" cla  is null");
		}
		
		//  获取表名
		String tableName  =  VoUtils.getTableName(cla);
		
		// 判断表是否存在
		if(!admin.tableExists(tableName)){
			// 创建表
			createTable(cla);
		}
	
		// 获取表map
		Map<String, HTableInterface>  tableMap  = getTableMap();
		
		// 初始化 表池子
		tableMap.put(tableName, tableMgr.getTable(tableName)) ; 
		L.info("===============  init succ table  tableName  " + tableName);
		
	}
	
	
	
	/**
	 * 获取默认配置文件
	 * @Title: getDefaultConf
	 * @return
	 * @throws Throwable
	 * @author xiaoa
	 */
	public static Configuration getDefaultConf()throws Throwable{
		
	
		Configuration  conf  = new Configuration();
		
		// 加载  jar 根目录
		// 加载配置文件
		Properties   proper  = HbaseConfig.getProperties();
		
		// 获取key集合
		Enumeration<?>  keys =   proper.propertyNames();
		
		while (keys.hasMoreElements()) {
			Object name = keys.nextElement();
			
			if(name != null ){
				
				String value = proper.getProperty(name.toString());
				
				//  添加到配置文件中
				conf.set(name.toString(), value);
			}
		}
		
		return conf;
	}
	
	
	/***
	 * 获取表操作对象
	 * @Title: getTable
	 * @param cla
	 * @return
	 * @throws Throwable
	 * @author xiaoa
	 */
	public  static <TT extends HbaseBean> HTableInterface  getTable(Class<TT>  cla)throws Throwable{
		
		if(cla == null ){
			throw new RuntimeException("参数有误");
		}

		String tableName  = VoUtils.getTableName(cla);
		
		// 从池子中获取table 操作链接
		HTableInterface  table = getTableMap().get(tableName);
		
		if(table == null ){
			if(!admin.tableExists(tableName)){
				throw new RuntimeException("该表不存在 ");
			}
			
		    table = tableMgr.getTable(tableName);
		    
		    // 添加到池子中  
		    getTableMap().put(tableName, table);
		}
		
		return table;
	}
	
	
	
	/**
	 * 创建一个表
	 * @param t
	 * @return
	 */
	public static  boolean createTable(Class<?> clas)throws Throwable{
		
		if(clas == null){
			throw new RuntimeException("参数有误");
		}
		
		// 判断是否有表注解
		if(!clas.isAnnotationPresent(HBaseTable.class)){
			throw new RuntimeException("  该对象没有  HBaseTable 注解");
		}
		
		//  获取表名
		String tableName  = VoUtils.getTableName(clas.getAnnotation(HBaseTable.class) , clas);
		
		
		// 创建一个 table 描述类
		HTableDescriptor  desc  = new HTableDescriptor(tableName);
		
		// 获取field
		Field[] fs = clas.getFields();
		
		// 定义一个map  存储  列族
		Map<String, HColumnDescriptor>   familyMap  = new  HashMap<String, HColumnDescriptor>();
		// 循环
		for(Field  f : fs){
			
			//  如果该属性是一个列
			if(f.isAnnotationPresent(HBaseColumn.class)){
				
				// 获取列注解 
				HBaseColumn column  =  f.getAnnotation(HBaseColumn.class);
				
				// 获取列族名字
				String fName   =  VoUtils.getFamily(column);
				
				// 添加列
				HColumnDescriptor   family  =  familyMap.get(fName);
				
				if(family == null ){
					
					// 创建一个列族
					family   =  new HColumnDescriptor(fName);
					
					// 添加到map中
					familyMap.put(fName, family);
					
					// 添加到表中
					desc.addFamily(family);
				}
				
			}
		}
		
		if(familyMap.size() == 0){
			throw new RuntimeException(" column != 0 必须要有一个列 ");
		}
	
		// 创建表
		admin.createTable(desc);
		
		return false;
	}
	
	/**
	 * put 一个对象
	 * @param t
	 */
	public static <TT extends HbaseBean > void put(TT   data )throws Throwable{
	
		if( data == null){
			throw new RuntimeException("参数有误 ");
		}
		
		if( !data.getClass().isAnnotationPresent(HBaseTable.class) ){
			throw new RuntimeException("该表没有注解 ");
		}
		
		// 创建一个put操作
		byte[] key  = data.keyGen();
		
		if(key == null || key.length == 0){
			throw new RuntimeException("  key  不能为空 ");
		}
		
		// 获取表操作对象
		HTableInterface   table   =  getTable(data.getClass());
		
		// 创建一个put对象
		Put   put  = new Put(key);
	
		// 获取valueMap
		Map<String,Map<String , String> >   valueMap  =  VoUtils.getColumnValueList(data);
		
		if(valueMap == null || valueMap.size() == 0){
			return ;
		}
		
		// 添加到put对象中
		for(String fKey : valueMap.keySet()){
			
			// 列值集合
			Map<String, String>  cMap  = valueMap.get(fKey);
  			for(String cKey :  cMap.keySet()){
  				
  				// 添加类值到 map中
  				put.add(fKey.getBytes(), cKey.getBytes(), cMap.get(cKey).getBytes());
  			}
		}
		
		// 添加到表中
		table.put(put);
	}
	
	/**
	 * 插入一条记录
	 * @Title: put
	 * @param dataList
	 * @throws Throwable
	 * @author xiaoa
	 */
	public static <TT extends HbaseBean > void put(List<TT>   dataList)throws Throwable{
		
		if( dataList == null ||dataList.size() == 0 ){
			return ;
		}
		
		// 获取表操作对象
		HTableInterface   table   =  getTable(dataList.get(0).getClass());
		
		// 设置不自动刷新
		table.setAutoFlush(false);
		
		// 创建一个putlist集合
		List<Put>   putList  =  new ArrayList<Put>();
		
		// 循环所有数据
		for(TT data : dataList ){
			
			if( !data.getClass().isAnnotationPresent(HBaseTable.class) ){
				throw new RuntimeException("该表没有注解 ");
			}
				
			// 创建一个put操作
			byte[] key  = data.keyGen();
			
			if(key == null || key.length == 0){
				throw new RuntimeException("  key  不能为空 ");
			}
			
			// 创建一个put对象
			Put   put  = new Put(key);
		
			// 获取valueMap
			Map<String,Map<String , String> >   valueMap  =  VoUtils.getColumnValueList(data);
			
			if(valueMap == null || valueMap.size() == 0){
				return ;
			}
			
			// 添加到put对象中
			for(String fKey : valueMap.keySet()){
				
				// 列值集合
				Map<String, String>  cMap  = valueMap.get(fKey);
	  			for(String cKey :  cMap.keySet()){
	  				
	  				// 添加类值到 map中
	  				put.add(fKey.getBytes(), cKey.getBytes(), cMap.get(cKey).getBytes());
	  			}
			}
			
			// 添加到list中
			putList.add(put);
			
		}
		
		// 添加到表中
		table.put(putList);
		
		// 刷新缓存
		table.flushCommits();
		
		// 恢复自动刷新
		table.setAutoFlush(true);

		
	}
	
	/**
	 * lkc
	 * @param args
	 * @throws Throwable
	 */
	public static void main(String[] args) throws Throwable {
		
//		
//		//  默认配置
//		String hbase_default_xml =  "/xiaoa/java/Hbase/config/hbase-default.xml";
//		
//		// 自定义配置
//		String hbase_site_xml    =  "/xiaoa/java/Hbase/config/hbase-site.xml";
		
		// 初始化mgr
		init();
		
		// 创建表
		//createTable(Student.class);
		
		//  添加表数据
		
		Long   start  =  System.currentTimeMillis();
		
		for(int  i = 0 ; i < 100000000 ; i ++  ){
			    Student  student  = new Student();
			    //	student.key       = "1483885690439_620144";
				student.name      = "xiaoa1";
				student.teacherName = "老师名字";
				student.teacherEvaluation  = "goods _ sdada _dafdaf";
				put(student);
				
		}
	
		L.info("=============== 插入成功  耗时" + (System.currentTimeMillis() - start) );
		
	}
	
	
	
	

}
