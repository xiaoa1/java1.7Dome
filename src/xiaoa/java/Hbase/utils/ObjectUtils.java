package xiaoa.java.Hbase.utils;

import java.math.BigDecimal;

import java.math.BigInteger;
import java.sql.Date;

import org.joda.time.DateTime;

/**
 * obj 常用类
 * @author xiaoa
 * @date 2017年1月7日 上午1:32:00
 * @version V1.0
 *
 */
public class ObjectUtils {

	
	/**  
	  * 判断一个类是否为基本数据类型。  
	  * @param clazz 要判断的类。  
	  * @return true 表示为基本数据类型。  
	  */ 
	 public static boolean isBaseDataType(Class<?> clazz) 
	 {   
	     return 
	     (   
	         clazz.equals(String.class) ||   
	         clazz.equals(Integer.class)||   
	         clazz.equals(Byte.class) ||   
	         clazz.equals(Long.class) ||   
	         clazz.equals(Double.class) ||   
	         clazz.equals(Float.class) ||   
	         clazz.equals(Character.class) ||   
	         clazz.equals(Short.class) ||   
	         clazz.equals(BigDecimal.class) ||   
	         clazz.equals(BigInteger.class) ||   
	         clazz.equals(Boolean.class) ||   
	         clazz.equals(Date.class) ||   
	         clazz.equals(DateTime.class) ||
	         clazz.isPrimitive()   
	     );   
	 }
	 
	
	 /**
	  * 判断一个类是否是基本数据类型
	  * @Title: isBaseDataType
	  * @param clazz
	  * @return
	  * @throws Exception
	  * @author xiaoa
	  */
	 public static <T> boolean isBaseDataType(T t){
		 
		 if(t == null ){
			 return false;
		 }
		 
		 return isBaseDataType(t.getClass());
	 } 

	 
	
}
