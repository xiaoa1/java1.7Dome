package xiaoa.java.Hbase.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 生成key
 * @author xiaoa
 * @date 2017年1月7日 上午1:05:39
 * @version V1.0
 *
 */
public class KeyGen {
	
	/**
	 * 获取一个key
	 * @Title: getKey
	 * @return
	 * @author xiaoa
	 */
	public static  String getKey(){
		
		// 生成key
		String key = System.currentTimeMillis() + "_" + (int)(Math.random() * 1000000);
		
		return key;
	}
	
	
	/**
	 * 格式化数字
	 * @Title: formartNumber
	 * @param l
	 * @param length
	 * @return
	 * @author xiaoa
	 */
	public static String formartNumber(long l , int length ){
		
		String str  = new String(l+"");
		
		int i  = str.length();
		
		// 如果字符长度大于当前数字
		if(i > length){
			return str.substring(0, length);
		}
		
		String prefix  = "";
		
		for( ; i <= length; i ++ ){
			prefix = "0" + prefix;
		}
		
		return str + prefix;
	}
	
	/**
	 * 格式化日期
	 * @Title: dateFormartNumber
	 * @param l
	 * @return
	 * @author xiaoa
	 */
	public static String dateFormartNumber(Date date){
		return new SimpleDateFormat("yyyyMMdd").format(date); 
	}
	
	
}
