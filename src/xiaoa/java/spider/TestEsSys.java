package xiaoa.java.spider;

import com.alibaba.fastjson.JSON;


import xiaoa.java.es.client.ClientUtils;
import xiaoa.java.jms.rabbitMq.Factory;
import xiaoa.java.jms.rabbitMq.LinkQueueMgr;
import xiaoa.java.jms.rabbitMq.LinkQueueMgr.LinkNode;
import xiaoa.java.log.L;
import xiaoa.java.mongoDB.DbMgr;
import xiaoa.java.netty.HttpClientUtils;
import xiaoa.java.netty.SimpleHttpClient;
import xiaoa.java.zk.ZookeeperMgr;

public class TestEsSys {
	
	   /**
     * 初始化系统
     * @Title: initSys
     * @throws Throwable
     * @author xiaoa
     */
	public static void initSys()throws Throwable{
		
		// 初始化netty
		HttpClientUtils.init(new SimpleHttpClient());
		
		/**
		 * 初始化 zk
		 */
		ZookeeperMgr.init("xiaoa1:2181");
		L.info("初始化 Zookeeper 成功");
		
		
		// 初始化
		/**
		 * 初始化mq服务
		 */
		Factory.init("xiaoa", 5672);
		L.info("初始化mq服务成功");
		
		/**
		 * 初始化 链表服务
		 */
		LinkQueueMgr.init("spider");
		L.info("初始化 LinkQueueMgr 成功");
		
		// 初始化链表节点
		initNode();
		
		// 初始化es 客户端
	    ClientUtils.initClient();
		  
		// 初始化数据库
		DbMgr.init("xiaoa", 27017);
		DbMgr.initDatabase("fetchEs");
	}
	
	

	/**
	 * 初始化节点
	 * @Title: init
	 * @author xiaoa
	 */
	public static void initNode()throws Throwable{
		
		
		// 主节点
	    LinkNode  errorTest = new LinkNode();
	    errorTest.setExchange("spider");
	    errorTest.setRoutingKey("website");
	    errorTest.setRoutingKey("mainQueue");
	    errorTest.setNextKey("fetchNode_img");
		LinkQueueMgr.insetOrupdate("fetchNode", errorTest);
		
	    LinkNode  errorTest1 = new LinkNode();
	    errorTest.setExchange("spider");
	    errorTest.setRoutingKey("website");
	    errorTest.setRoutingKey("mainQueue1");
	    errorTest.setNextKey("fetchNode_img");
		LinkQueueMgr.insetOrupdate("fetchNode1", errorTest1);
		
		
	    LinkNode  imgTest = new LinkNode();
	    imgTest.setExchange("spider");
	    imgTest.setRoutingKey("website_img");
		LinkQueueMgr.insetOrupdate("fetchNode_img1", imgTest);
		
		
	}
	
	public static void main(String[] args) throws Throwable{
		
		
		/**
		 * 初始化 zk
		 */
//		ZookeeperMgr.init("xiaoa1:2181");
//		L.info("初始化 Zookeeper 成功");
		
	    LinkNode  errorTest = new LinkNode();
	    errorTest.setExchange("spider");
	    errorTest.setRoutingKey("website");
	    errorTest.setNextKey("fetchNode_img");
	    
	    System.out.println(JSON.toJSONString(errorTest));
//		LinkQueueMgr.insetOrupdate("fetchNode", errorTest);
		
	}
	
	
	
	

}
