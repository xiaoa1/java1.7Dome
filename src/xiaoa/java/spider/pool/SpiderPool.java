package xiaoa.java.spider.pool;

/**
 * 线程处理池
 * @author xiaoa
 * @date 2016年11月5日 下午5:41:09
 * @version V1.0
 *
 */
public class SpiderPool {
	
	/**
	 * 处理线程数量
	 */
	private int size  =  3;
	
	/**
	 * 处理线程
	 */
	private Runnable threadHandle = null;
	
	public SpiderPool(int size , Runnable handle) {
		
		if(handle == null ){
			throw new RuntimeException("参数有误");
		}
		
		if(size > 0){
			this.size = size;
		}
		
		this.threadHandle  = handle;
		
	}
	
	
	private void initPool(){
		// 创建线程
		for(int i = 1 ; i <= size ; i ++){
			
			Thread  thread = new Thread(threadHandle);
			thread.setName("线程 ：" + i);
			thread.start();
			
		}
		
	}
	
	/**
	 * 启动
	 * @Title: start
	 * @author xiaoa
	 */
	public void start(){
		initPool();
	}
	

}
