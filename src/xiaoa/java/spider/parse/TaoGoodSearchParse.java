package xiaoa.java.spider.parse;



import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

import xiaoa.java.Hbase.mgr.HBaseMgr;
import xiaoa.java.Hbase.utils.KeyGen;
import xiaoa.java.log.L;
import xiaoa.java.spider.bean.TaoSreachItem;
import xiaoa.java.utils.ObjectUtis;
import xiaoa.java.utils.net.HttpUtils;
import xiaoa.java.zk.ZookeeperMgr;
import xiaoa.java.zk.mgr.KeyMgr;

public class TaoGoodSearchParse {
	
	/**
	 * 解析
	 * @Title: parse
	 * @param body
	 * @return
	 * @throws Throwable
	 * @author xiaoa
	 */
	public static  List<TaoSreachItem> parse(String html) throws Throwable{
		if(html == null || html.trim().equals("")){
			return new ArrayList<TaoSreachItem>();
		}
		
		// 创建商品集合
		List<TaoSreachItem>  spusItList  = new ArrayList<TaoSreachItem>();
		
		// 解析html
		Document   doc  =  Jsoup.parse(html);
	
		
		// 获取数据所在标签
		Element    el   =  doc.select("script").get(6);

		String text = el.html();
		
		if(text != null ){
			
			// 截取json
			text = text.substring("g_page_config = ".length() -1 , text.indexOf("\n") - 1);
			
			// 转换json
			JSONObject   json  =   JSON.parseObject(text);
			
			// 获取商品列表
			String spus  = null;
			
			JSONObject  mods = json.getJSONObject("mods");
			
			if(mods != null){
				JSONObject  grid	= mods.getJSONObject("grid");
				
				if(grid != null ){
					JSONObject  data  = grid.getJSONObject("data");
					if(data != null){
						spus  = data.getString("spus");
					}
				}
			}
			
			if(spus == null || spus.equals("")){
				L.info("======================  没有更多了   ThreadName = " + Thread.currentThread().getName());
				
			}else{
				
				// 将json 解析到  对象中
			    spusItList  =  JSON.parseArray(spus, TaoSreachItem.class);
				
				for(TaoSreachItem  it : spusItList){
					
					if(it == null ){
						continue;
					}
					// 添加数据
					// 设置商户信息
					if(it != null && it.seller != null){
						it.s_num   =  it.seller.num;
						it.s_trace =  it.seller.trace;
						it.s_url   =  it.seller.url;
					}
					
					// 设置标签
					if(it != null && it.tag_info != null && it.tag_info.size() > 0){
						it.tags  =  ObjectUtis.toMerge(it.tag_info , ",");
					}
					
					// 设置key
					it.setKey(ObjectUtis.longToByte(Long.valueOf(KeyGen.dateFormartNumber(new Date()))) 
							, "_".getBytes() ,ObjectUtis.longToByte( it.cat ) 
							, ObjectUtis.longToByte(KeyMgr.getKey())); 
					
					L.info("===================== 商品名称 : " + it.title + " key = " + it.keyGen());
				
				}
				
			}
			
		}
		
		return spusItList;
	}
	
	
	public static void main(String[] args) throws Throwable {
		
		// 初始化zk
		ZookeeperMgr.init("192.168.218.136:2181");
		L.info("================ 初始化 zk 成功 ");
		
		// 初始化key
		KeyMgr.init("hbaseDome");
		
		// 初始化mgr
		HBaseMgr.init();
		
		// 添加表
		HBaseMgr.addTable(TaoSreachItem.class);
		
		for(int i = 3 ; i < 23 ; i ++){
			// 加载网页
			String html = HttpUtils.get("https://s.taobao.com/search?q=%E6%89%8B%E6%9C%BA&tab=all&promote=0&bcoffset=-3&ntoffset=-3&p4ppushleft=5%2C48&s=" + (i*48));
			
			L.info("===================== 加载网页成功  ");
			parse(html);
			L.info("============== 解析插入成功  i = " + i );
		}
		
	}
	


}
