//package xiaoa.java.spider;
//
//import java.io.File;
//import java.util.ArrayList;
//import java.util.Collections;
//import java.util.HashMap;
//import java.util.HashSet;
//import java.util.List;
//import java.util.Map;
//import java.util.Set;
//
//import org.apache.commons.io.FileUtils;
//
//import com.alibaba.fastjson.JSON;
//import com.alibaba.fastjson.JSONArray;
//import com.alibaba.fastjson.JSONObject;
//
//import jxl.Workbook;
//import jxl.write.Label;
//import jxl.write.WritableSheet;
//import jxl.write.WritableWorkbook;
//import xiaoa.java.utils.net.HttpUtils;
//
//public class TaoBaoAddress {
//	
//	
//	public static void doDown()throws Throwable{
//		
//		String json = FileUtils.readFileToString(new File("E:/Users/Administrator/git/java1.7Dome/src/xiaoa/java/spider/TaobaoAdressJson.json") , "utf-8");
//		
//		List<City>  cityList  = JSON.parseArray(json, City.class);
//		
//		Set<Long>   parentKeySet = new HashSet<>();
//		
//		Map<Long, City>  cityMap  = new HashMap<>();
//		
//		for (City city : cityList ){
//			parentKeySet.add(city.parentKey);
//			cityMap.put(city.key, city);
//		}
//		
//		// 临时保存城市信息
//		List<City>  tempCityList = new ArrayList<>();
//		
//		for (City city : cityList ){
//			
//			// 如果该城市没有子集
//			if (!parentKeySet.contains(city.key)){
//				
//				List<Long>  parentKeyList = new ArrayList<>(5);
//				Long  parentKey = city.parentKey;
//				parentKeyList.add(city.key);
//
//				while(true){
//					
//					// 找到父类城市对象
//					City parentCity  = cityMap.get(parentKey);
//					
//					if (parentCity != null){
//						parentKeyList.add(parentKey);
//						
//						parentKey = parentCity.parentKey;
//					}else{
//						break;
//					}
//					
//				}
//				
//				// 如果该城市有上级城市并且已经是最低级城市
//				if (parentKeyList != null && parentKeyList.size() > 0){
//
//					Collections.reverse(parentKeyList);
//					
//					StringBuilder lStr = new StringBuilder();
//					
//					lStr.append("l1=").append(parentKeyList.get(0));
//					
//					for (int ip =1  ; ip < parentKeyList.size()   ; ip ++){
//						lStr.append("&").append("l"+(ip+1)+"=").append(parentKeyList.get(ip));
//					}
//					
//					// 获取街道信息
//					String addrStr = HttpUtils.get("https://lsp.wuliu.taobao.com/locationservice/addr/output_address_town_array.do?" 
//										+ lStr.toString()
//										+"&lang=zh-S&_ksTS=1492001499319_7625&callback=jsonp7626");
//					
//					addrStr  = addrStr.substring(addrStr.indexOf("{"), addrStr.lastIndexOf("}")+1);
//
//					JSONObject addrJsonObj  = JSON.parseObject(addrStr); 
//					
//					JSONArray  addrlist = addrJsonObj.getJSONArray("result");
//					
//					for (int addrI = 0 ; addrI < addrlist.size() ; addrI ++ ){
//						
//						JSONArray addr = addrlist.getJSONArray(addrI);
//						
//						City newCity = new City();
//						
//						newCity.key       = Long.valueOf(addr.getString(0));
//						newCity.name      = addr.getString(1);
//						newCity.parentKey = city.key;
//						tempCityList.add(newCity);
//						
//						System.out.println(" cityName = " + city.name + "======= addrName = " + newCity.name );
//						
//					}
//				}
//				
//			}
//			
//		}
//		
//		
//		
//		cityList.addAll(tempCityList);
//		
//		// 写入到文件
//		String  dataJson = JSON.toJSONString(cityList);
//		
//		FileUtils.writeStringToFile(new File("e://city.json"), dataJson , "utf-8");
//		
//		System.out.println("================ 完成");
//		
//		
//	}
//	
//	/**
//     * 对象数据写入到Excel
//     */
//    public static void writeExcel(List<City> clist) {
//        WritableWorkbook book = null;
//        try {
//          // 打开文件
//          book = Workbook.createWorkbook(new File("e:/city.xls"));
//          // 生成名为"学生"的工作表，参数0表示这是第一页
//          WritableSheet sheet = book.createSheet("学生", 0);
//          //创建要显示的内容,创建一个单元格，第一个参数为列坐标，第二个参数为行坐标，第三个参数为内容
//          Label id = new Label(0,0,"id");
//          sheet.addCell(id);
//          Label name = new Label(1,0,"name");
//          sheet.addCell(name);
//          
//          Label parent_id = new Label(2,0,"parent_id");
//          sheet.addCell(parent_id);
//          Label level = new Label(3,0,"level");
//          sheet.addCell(level);
//          for(int i=0;i<clist.size();i++){
//          	  Label _id = new Label(0,i+1,clist.get(i).getKey()+"");
//            sheet.addCell(_id);
//            Label _name = new Label(1,i+1,clist.get(i).getName());
//            sheet.addCell(_name);
//            Label _parent_id = new Label(2,i+1,clist.get(i).getParentKey()+"");
//            sheet.addCell(_parent_id);
//          
//          }
//          //把创建的内容写入到输出流中，并关闭输出流
//          book.write();
//        } catch (Exception e) {
//            System.out.println(e);
//        }finally{
//            if(book!=null){
//                try {
//                    book.close();
//                } catch (Exception e) {
//                    e.printStackTrace();
//                } 
//            }
//        }
//    }
//
//	
//	public static void main(String[] args)throws Throwable {
//		
//		String json = FileUtils.readFileToString(new File("e://city.json") , "utf-8");
//		
//		List<City>  cityList = JSON.parseArray(json, City.class);
//		
//		writeExcel(cityList);
//		
//		System.out.println("完成");
//		//doDown();
//		
//	}
//	
//	
//	/**
//	 * 城市类
//	 * @author xiaoa
//	 * @date 2017年4月12日 下午9:05:43
//	 * @version V1.0
//	 *
//	 */
//	public static class City{
//		
//		public String  name;
//		public String  fanName;
//		public Long key;
//		
//		public Long parentKey;
//		
//		public Long depth;
//
//		public String getName() {
//			return name;
//		}
//
//		public void setName(String name) {
//			this.name = name;
//		}
//
//		public String getFanName() {
//			return fanName;
//		}
//
//		public void setFanName(String fanName) {
//			this.fanName = fanName;
//		}
//
//		public Long getKey() {
//			return key;
//		}
//
//		public void setKey(Long key) {
//			this.key = key;
//		}
//
//		public Long getParentKey() {
//			return parentKey;
//		}
//
//		public void setParentKey(Long parentKey) {
//			this.parentKey = parentKey;
//		}
//
//		public Long getDepth() {
//			return depth;
//		}
//
//		public void setDepth(Long depth) {
//			this.depth = depth;
//		}
//		
//		
//		
//	}
//	
//
//}
