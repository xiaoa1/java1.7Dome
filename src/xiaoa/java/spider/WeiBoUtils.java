package xiaoa.java.spider;

import java.net.URL;

import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.NicelyResynchronizingAjaxController;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.WebRequest;
import com.gargoylesoftware.htmlunit.html.HtmlForm;
import com.gargoylesoftware.htmlunit.html.HtmlInput;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.html.HtmlSubmitInput;

public class WeiBoUtils {
	
	static WebClient client = new WebClient(BrowserVersion.CHROME);
	
	public static HtmlPage login (String  userName , String passwd)throws Throwable{
		
        client.getOptions().setJavaScriptEnabled(true);    //默认执行js，如果不执行js，则可能会登录失败，因为用户名密码框需要js来绘制。
        client.getOptions().setCssEnabled(false);
        client.setAjaxController(new NicelyResynchronizingAjaxController());
        client.getOptions().setThrowExceptionOnScriptError(false); 
        client.getOptions().setTimeout(20000);  
        client.waitForBackgroundJavaScript(10000);
       
        WebRequest webRequest = new WebRequest(new URL("http://login.sina.com.cn/sso/login.php?client=ssologin.js(v1.3.16)"));
		webRequest.setCharset("gbk");
		HtmlPage page = (HtmlPage) client.getPage(webRequest);
        HtmlForm form = page.getFormByName("vForm");
        //登录
        HtmlInput ln = page.getHtmlElementById("username");
        HtmlInput pwd = page.getHtmlElementById("password");
        HtmlSubmitInput submitInput = form.getInputByValue("登 录");
        ln.setAttribute("value", "13648445540");
        pwd.setAttribute("value", "1347562511");
        
        submitInput.click();
    	 //登录完成
        System.out.println("登录成功\n\n\n");
        
        return page;
        
	}
	
	public static void main(String[] args)throws Throwable {
		
		login("","");
		
	}


}
