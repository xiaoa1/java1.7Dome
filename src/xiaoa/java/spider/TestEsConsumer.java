package xiaoa.java.spider;

import java.util.Map;


import com.alibaba.fastjson.JSON;
import com.rabbitmq.client.QueueingConsumer.Delivery;

import xiaoa.java.es.client.ClientUtils;
import xiaoa.java.jms.rabbitMq.ConsumerQueue;
import xiaoa.java.log.L;
import xiaoa.java.spider.bean.EsMqHtmlBean;

/**
 * es 消费者
 * @author xiaoa
 * @date 2017年10月29日 下午2:55:24
 * @version V1.0
 *
 */
public class TestEsConsumer {
	
	public static void main(String[] args) throws Throwable{
		
		TestEsSys.initSys();
		
		L.info("初始化系统成功");
		
		ConsumerQueue  consumerQueue = new ConsumerQueue("fetchNode");
		
		ConsumerQueue.Listen  listen  = new ConsumerQueue.Listen(){

			@Override
			public Object doWord(Delivery delivery, Map<String, String> logMap) throws Throwable {
				
				String json = new String(delivery.getBody());
				

				// 获取list
				EsMqHtmlBean  data =  JSON.parseObject(json, EsMqHtmlBean.class);

				// 获取id
				String id = data.getId() != null ? data.getId() : data.getData().getId();
				
				ClientUtils.addDocToCache(data.getIndex(), data.getType(), id, data.getData().toEsJSON());
				
				return data;
			}
			
		};
		
		int ts = 20;
		
		if (args != null && args.length > 0){
			ts = Integer.valueOf(args[0]);
		}
		
		for (int i = 0 ; i < ts ; i ++){
			 
			consumerQueue.addListen(i + "", listen);
		}
		
		System.out.println("监听成功");
		
		
	}


}
