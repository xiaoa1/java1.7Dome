package xiaoa.java.spider.db.vo;

import java.util.Date;
import javax.persistence.Id;

import com.alibaba.fastjson.JSON;

/**
 * 基础类
 * @author xiaoa
 * @date 2017年10月7日 下午2:35:49
 * @version V1.0
 *
 */
public class VoBase {
	
	/**
	 * 数据id
	 */
	@Id
	private String  id;
	
	/**
	 * 创建时间
	 */
	private Date createTime;
	
	/**
	 * 修改时间
	 */
	private Date updateTime;
	
	
	/**
	 * 是否删除
	 */
	
	public static final int DELETE_DEL   = 0;
	public static final int DELETE_NOMAL = 1;
	private Integer delete;

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public Integer getDelete() {
		return delete;
	}
	public void setDelete(Integer delete) {
		this.delete = delete;
	}
	
	
	public Date getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	@Override
	public String toString() {
		return JSON.toJSONString(this);
	}
	

}
