package xiaoa.java.spider.db.vo;

/**
 * 抓取url
 * @author xiaoa
 * @date 2017年10月2日 上午9:52:06
 * @version V1.0
 *
 */
public class FetchUrl extends VoBase {
	
	/**
	 * urkl
	 */
	private String url;
	
	/**
	 * 父url
	 */
	private String parent;
	
	// 下个批次
	public final static int STATE_NEXT = 0; // 下个批 次
	public final static int STATE_WAIT = 1; // 待处理
	public final static int STATE_IN = 2; // 处理中
	public final static int STATE_SUCC = 3; // 处理成功
	public final static int STATE_FAIL = 4; // 处理失败
	private Integer state; 
	
	/**
	 * 处理信息
	 */
	private String message;
	
	/**
	 * 深度
	 */
	private Integer depth =  0;
	
	
	/**
	 * 处理时间  毫秒
	 */
	private Integer useTime;

	
	

	public String getParent() {
		return parent;
	}


	public void setParent(String parent) {
		this.parent = parent;
	}


	public int getDepth() {
		return depth;
	}


	public void setDepth(int depth) {
		this.depth = depth;
	}


	public String getUrl() {
		return url;
	}


	public void setUrl(String url) {
		this.url = url;
	}


	public Integer getState() {
		return state;
	}


	public void setState(Integer state) {
		this.state = state;
	}


	public String getMessage() {
		return message;
	}


	public void setMessage(String message) {
		this.message = message;
	}


	public Integer getUseTime() {
		return useTime;
	}


	public void setUseTime(Integer useTime) {
		this.useTime = useTime;
	}
	
	
	
	
	

}
