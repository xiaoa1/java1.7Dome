package xiaoa.java.spider.fetch;

import java.io.File;
import java.net.URLEncoder;

import org.apache.commons.io.FileUtils;

import xiaoa.java.log.L;
import xiaoa.java.spider.parse.TaoGoodSearchParse;
import xiaoa.java.utils.ObjectUtis;
import xiaoa.java.utils.net.HttpUtils;

/**
 * 抓取淘宝列表
 * @author xiaoa
 * @date 2017年1月10日 下午2:16:27
 * @version V1.0
 *
 */
public class TaoGoodSearchFetch {
	
	/**
	 * 页码
	 * @Title: fetch
	 * @param offset
	 * @author xiaoa
	 */
	public static String fetch( String sreachName , long sleepTime ,  int offset) throws Throwable{
		
		if(offset < 0 || sreachName == null  || sreachName.equals("")){
			throw new RuntimeException("参数有误");
		}
		
		if(sleepTime > 0){
			// 开始休眠	
			Thread.sleep(sleepTime);
		}
		sreachName   = ObjectUtis.trim(sreachName);
		
		// 拼接 url
		//String url  = "https://s.taobao.com/search?q=" + URLEncoder.encode(sreachName , "utf-8")  + "&imgfile=&js=1&stats_click=search_radio_all%3A1&initiative_id=staobaoz_20170117&ie=utf8&p4ppushleft=5%2C48&s=" + (offset*48);  // 2017 -1 - 17
		String url  = "https://s.taobao.com/search?q=" + URLEncoder.encode(sreachName , "utf-8")  + "&tab=all&promote=0&bcoffset=-3&ntoffset=-3&p4ppushleft=5%2C48&s=" + (offset*48);  //  2016-12-12
		
		L.info("================ TaoGoodSearchFetch.fetch sreachName = " + sreachName + "  offset = " + offset+ "  url " + url);
		
		String html  = null;
		
		for(int i = 0 ; i < 10 ; i ++ ){
			try {
				 // 加载网页
				 html = HttpUtils.get(url);
				 
				 // 如果加载到网页
				 if(html != null && !html.trim().equals(""))
				       break;
			} catch (Exception e) {
				L.info("================================  重试 : " + i + " url =  " + url );
			}
			
		}
		
		
		return html;
		
	}
	
	
	/**
	 * 抓取
	 * @Title: fetch
	 * @param sreachName
	 * @param offset
	 * @throws Throwable
	 * @author xiaoa
	 */
	public static String fetch( String sreachName  ,  int offset) throws Throwable{
		
	    // 设置默认休眠时间
		return fetch(sreachName, 500l, offset);
	}
	
	public static void main(String[] args)throws Throwable {
		
		String html = fetch("手机", 0);
		
		TaoGoodSearchParse.parse(html);
	
		FileUtils.writeStringToFile(new  File("e://淘宝.txt"), html, "utf-8");
		
	}
	
	
	

}
