package xiaoa.java.spider;

import java.net.URI;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSON;
import com.rabbitmq.client.QueueingConsumer.Delivery;

import xiaoa.java.es.client.ClientUtils;
import xiaoa.java.jms.rabbitMq.ConsumerQueue;
import xiaoa.java.log.L;
import xiaoa.java.spider.bean.EsMqHtmlBean;
import xiaoa.java.spider.bean.EsMqImglBean;
import xiaoa.java.spider.bean.ImgBean;
import xiaoa.java.spider.parse.ParseHtml;

/**
 * es 消费者
 * @author xiaoa
 * @date 2017年10月29日 下午2:55:24
 * @version V1.0
 *
 */
public class TestEsConsumerForImg {
	
	public static void main(String[] args) throws Throwable{
		
		TestEsSys.initSys();
		
		L.info("初始化系统成功");
		
		ConsumerQueue  consumerQueue = new ConsumerQueue("fetchNode_img");
		
		ConsumerQueue.Listen  listen  = new ConsumerQueue.Listen(){

			@Override
			public Object doWord(Delivery delivery, Map<String, String> logMap) throws Throwable {
				
				String json = new String(delivery.getBody());

				// 获取list
				EsMqHtmlBean  data =  JSON.parseObject(json, EsMqHtmlBean.class);
				
				if (data == null){
					logMap.put("error", " data is null");
					return null;
				}
				
				if (data.getData().getHtml() == null || data.getData().getHtml().isEmpty()){
					logMap.put("error", " getData().getHtml() is null");
					return null;
				}

				
				List<EsMqImglBean>  resultList = new ArrayList<>();
				
				// 获取图片链接
				List<String>  imgs =  ParseHtml.getImgs(data.getData().getHtml(), URI.create(data.getData().getUrl()));
				
				for (String img :  imgs){
					
					ImgBean  imgData = new ImgBean();
					imgData.setIndexTime(new Date(System.currentTimeMillis()));
					imgData.setId(System.nanoTime() +"");
					
					// 设置图片
					imgData.setImgUrl(img);
					imgData.setTitle(data.getData().getTitle());
					imgData.setWebUrl(data.getData().getUrl());
					
					//  图片类
					EsMqImglBean  imgBean = new EsMqImglBean();
					imgBean.setIndex("web");
					imgBean.setType("img");
					
					// 设置索引
					imgBean.setData(imgData);
					imgBean.setId(data.getId());
					
					// 添加到返回的集合中
					resultList.add(imgBean);
					
					// 获取id
					String id = imgBean.getId() != null ? imgBean.getId() : imgBean.getData().getId();
					
					// 写入es
					ClientUtils.addDocToCache(imgBean.getIndex(), imgBean.getType(), id, imgBean.getData().toEsJSON());
				}
				
				return resultList;
			}
			
		};
		
		consumerQueue.addListen("1", listen);
		consumerQueue.addListen("2", listen);
		consumerQueue.addListen("3", listen);
		consumerQueue.addListen("4", listen);
		consumerQueue.addListen("5", listen);
		consumerQueue.addListen("6", listen);

		
		System.out.println("监听成功");

	}


}
