package xiaoa.java.spider.TeskHandle;



import java.net.URL;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.BlockingQueue;

import xiaoa.java.log.L;
import xiaoa.java.spider.Url;
import xiaoa.java.spider.parse.ParseHtml;
import xiaoa.java.spider.utils.SpiderFileUtils;
import xiaoa.java.utils.net.HttpUtils;

/**
 * 
 * @author lkc
 * @date 2016年11月5日 下午3:10:35
 * @version V1.0
 *
 */
public class HandlerDome implements Runnable {
	
	// 已经处理过url
	private Set<String> treatedUrl = new HashSet<String>();
	
	
	// 需要一个阻塞队列
	private BlockingQueue<Url>  queue;
	
	// 深度
	private int      maxDepth    =  10;
	
	
	public HandlerDome(BlockingQueue<Url> queue ) {
		this.queue  = queue;
		
	}

	@Override
	public void run() {
		
	boolean bu = true;	
		
	while (bu) {
		
		try {
			
			// 获取一个url
			Url  url = queue.poll();
			
			if(url == null || url.depth >= maxDepth || treatedUrl.contains(url.url.getHost() + url.url.getPath())){
				continue ;
			}
			
			// 获取网络资源
			
			// 获取html内容
			String  htmlBody  = HttpUtils.get(url.url);
			
			// 如果网页内容为空
			if(htmlBody == null || htmlBody.trim().equals("")){
				continue ;
			}
			
			
//			System.out.println(htmlBody);
			
			// 获取url链接
			List<String>  urlList  = ParseHtml.getUrls(htmlBody ,url.url.toURI() );
			
			for(String urlStr : urlList){
				
				URL  url_ = new URL(urlStr);
				
				Url  queueUrl  = new Url();
				queueUrl.depth = url.depth + 1;
				queueUrl.url   = url_;
				queueUrl.parent= url;
				
				// 添加到队列中
				queue.add(queueUrl);
			}
			
			// 保存网页内容
			SpiderFileUtils.write(htmlBody, SpiderFileUtils.getFilePath(url.url),  SpiderFileUtils.getFileName(url.url));
			
			
			
			
			// 将网页保存到已保存set集合中
			treatedUrl.add(url.url.getHost() + url.url.getPath());
			
			
		} catch (Throwable e) {
			e.printStackTrace();
		}
		
	}
	
	L.info("============== 线程死亡 threadName = " + Thread.currentThread().getName() + " bu = " + bu);
		
	}
	

}
