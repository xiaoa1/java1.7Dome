package xiaoa.java.spider;

import java.net.URL;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import xiaoa.java.spider.TeskHandle.HandlerDome;
import xiaoa.java.spider.pool.SpiderPool;

public class Test {
	
	public static void main(String[] args) throws Throwable {
		
		// 创建一个阻塞队列
		BlockingQueue<Url>  queue = new LinkedBlockingQueue<Url>(Integer.MAX_VALUE);
		
		
		// 队列中添加第一个链接
		Url  mainUrl = new Url();
		mainUrl.url = new URL("https://www.2345.com/");
		queue.add(mainUrl);
		
		HandlerDome handler = new HandlerDome(queue);
		
		// 创建一个线程池处理
		SpiderPool  pool  = new SpiderPool(3, handler);
		
		// 启动线程池
		pool.start();
		
	}

}
