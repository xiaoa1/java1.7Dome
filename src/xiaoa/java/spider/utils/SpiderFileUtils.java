package xiaoa.java.spider.utils;

import java.io.File;
import java.net.URL;

import org.apache.commons.io.FileUtils;

/**
 * 
 * @author xiao
 * @date 2016年11月5日 下午4:19:08
 * @version V1.0
 *
 */
public class SpiderFileUtils {
	
	/***
	 * 获取网页存储路径
	 * @Title: getFilePath
	 * @param url
	 * @return
	 * @author lkc
	 */
	public static String getFilePath(URL url){
		if(url == null){
			return null;
		}
//		
//		// 获取网页path
//		String  host = url.getHost();
//		
//		// 获取网页path
//		String  path = url.getPath();
		// TODO 这里需要补充网页路径
		
		return "e://spider";
		
	}
	
	/**
	 * 获取文件名
	 * @Title: getFileName
	 * @param url
	 * @return
	 * @author lkc
	 */
	public static String getFileName(URL url){
		if(url == null){
			return null;
		}
//		
//		// 获取网页path
//		String  host = url.getHost();
//		
//		// 获取网页path
		String  path = url.getPath();
		// TODO 这里需要补充网页名字
		
		return path.replace("/", "_");
		
	}
	
	/**
	 * 写入文件
	 * @Title: write
	 * @param file
	 * @param path
	 * @param fileName
	 * @author xiaoa
	 */
	public static void write(String fileData , String filePath , String fileName )throws Throwable{
		if(fileData == null || filePath == null || fileName == null){
			throw new RuntimeException("参数有误！");
		}
		
		// 文件夹
		File  fileDirectory = new File(filePath);
		if(!fileDirectory.exists() || !fileDirectory.isDirectory()){
			throw new RuntimeException(filePath + " ：该文件夹不存在！");
		}
		
		// 创建文件
		File  file  = new File(filePath + File.separator + fileName);
		
		System.out.println("=================SpiderFileUtils.write  theandName = " + Thread.currentThread().getName());
		
		System.out.println("=================SpiderFileUtils.write  保存成功      fileName = " + (filePath + File.separator + fileName));
		
		if(fileName.equals("") || fileName.contains("\\")){
			throw new RuntimeException("文件名有误");
		}
		
		// 将数据写入到文件中
		FileUtils.writeStringToFile(file, fileData, "utf-8");
		
	}  


}
