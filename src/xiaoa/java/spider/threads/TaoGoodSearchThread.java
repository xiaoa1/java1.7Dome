package xiaoa.java.spider.threads;
import java.util.List;
import xiaoa.java.Hbase.mgr.HBaseMgr;
import xiaoa.java.log.L;
import xiaoa.java.spider.bean.TaoSreachItem;
import xiaoa.java.spider.fetch.TaoGoodSearchFetch;
import xiaoa.java.spider.parse.TaoGoodSearchParse;
import xiaoa.java.zk.ZookeeperMgr;
import xiaoa.java.zk.mgr.KeyMgr;

/**
 * 淘宝搜索抓取  线程
 * @author xiaoa
 * @date 2017年1月11日 下午7:34:42
 * @version V1.0
 *
 */
public class TaoGoodSearchThread extends RunThread<Integer> {
	
	// 抓取名字
	public String sName;
	
	
	/**
	 * 
	 * 构造器
	 * <p>Title: </p>
	 * <p>Description: </p>
	 * @author xiaoa
	 * @param sName
	 * @param start
	 * @param end
	 * @throws Throwable
	 */
	public TaoGoodSearchThread()throws Throwable{
		
	}
	
	/**
	 * 设置抓取条件
	 * @Title: set
	 * @param sName
	 * @param start
	 * @param end
	 * @author xiaoa
	 */
	public void setType( String sName , int start , int end ){
		
		// 判断参数
		if(sName == null || sName.trim().isEmpty() || start > end || start < 0){
			throw new RuntimeException("参数有误");
		}
		
		this.sName  = sName;
		
		// 初始化页数
		for( ; start <= end ; start ++ ){
			queue.add(start);
		}
				
	}
	
	/**
	 * 初始化表
	 * @Title: initTable
	 * @throws Throwable
	 * @author xiaoa
	 */
	private void initTable()throws Throwable{
		
		// 添加表
		HBaseMgr.addTable(TaoSreachItem.class);
	}
	
	
	@Override
	public void runThread() throws Throwable {
		// 开始运行
		while(!queue.isEmpty()){
			// 记录开始时间
			long  startTime  =  System.currentTimeMillis();
			// 获取一页
			Integer offset  =  queue.remove();  
			
			// 获取页面
			String html = TaoGoodSearchFetch.fetch(sName, 300l, offset);
			
			// 如果网页不为空
			if(html != null && !html.equals("")){
				
				// 解析页面
				List<TaoSreachItem> list  = TaoGoodSearchParse.parse(html);
				
				// 如果有 插入到数据库中
				if(list != null && list.size() > 0){
					HBaseMgr.put(list);
				}
			}
			
			L.info("================ TaoGoodSearchFetch.fetch  抓取完毕   sName = " + sName + "  offset = " + offset+ "  耗时 ：" + (System.currentTimeMillis() - startTime - 300)  + " 毫秒 " );
		}
	}
	
	/**
	 * 初始化 
	 */
	public void init()throws Throwable{
		
		// 初始化zk
		ZookeeperMgr.init("192.168.218.136:2181");
		L.info("================ 初始化 zk 成功 ");
		
		// 初始化key
		KeyMgr.init("hbaseDome");
		
		// 初始化mgr
		HBaseMgr.init();
		
		// 初始化表
		initTable();
		
	}
	
	
}
