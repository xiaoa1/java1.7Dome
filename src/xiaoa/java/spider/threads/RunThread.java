package xiaoa.java.spider.threads;

import java.util.ArrayList;

import java.util.Collections;
import java.util.List;

import org.eclipse.jetty.util.BlockingArrayQueue;

import xiaoa.java.log.L;

/**
 * 线程抽象类运行类 
 * @author xiaoa
 * @date 2017年1月22日 下午2:11:05
 * @version V1.0
 *
 */
public abstract class RunThread<T> implements Runnable{
	
	// 控制线程变量
	// 创建一个安全list集合
	private    List<String>  threadsState  =  Collections.synchronizedList(new  ArrayList<String>());

	// 线程总数量
     private  int threads  =  0;
	
	// 任务需要变量
	// 任务队列属性
    protected   BlockingArrayQueue<T>  queue  =  new   BlockingArrayQueue<T>();
		
    /**
     * 获取队列对象
     * @Title: getQueue
     * @return
     * @author xiaoa
     */
    public BlockingArrayQueue<T> getQueue(){
    	return queue;
    }
    
    ///   控制线程方法
 // 移除一个线程数量
	public synchronized void  poolThreads() {
		this.threads = threads -1 ;
	}

	/**
	 * 设置本次线程数量
	 * @Title: setThreads
	 * @param threads
	 * @author xiaoa
	 */
	public void setThreads(int threads) {
		
		if(!isRunAlive()){
			throw new RuntimeException("还有存活线程");
		}
		
		this.threads = threads;
	}


	/**
	 * 添加一个线程名字
	 * @Title: addThread
	 * @param threadName
	 * @throws Throwable
	 * @author xiaoa
	 */
	public  void  addThread(String threadName)throws Throwable{
		threadsState.add(threadName);
	}
	
	/**
	 * 移除一个线程
	 * @Title: addThread
	 * @param threadName
	 * @throws Throwable
	 * @author xiaoa
	 */
	public  void  removeThread(String threadName){
		
		// 移除一个线程
		poolThreads();
		// 移除一个线程
		threadsState.remove(threadName);
	}	
	
	/**
	 * 正在运行线程数量
	 * @Title: getRuns
	 * @return
	 * @author xiaoa
	 */
	public  int getRuns(){
		return threadsState.size();
	}
	
	/**
	 * 判断是否还有线程存活
	 * @Title: isAlive
	 * @return
	 * @author xiaoa
	 */
	public  boolean isRunAlive (){
		
		L.info("======================  isRunAlive   threads = " + threads + "   threadsState.size() = " + threadsState.size());
		// 判断是否还有线程
		return threadsState.size() == 0  && threads == 0; 
	}

	// 线程运行入口
    @Override
    public void run() {

		// 获取当前线程名字添加到list中
	   threadsState.add(Thread.currentThread().getName());
			
		try {
			
			// 运行线程
			runThread();
			
		} catch (Throwable e) {
			e.printStackTrace();
		}
		
		// 移除该线程
		removeThread(Thread.currentThread().getName());
    }
    
    /**
     * 线程实现类
     * @Title: runThread
     * @author xiaoa
     */
    public abstract void runThread()throws Throwable;
    
    /**
     * 线程初始化类
     */
    public abstract void init()throws Throwable;
    
    /**
     * 等待线程运行结束
     */
    public void waitThreads()throws Throwable {
    	  // 检查是否运行完毕
	    while(!isRunAlive()){
	    	
	    	L.info("==================  等待 执行完毕");
	        // 一秒检查一遍
	    	Thread.sleep(1000);
	    }
	}
}
