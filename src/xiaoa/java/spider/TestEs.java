package xiaoa.java.spider;

import java.io.File;

import java.util.Arrays;
import java.util.Date;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.commons.io.FileUtils;


import xiaoa.java.spider.TeskHandle.HandlerEsFetch;
import xiaoa.java.spider.db.vo.FetchUrl;
import xiaoa.java.spider.load.LoadRun;
import xiaoa.java.spider.pool.SpiderPool;
import xiaoa.java.spider.save.SaveRun;
import xiaoa.java.utils.time.DateFormatUtils;

public class TestEs {
	
	

	
	
	/**
	 * 主方法
	 * @Title: main
	 * @param args
	 * @throws Throwable
	 * @author xiaoa
	 */
	public static void main(String[] args) throws Throwable {
		
		TestEsSys.initSys();
		
	    BlockingQueue<FetchUrl>  inQueue  = new LinkedBlockingQueue<>(200);
		 
		BlockingQueue<FetchUrl>  outQueue  = new LinkedBlockingQueue<>(2000 );
		 
		HandlerEsFetch handler = new HandlerEsFetch(inQueue , outQueue);
		
		
		// 处理线程
		int threads = (args == null || args.length == 0 ? 15 : Integer.parseInt(args[0]));
		
		
		// 创建一个线程池处理
		SpiderPool  pool  = new SpiderPool(threads, handler);
		
		// 启动线程池
		pool.start();
		
		// 启动加载线程
		Runnable saveRun = new SaveRun(outQueue);
		
		for (int i =0 ; i < 10 ; i ++){
			new Thread(saveRun).start();
		}

		
		new Thread(new LoadRun<FetchUrl>(inQueue , FetchUrl.class)).start();
		
		File logFile = new File("fetch.log");
		
		System.out.println("logPath : " + logFile.getAbsolutePath());
		
		FileUtils.writeLines(logFile, Arrays.asList("时间：" + DateFormatUtils.format_yyyyMMddhhssSSS(new Date()) )  , false);
		
		while(true){
			
			String log = "时间：" + DateFormatUtils.format_yyyyMMddhhssSSS(new Date()) 
					       + " queueIn = " + inQueue.size()
					       + " queueOut = " + outQueue.size()
					       + " speedCount + " + handler.speed();
					       ; 
					
			FileUtils.writeLines(logFile, Arrays.asList(log) , true);
			
			Thread.sleep(1000);
		}
		
		
		
	}

}
