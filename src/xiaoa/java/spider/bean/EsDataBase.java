package xiaoa.java.spider.bean;

import java.util.Calendar;
import java.util.Date;

import com.alibaba.fastjson.JSON;

import xiaoa.java.utils.time.DateFormatUtils;

/**
 * es公共包
 * @author xiaoa
 * @date 2017年10月2日 上午1:47:59
 * @version V1.0
 *
 */
public class EsDataBase extends BaseBean implements Cloneable {
	
	/**
	 * 数据id
	 */
	private String id ;
	
	/**
	 * 发布时间
	 */
	private Date pushTime;
	
	/**
	 * 索引时间
	 */
	private Date indexTime;
	
	/**
	 * 索引时间
	 */
	private String indexTimeStr;
	
	/**
	 * 索引时间
	 */
	private String pushTimeStr;
	
	/**
	 * 发布月
	 */
	private int pushMonth;
	
	
	/**
	 * 发布天
	 */
	private int pushDate;
	
	
	/**
	 * 发布小时
	 */
	private int pushHour;


	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}

	public Date getPushTime() {
		return pushTime;
	}


	public Date getIndexTime() {
		return indexTime;
	}


	public void setIndexTime(Date indexTime) {
		if (indexTime != null){
			setIndexTimeStr(DateFormatUtils.format_yyyyMMddhhssSSS(indexTime));
		}else {
			setIndexTimeStr(null);
		}
		this.indexTime = indexTime;
	}


	
	
	

	public String getPushTimeStr() {
		return pushTimeStr;
	}


	public void setPushTimeStr(String pushTimeStr) {
		this.pushTimeStr = pushTimeStr;
	}


	public int getPushMonth() {
		return pushMonth;
	}


	public void setPushMonth(int pushMonth) {
		this.pushMonth = pushMonth;
	}


	public int getPushDate() {
		return pushDate;
	}


	public void setPushDate(int pushDate) {
		this.pushDate = pushDate;
	}


	public int getPushHour() {
		return pushHour;
	}


	public void setPushHour(int pushHour) {
		this.pushHour = pushHour;
	}


	public void setPushTime(Date pushTime) {
		
          if (pushTime != null){
			
			Calendar  ca = Calendar.getInstance();
			ca.setTime(pushTime);
			
			setPushMonth(ca.get(Calendar.MONTH) );
			setPushDate(ca.get(Calendar.DAY_OF_MONTH) );
			setPushHour(ca.get(Calendar.HOUR_OF_DAY) );
			setPushTimeStr(DateFormatUtils.format_yyyyMMddhhssSSS(pushTime));
		}
		
		
		this.pushTime = pushTime;
	}


	public String toEsJSON(){
		return JSON.toJSONString(this);
	}


	public String getIndexTimeStr() {
		return indexTimeStr;
	}


	public void setIndexTimeStr(String indexTimeStr) {
		this.indexTimeStr = indexTimeStr;
	}
	
	

}
