package xiaoa.java.spider.bean;

import java.math.BigDecimal;

import java.util.List;


import xiaoa.java.Hbase.annotations.HBaseColumn;
import xiaoa.java.Hbase.annotations.HBaseTable;
import xiaoa.java.Hbase.bean.HbaseBean;

/**
 * 淘宝搜索模型
 * @author xiaoa
 * @date 2017年1月6日 下午4:10:07
 * @version V1.0
 */
@HBaseTable
public class TaoSreachItem  extends HbaseBean{
	
	private static final long serialVersionUID = 1L;

	// 商品id
	@HBaseColumn
	public Long cat;
	
	// 标题
	@HBaseColumn
	public String title;
	
	// url
	@HBaseColumn
	public  String pic_url;
	
	// 价格
	@HBaseColumn
	public  BigDecimal price;
	
	// 标签
	@HBaseColumn
	public String specialTag;
	
	// importantKey
	@HBaseColumn
	public String importantKey;
	
	// 
	@HBaseColumn
	public String month_sales;
	
	@HBaseColumn
	public String url;
	
	@HBaseColumn
	public String cmt_count;
	
	@HBaseColumn
	public String cmt_count_url;
	
	// 所在分页
	@HBaseColumn
	public Integer offset;
	
	@HBaseColumn
	public String tags;
	
	
	//  商户信息
	@HBaseColumn(family ="seller")
	public String s_trace;
	
	@HBaseColumn(family ="seller")
	public String s_url;
	
	@HBaseColumn(family ="seller")
	public String s_num;
	
	
	// 商户信息
	public Seller  seller;
	
	// 标签
	public List<Tag>   tag_info;
	
	@Override
	public byte[] keyGen() {
		
		return key;

	}
	
	/**
	 * 商户信息
	 * @author xiaoa
	 * @date 2017年1月6日 下午4:05:41
	 * @version V1.0
	 *
	 */
	public static class Seller{
		
		public String trace;
		
		public String url;
		
		public String num;

		public String getTrace() {
			return trace;
		}

		public void setTrace(String trace) {
			this.trace = trace;
		}

		public String getUrl() {
			return url;
		}

		public void setUrl(String url) {
			this.url = url;
		}

		public String getNum() {
			return num;
		}

		public void setNum(String num) {
			this.num = num;
		}
		
		
		
	}
	
	/**
	 * 标签信息
	 * @author xiaoa
	 * @date 2017年1月6日 下午4:07:20
	 * @version V1.0
	 *
	 */
	public static class Tag{
		
		public String tag;

		public String getTag() {
			return tag;
		}

		public void setTag(String tag) {
			this.tag = tag;
		}
	}
	
	

	public Long getCat() {
		return cat;
	}

	public void setCat(Long cat) {
		this.cat = cat;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getPic_url() {
		return pic_url;
	}

	public void setPic_url(String pic_url) {
		this.pic_url = pic_url;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public String getSpecialTag() {
		return specialTag;
	}

	public void setSpecialTag(String specialTag) {
		this.specialTag = specialTag;
	}

	public String getImportantKey() {
		return importantKey;
	}

	public void setImportantKey(String importantKey) {
		this.importantKey = importantKey;
	}

	public String getMonth_sales() {
		return month_sales;
	}

	public void setMonth_sales(String month_sales) {
		this.month_sales = month_sales;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getCmt_count() {
		return cmt_count;
	}

	public void setCmt_count(String cmt_count) {
		this.cmt_count = cmt_count;
	}

	public String getCmt_count_url() {
		return cmt_count_url;
	}

	public void setCmt_count_url(String cmt_count_url) {
		this.cmt_count_url = cmt_count_url;
	}

	public Seller getSeller() {
		return seller;
	}

	public void setSeller(Seller seller) {
		this.seller = seller;
	}

	public Integer getOffset() {
		return offset;
	}

	public void setOffset(Integer offset) {
		this.offset = offset;
	}

	public List<Tag> getTag_info() {
		return tag_info;
	}

	public void setTag_info(List<Tag> tag_info) {
		this.tag_info = tag_info;
	}

}
