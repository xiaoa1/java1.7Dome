package xiaoa.java.spider.bean;

/**
 * html数据
 * @author xiaoa
 * @date 2017年11月1日 下午9:54:07
 * @version V1.0
 *
 */
public class EsMqImglBean extends EsMqBean {
	
	/**
	 *  数据
	 */
	private  ImgBean data;

	public ImgBean getData() {
		return data;
	}

	public void setData(ImgBean data) {
		this.data = data;
	}

	
	

}
