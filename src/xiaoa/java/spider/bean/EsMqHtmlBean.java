package xiaoa.java.spider.bean;

/**
 * html数据
 * @author xiaoa
 * @date 2017年11月1日 下午9:54:07
 * @version V1.0
 *
 */
public class EsMqHtmlBean extends EsMqBean {
	
	/**
	 *  数据
	 */
	private  HtmlBean data;

	public HtmlBean getData() {
		return data;
	}

	public void setData(HtmlBean data) {
		this.data = data;
	}

	
	

}
