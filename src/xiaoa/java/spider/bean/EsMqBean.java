package xiaoa.java.spider.bean;

/**
 * 
 * @author xiaoa
 * @date 2017年10月29日 上午10:00:46
 * @version V1.0
 *
 */
public class EsMqBean extends BaseBean implements Cloneable{
	
	/**
	 * 索引
	 */
	private  String index ;
	
	/**
	 * 类型
	 */
	private  String type ; 
	
	
	/**
	 * 类型
	 */
	private  String id ; 
	

	public String getIndex() {
		return index;
	}

	public void setIndex(String index) {
		this.index = index;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}



	
	
	
	

}
