package xiaoa.java.spider.bean;

/**
 * html
 * @author xiaoa
 * @date 2017年10月2日 上午1:43:06
 * @version V1.0
 *
 */
public class HtmlBean extends EsDataBase {
	
	/**
	 * url
	 */
	private String url;
	
	/**
	 * 标题加内容
	 */
	private String keys;
	
	/**
	 * 标题
	 */
	private String title ;
	
	/**
	 * 文本内容
	 */
	private String text;
	
	
	/**
	 * html原文
	 */
	private String html;


	public String getUrl() {
		return url;
	}


	public void setUrl(String url) {
		this.url = url;
	}


	public String getKeys() {
		return keys;
	}


	public void setKeys(String keys) {
		this.keys = keys;
	}


	public String getTitle() {
		return title;
	}


	public void setTitle(String title) {
		this.title = title;
	}


	public String getText() {
		return text;
	}


	public void setText(String text) {
		this.text = text;
	}


	public String getHtml() {
		return html;
	}


	public void setHtml(String html) {
		this.html = html;
	}
	
	

}
