package xiaoa.java.spider.bean;

/**
 * 图片
 * @author xiaoa
 * @date 2017年11月1日 下午9:55:07
 * @version V1.0
 *
 */
public class ImgBean extends EsDataBase{

	
	/**
	 * 网站url
	 */
	private String webUrl;
	
	/**
	 * 标题
	 */
	private String title;
	
	/**
	 * 图片路径
	 */
	private String imgUrl;

	
	
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getWebUrl() {
		return webUrl;
	}

	public void setWebUrl(String webUrl) {
		this.webUrl = webUrl;
	}

	public String getImgUrl() {
		return imgUrl;
	}

	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}
	
	
	
	
	
}
