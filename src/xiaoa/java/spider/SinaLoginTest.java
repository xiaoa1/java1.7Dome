package xiaoa.java.spider;

import java.io.FileOutputStream;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlInput;
import com.gargoylesoftware.htmlunit.html.HtmlPage;

import xiaoa.java.log.L;

public class SinaLoginTest {
	public static void main(String[] args) throws FailingHttpStatusCodeException, MalformedURLException, IOException, InterruptedException {
		try {
			
 			WebClient client = new WebClient();
	        client.getOptions().setJavaScriptEnabled(false);    //默认执行js，如果不执行js，则可能会登录失败，因为用户名密码框需要js来绘制。
	        client.getOptions().setCssEnabled(false);
//	        client.setAjaxController(new NicelyResynchronizingAjaxController());
//	        client.getOptions().setThrowExceptionOnScriptError(false); 
//	        client.getOptions().setTimeout(20000);  
//	        client.waitForBackgroundJavaScript(10000);

	        L.info("==============  初始化完成  打开模拟浏览器");
	        
//	        HtmlPage page = client.getPage("http://login.sina.com.cn/sso/login.php?client=ssologin.js(v1.3.16)");
	        HtmlPage page = client.getPage("http://www.baidu.com");
        
	        System.out.println("=============  页面数据" + page.asText());

	        //登录
	        HtmlInput ln = page.getHtmlElementById("username");
	        HtmlInput pwd = page.getHtmlElementById("password");
	     //   HtmlInput btn = page.getFirstByXPath(".//*[@id='vForm']/div[3]/ul/li[6]/div[2]/input");

//	        ln.setAttribute("value", "13648445540");
//	        pwd.setAttribute("value", "1347562511");

	        
	        ln.setAttribute("value", "新浪账号");
	        pwd.setAttribute("value", "密码");
	        
	     //   HtmlPage page2 = btn.click();
	        
	        System.out.println("登录成功");
	        
	        //登录完成
	        System.out.println("\n\n\n");
	        for(int i=0;i<88;i++){
	        	if(i%5==0){
	        		Thread.sleep(200);
	        	}
	        	 getImg(client,i);
	        	 if(i==88){
	        		 System.out.println("处理完成====");
	        	 }
	        }
	     //   client.closeAllWindows();
		} catch (Exception e) {
			// TODO: handle exception
		}
		
    }

	private static void getImg(WebClient client,int page) throws IOException,
			MalformedURLException {
		//HtmlPage page3 = client.getPage("http://d.weibo.com/1087030002_2976_2004_7?page="+page+"#Pl_Core_F4RightUserList__4");
		HtmlPage page3 = client.getPage("http://d.weibo.com/1087030002_2976_2004_4?page="+page+"#Pl_Core_F4RightUserList__4");

		Document doc = Jsoup.parse(page3.asXml());

		Elements accounts = doc.select("script");
		
		String tmp="";
		
		for(Element e:accounts){
			//String str="\"ns\":\"";
			if(e.html().contains("粉丝列表模块")){
				
				 tmp=e.html().substring(e.html().indexOf("\"html\":\""),e.html().indexOf("\"})")).replaceAll("\"html\":\"","").replaceAll("\\\\", "").replaceAll("\\\\/", "/");
			// }
		}
		Document doc2 = Jsoup.parse(tmp);
		
		Elements img = doc2.select(".mod_pic img");
		Elements imga = doc2.select(".mod_pic a");
		
		for(int i=0;i<img.size();i++){
			Element e1=img.get(i);
			e1.attr("alt",imga.get(i).attr("title"));
			String str=e1.attr("src").replaceAll(".50/", ".180/");
			String str2=e1.attr("alt");
//	        	e1.attr("width", "180");
//	        	e1.attr("height","180");
			System.out.println(str+"=="+str2+"=="+e1.attr("width"));
			download(str2,str);
		
		}
	
		}
	}
	 /*** 
	 * 下载图片 
	 *  
	 * @param listImgSrc 
	 */  
    private static void download(String fileName,String listImgSrc) {  
        try {  
            //for (String url : listImgSrc) {  
                String imageName = listImgSrc.substring(listImgSrc.lastIndexOf("/") + 1,listImgSrc.length());  
                URL uri = new URL(listImgSrc);  
                InputStream in = uri.openStream();  
                FileOutputStream fo = new FileOutputStream("D:\\image(bagua)\\"+fileName+".jpg");  
                byte[] buf = new byte[1024];  
                int length = 0;  
                System.out.println("开始下载:" + listImgSrc);  
                while ((length = in.read(buf, 0, buf.length)) != -1) {  
                    fo.write(buf, 0, length);  
                }  
                in.close();  
                fo.close();  
                System.out.println(imageName + "下载完成");  
            //}  
        } catch (Exception e) {  
            System.out.println("下载失败");  
        }  
    }  
}
