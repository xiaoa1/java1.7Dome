package xiaoa.java.spider;

import java.net.URL;

/**
 * url
 * @author xiaoa
 * @date 2016年11月5日 下午12:49:24
 * @version V1.0
 *
 */
public class Url {
	
	/**
	 * 父类
	 */
	public Url parent ;
	
	/**
	 *  url 资源
	 */
	public URL url = null; 
	
	/**
	 * 深度
	 */
	public int depth =  0;
	
	/**
	 * 是否读取过
	 */
	public boolean isUsed = false;

	public URL getUrl() {
		return url;
	}

	public void setUrl(URL url) {
		this.url = url;
	}

	public int getDepth() {
		return depth;
	}

	public void setDepth(int depth) {
		this.depth = depth;
	}

	public boolean isUsed() {
		return isUsed;
	}

	public void setUsed(boolean isUsed) {
		this.isUsed = isUsed;
	}

	public Url getParent() {
		return parent;
	}

	public void setParent(Url parent) {
		this.parent = parent;
	}
	
	
}
