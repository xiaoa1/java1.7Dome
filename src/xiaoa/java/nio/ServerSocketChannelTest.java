package xiaoa.java.nio;

import xiaoa.java.log.L;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.nio.channels.*;
import java.nio.channels.spi.AbstractSelector;
import java.util.*;


public class ServerSocketChannelTest {
	
	
	// 选择器
	private  static Selector select = null;
	
	// 服务器map
	private static Map<String , ServerSocketChannel>   serverMap = null; 
	
	// 客户端数量
	private  static volatile int  clientNumber = 0;
	
	
	public static void  init()throws Throwable{
		
		serverMap = new HashMap<String, ServerSocketChannel>();
		
		// 打开一个选择器
		select = AbstractSelector.open();
		
		List<Integer>  portList = new ArrayList<>();
		
		portList.add(8001);
		portList.add(8002);
		portList.add(8003);
		portList.add(8004);
		portList.add(8006);
		portList.add(8007);
		portList.add(8008);
		portList.add(8009);
		portList.add(8010);
		
		for (Integer port : portList ){
			ServerSocketChannel  server  = ServerSocketChannel.open();
			server.bind(new InetSocketAddress(port));
			
			String serverName = "server:" + port;
			
			// 注册到选择器
			server.configureBlocking(false);
		    server.register(select, SelectionKey.OP_ACCEPT , serverName);
			
			// 添加到map中
			serverMap.put(serverName, server);
			
		}
		
	}
	
	/**
	 * 监听
	 * @Title: accept
	 * @author xiaoa
	 */
	public static void accept()throws Throwable{
		
		if (select == null){
			
			throw new RuntimeException(" is no init ");
			
		}
		
		System.out.println("启动成功");
		
		while(true){
			
			int num = select.select();
			
			
			if (num > 0){
				
				L.info("num = " + num );

				
				Set<SelectionKey>  keySet = select.selectedKeys();
				
				for (SelectionKey key : keySet){
					
					
					// 如果是等待接受监听，，，，说明这个是服务端
					if (key.isAcceptable()){
						
						// 获取客户端
						ServerSocketChannel  client = (ServerSocketChannel) key.channel();
						
						SocketChannel clientServer =   client.accept();
						
						Socket  socket =  clientServer.socket();
						
						InputStream  input =  socket.getInputStream();
						
						BufferedReader  reader = new BufferedReader(new InputStreamReader(input));
						
						String line = null;
						
						StringBuilder  response = new StringBuilder();
						
						while((line = reader.readLine()) != null && !line.contains("\n")
								&&  !line.contains("Connection")){
							
							System.out.println(line);
							
							response.append(line);
						}
						
						
						socket.close();
					
						
					} else if (key.isReadable()){
						
						// 获取客户端
						SelectableChannel  client =  key.channel();
						
						System.out.println(client);
						
					}else if (key.isWritable()){
						
						// 获取客户端
						SelectableChannel  client =  key.channel();
						
						System.out.println(client);
						
					}else if (key.isConnectable()){
						
						// 获取isConnectable
						SelectableChannel  client =  key.channel();
						
						System.out.println(client);
						
					}
					
					
				}
				
				System.out.println(":-----------------------");

			}
			
			
		}
		
	}
	
	
	public static void main(String[] args)throws Throwable {
		
		init();
		
		accept();
		
	}
	
	
	

}
