package xiaoa.java.atomic;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;

public class Test {

	private String test1 = "" ;
	
	public  volatile int test2 = 100 ;
	
	AtomicIntegerFieldUpdater<Test>  update = AtomicIntegerFieldUpdater.newUpdater(Test.class, "test2");

	public static void main(String[] args) throws Throwable{
		
		
		long startTime = System.currentTimeMillis();
		final CountDownLatch  count = new CountDownLatch(300);
		
		final Test  test = new Test();
		Runnable  run = new Runnable() {
			public void run() {
				count.getCount();
				
				for (int i = 0 ; i < 100000 ; i ++){
					
					test.update.incrementAndGet(test);
//					System.out.println(test.update.incrementAndGet(test));;
					
//					synchronized (test.test1) {
//						test.test2++;

//					}
//					System.out.println();
				}
				
				count.countDown();
				
			}
		} ;
		
		for (int i = 0 ; i < 300 ; i ++){
			new Thread(run).start();
		}
		
		
		count.await();
		
		System.out.println("完成：" + test.test2 + "   use = " + (System.currentTimeMillis() - startTime));
		
		
		
//		System.out.println(test.update.incrementAndGet(test));;
//		System.out.println(test.update.incrementAndGet(test));;
//		System.out.println(test.update.incrementAndGet(test));;
//		System.out.println(test.update.incrementAndGet(test));;
//		System.out.println(test.update.incrementAndGet(test));;

		
		
	}
	
}
