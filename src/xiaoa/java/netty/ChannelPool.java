//package xiaoa.java.netty;
//
//import java.net.URI;
//import java.util.HashMap;
//import java.util.Map;
//
//import io.netty.bootstrap.Bootstrap;
//import io.netty.channel.Channel;
//import io.netty.channel.ChannelFuture;
//import io.netty.util.concurrent.GenericFutureListener;
//
///**
// * 通道工具类
// * @author xiaoa
// * @date 2017年8月18日 下午2:28:45
// * @version V1.0
// *
// */
//public class ChannelPool {
//	
//	/**
//	 * 所有通道map
//	 */
//	
//	
//	/**
//	 * 最大通道数量
//	 */
//	private int maxChannelNum = 10;
//	
//	/**
//	 * 获取端口
//	 * @Title: getPort
//	 * @param uri
//	 * @return
//	 * @author xiaoa
//	 */
//	private static int getPort(URI uri){
//		
//		int port = uri.getPort();
//
//		if (port == -1){
//			if(uri.toString().toLowerCase().startsWith("http:")){
//				port = 80;
//			}else{
//				port = 443;
//			}
//		}
//		
//		return port;
//	}
//	
//	/**
//	 * 获取一个  channel  不从连接池
//	 * @Title: getChannel
//	 * @param boot
//	 * @return
//	 * @author xiaoa
//	 */
//	public static Channel  getChannel(Bootstrap  boot , URI uri)throws Throwable{
//		
//		if (boot == null || uri == null || uri.toString().equals("")){
//			throw new RuntimeException("参数有误 boot = " + boot + "  uri = " + uri);
//		}
//
//		// 连接
//		ChannelFuture  future =  boot.connect(uri.getHost() , getPort(uri) ).addListener(new GenericFutureListener<ChannelFuture>() {
//
//			@Override
//			public void operationComplete(ChannelFuture p0) throws Exception {
//				
//				Log.debug(p0.channel().toString() + ":连接" + p0.isSuccess());
//			}
//		}).sync();
//		
//		if (!future.isSuccess()){
//			
//			// 关闭连接
//			future.channel().close();
//			
//			if (future.cause() != null){
//				throw future.cause();
//			}
//		}
//		
//		return future.channel();
//	} 
//	
//	
//	/**
//	 * 从连接池中获取链接
//	 * @Title: getChannelByPool
//	 * @param boot
//	 * @param uri
//	 * @return
//	 * @throws Throwable
//	 * @author xiaoa
//	 */
//	public  Channel  getChannelByPool(Bootstrap  boot , URI uri)throws Throwable{
//		
//		if (boot == null || uri == null || uri.toString().equals("")){
//			throw new RuntimeException("参数有误 boot = " + boot + "  uri = " + uri);
//		}
//		
//		
//		
//		
//		
//		int port = getPort(uri);
//		
//		String key = uri.getScheme() + "://"  + uri.getHost() + ":" + port;
//		
//		
//
//	}
//
//}
