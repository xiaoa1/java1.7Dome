package xiaoa.java.netty;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.http.HttpRequestDecoder;
import io.netty.handler.codec.http.HttpResponseEncoder;

/**
 * Created by Administrator on 2017/7/21.
 */
public class NettyServer {



    public static void main(String[] args) throws Throwable {


        // 创建一个管理器
        ServerBootstrap   server  = new ServerBootstrap();

        // 绑定一个group
        server.group(new NioEventLoopGroup());

        // 绑定通道
        server.channel(NioServerSocketChannel.class);

        server.childHandler(new ChannelInitializer<NioSocketChannel>() {

            @Override
            protected void initChannel(NioSocketChannel ch) throws Exception {

                // 设置请求解码

                ch.pipeline().addLast(new HttpRequestDecoder());

                // 设置输出编码
                ch.pipeline().addLast(new HttpResponseEncoder());

                // 添加自己处理器
                ch.pipeline().addLast(new ServerHandle());

            }
        });


        server.bind(8012).sync();

        System.out.print("启动成功");


    }


    }
