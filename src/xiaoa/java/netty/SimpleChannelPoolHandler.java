package xiaoa.java.netty;

import io.netty.channel.Channel;
import io.netty.channel.pool.ChannelPoolHandler;

public class SimpleChannelPoolHandler  implements ChannelPoolHandler{

	@Override
	public void channelReleased(Channel ch) throws Exception {
		
		Log.debug("channelReleased = " + ch);
		
	}

	@Override
	public void channelAcquired(Channel ch) throws Exception {
		
		Log.debug("channelAcquired = " + ch);
		
	}

	@Override
	public void channelCreated(Channel ch) throws Exception {
		
		Log.debug("channelCreated = " + ch);
		
	}

}
