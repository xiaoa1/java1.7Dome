package xiaoa.java.netty;

import java.io.UnsupportedEncodingException;
import java.util.LinkedHashMap;

/**
 * http 返回结果
 * @author xiaoa
 * @date 2017年7月25日 下午9:13:32
 * @version V1.0
 *
 */
public class HttpResponseResult extends Result {

	/**
	 * 返回头信息
	 */
	public LinkedHashMap<String, String>  handles = new LinkedHashMap<>();
	
	public int code;
	
	public byte[] body;
	
	/**
	 * 获取handleValue
	 * @Title: getHandle
	 * @param key
	 * @return
	 * @author xiaoa
	 */
	public String getHandleValue(String key , String defaultValue ){
		
		String value = defaultValue;
		
		if (handles.containsKey(key)){
			return handles.get(key);
		}
		
		// 不区分大小写寻找
		for(String i_k : handles.keySet()){
			if (i_k != null && !i_k.isEmpty() && i_k.toLowerCase().equals(key)){
				
				value = handles.get(i_k);
			}
			
		}
		
		return value;
		
	}
	
	/**
	 * 获取编码
	 * @Title: getCharset
	 * @return
	 * @author xiaoa
	 */
	public String getCharset(){
		
		//  字符串
		String charsetStr = getHandleValue("Content-Type", null);
		
		// 拆分
		String [] cs = null;
		if (charsetStr.contains(";")){
			cs = charsetStr.split(";");
		}else {
			cs = new String[]{charsetStr};
		}
		
		// 解析
		for (String s : cs){
			
			String[] ss = s.split("=");
		    
			if (ss.length == 2){
				
				if(ss[0].toLowerCase().equals("charset")){
					 return  ss[1];
				}
			}
			
		}
		
		return null;
	}
	
	
	/**
	 * 将body转为字符串
	 * @Title: getBodyToString
	 * @return
	 * @throws UnsupportedEncodingException
	 * @author xiaoa
	 */
	public String getBodyToString() throws UnsupportedEncodingException{
		
		if (body == null || body.length == 0){
			return null;
		}
		
		// 默认为utf-8编码
		String charset = getCharset();
		
		if (charset == null){
			charset = "utf-8";
		}
		
		return new String(body , charset);
		
	}

}
