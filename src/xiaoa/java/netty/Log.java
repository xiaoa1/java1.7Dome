package xiaoa.java.netty;

import java.io.PrintStream;
import java.util.Date;

import com.alibaba.fastjson.JSON;

import xiaoa.java.utils.time.DateFormatUtils;

/**
 * 日志工具类
 * @author xiaoa
 * @date 2017年8月20日 下午11:50:21
 * @version V1.0
 *
 */
public final class Log {
	
	
	/**
	 *  日志输出out
	 */
	private static PrintStream  out = System.out;

	private static boolean debug = false;
	
	public static PrintStream getOut() {
		return out;
	}

	public  static void setOut(PrintStream out) {
		Log.out = out;
	}
	
	
	/**
	 * 输出日志
	 * @Title: info
	 * @param obj
	 * @author xiaoa
	 */
	public static void debug(Object obj){
		
		if (obj != null && debug){
			print(obj);
		}
		
	}
	
	
	/**
	 * info
	 * @Title: info
	 * @param obj
	 * @author xiaoa
	 */
	public static void info(Object obj){
		print(obj);
	}
	
	public static void info(Object obj, long startTime , long endTime){
		print(obj);
	}
	
	
	private static void print(Object obj){
		out.println( DateFormatUtils.format_yyyyMMddhhssSSS(new Date()) + ":" + JSON.toJSONString(obj));
	}
	
	
	
	public static void setDebug(boolean debug){
		
		Log.debug = debug;
		
	}
	
	public static boolean isDebug(){
		
		return debug;
	}
	
	
	
	
	

}
