package xiaoa.java.hadoop1.mapperReduce;

import java.io.IOException;

import java.util.Iterator;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

/**
 * 淘宝商品抓取reduce
 * @author xiaoa
 * @date 2017年1月12日 下午5:05:06
 * @version V1.0
 *
 */
public class TaoGoodSearchReduce  extends Reducer<Text, Text, Text, Text>{

	
	/**
	 * reduce
	 */
	@Override
	protected void reduce(Text key, Iterable<Text> values, Reducer<Text, Text, Text, Text>.Context resp)
			throws IOException, InterruptedException {
		// 循环填充key
		
		       Iterator<Text>  it  =  values.iterator();
				while(it.hasNext()){
					
					// 返回数据
					resp.write(key, it.next());
				}
	}


}
