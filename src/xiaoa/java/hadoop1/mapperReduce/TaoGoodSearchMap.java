package xiaoa.java.hadoop1.mapperReduce;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import xiaoa.java.log.L;
import xiaoa.java.spider.threads.TaoGoodSearchThread;

/**
 * 抓取map
 * @author xiaoa
 * @date 2017年1月12日 上午11:53:38
 * @version V1.0
 *
 */
public class TaoGoodSearchMap extends Mapper<LongWritable, Text, Text, Text> {
	
	// 创建一个抓取线程
    TaoGoodSearchThread  run = null;
    
    
    public TaoGoodSearchMap() {
    	// 初始化线程
		try {
			L.info("============= TaoGoodSearchMap.TaoGoodSearchMap   正在初始化 ");
			initRun();
			L.info("============= TaoGoodSearchMap.TaoGoodSearchMap   初始化成功 ");

		} catch (Throwable e) {
			e.printStackTrace();
		}
    }
	
	/**
	 * 初始化系统
	 * @Title: initRun
	 * @throws Throwable
	 * @author xiaoa
	 */
	public void initRun()throws Throwable{
		
		// 初始化配置文件
		if(run == null ){
			run =  new TaoGoodSearchThread();
			
			//初始化线程
			run.init();
		}
	}

	
	/**
	 * mapper  运行
	 */
	@Override
	protected void map(LongWritable key, Text line, Mapper<LongWritable, Text, Text, Text>.Context response)
			throws IOException, InterruptedException {

		try {
			L.info("===========================  key = " + key + "  line = " + line);
			
			// 初始化线程
			initRun();
			
			if( line != null ){
				
				String[] valus = line.toString().split("_");
				
				// 获取任务开始时间
				long startTime =  System.currentTimeMillis(); 
				
				// 获取名字
				String name = valus[0];
				
				// 获取分页
				String[]  strs = valus[1].toString().split(",");
				
				int start = Integer.valueOf(strs[0]);   // 开始页
				int end   = Integer.valueOf(strs[1]);   // 结束页
				
				// 设置抓取条件
				run.setType(name, start, end);
				
				// 线程数量
				int threads   =  10;
				
				// 如果指定了抓取线程数量
				if(strs.length >= 3){
					threads  = Integer.valueOf(strs[2]);
				}
			     
			     L.info("=========  开始创建线程");
				
			     // 设置本次线程运行数量
			     run.setThreads(threads);
			     
			     for(int  i = 0 ; i < threads ; i ++ ){
			    	 
			    	 // 创建一个线程
			    	 Thread  thread  = new  Thread(run);
			    	 thread.setName(name + "_" + i);
			    	 thread.start();
			     }
				
			    L.info("================ 创建线程完毕  run.isRunAlive() = " + run.isRunAlive());
				
			    // 等待线程运行结束
			    run.waitThreads();
			    
			    L.info("=======================   线程执行完毕   name =  " + name + "  耗时 ： " + (System.currentTimeMillis() - startTime) );
			    
			    // 返回数据
			    Text  respName    = new Text(name);
			    Text  respValue   = new Text(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()) + "_succ");
			     
			    // 填充到返回对象
			    response.write(respName,respValue );
			}
		
		
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}
	

}
