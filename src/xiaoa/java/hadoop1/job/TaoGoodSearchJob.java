package xiaoa.java.hadoop1.job;


import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import xiaoa.java.hadoop1.mapperReduce.TaoGoodSearchMap;
import xiaoa.java.hadoop1.mapperReduce.TaoGoodSearchReduce;
import xiaoa.java.log.L;

/**
 * 淘宝搜索运行 job
 * @author xiaoa
 * @date 2017年1月13日 下午2:20:31
 * @version V1.0
 *
 */
public class TaoGoodSearchJob implements Tool{

	// 配置
	private  Configuration  conf  =  null;
	 
	@Override
	public Configuration getConf() {

		return conf == null ?  (conf  = new Configuration()) : conf;
	}

	@Override
	public void setConf(Configuration conf) {
		this.conf  = conf;
	}

	@Override
	public int run(String[] args) throws Exception {
		
		
		if(args == null || args.length != 2){
			throw new RuntimeException("参数有误");
		}
		
		
		// 创建一个配置文件
		Configuration  config  = getConf();
		
		// 创建一个job
		Job  job  = Job.getInstance(config);
		
		// 设置jar
		job.setJarByClass(TaoGoodSearchJob.class);
		
		// 设置输入类型
		job.setMapOutputKeyClass(Text.class);
		job.setMapOutputValueClass(Text.class);
		
		// 设置输出类型
		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(Text.class);
		
		// 设置处理器
		job.setMapperClass(TaoGoodSearchMap.class);
		job.setReducerClass(TaoGoodSearchReduce.class);
		
		
		// 输入输出目录
		Path  inputDir  =  new Path(args[0]);
		
		Path  outDir    =  new Path(args[1]);
		
		FileInputFormat.setInputPaths(job, inputDir);
		FileOutputFormat.setOutputPath(job, outDir);
		
		// 运行 job
		boolean bu  = job.waitForCompletion(true);
		
		if(bu){
			L.info("==============  job 运行成功");
		}
		
		return 0;
	}
	
	
	public static void main(String[] args) throws Throwable {
		
		
		// 创建一个运行job
		TaoGoodSearchJob  runJob  = new TaoGoodSearchJob();
		
		// 添加配置文件
		
		Configuration  conf  = new Configuration();
		conf.addResource("xiaoa/java/hadoop1/job/core-site.xml");
		conf.addResource("xiaoa/java/hadoop1/job/hdfs-site.xml");
		
		System.out.println("=============" + conf.get("dfs.permissions"));
		System.out.println("=============" + conf.get("hadoop.tmp.dir"));
		System.out.println("=============" + conf.get("mapreduce.jobtracker.staging.root.dir"));

		
		runJob.setConf(conf);
		
		// 创建一个运行工具
		
		ToolRunner.run(runJob, new String[]{"e://.txt" , "e://output/" + (new SimpleDateFormat("yyyyMMddhhmmss").format(new Date()))});
		
	}

}
