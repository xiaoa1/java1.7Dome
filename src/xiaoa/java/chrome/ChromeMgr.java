package xiaoa.java.chrome;

import java.net.URI;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import xiaoa.java.log.L;
import xiaoa.java.utils.net.MyHttpUtils;

/**
 * 模拟浏览器
 * @author xiaoa
 * @date 2017年1月5日 下午5:51:37
 * @version V1.0
 *
 */
public class ChromeMgr {
	
	
	//  设置属性
	public static  final String CHROMEDRIVER_PATH  =   "webdriver.chrome.driver";
	
    // 静态Driver
	private static  WebDriver   web           =   null;
	
	
	/**
	 * 初始化管理器
	 * @Title: init
	 * @param driverPath
	 * @throws Throwable
	 * @author xiaoa
	 */
	public static  void init(String driverPath)throws Throwable{
		
		if(driverPath == null || driverPath.trim().equals("")){
			throw new RuntimeException("driverPath 参数有误");
		}
		//  初始化驱动位置
		System.setProperty(CHROMEDRIVER_PATH, driverPath);
		
		// 初始化驱动
		web  = new ChromeDriver();
	}
	
	
	/**
	 * 获取网页资源
	 * @Title: get
	 * @param url
	 * @return
	 * @throws Throwable
	 * @author xiaoa
	 */
	public static String get(String url )throws Throwable{
		
		if(url == null || url.trim().equals("")){
			return null;
		}
		
		// 加载网页
		web.get(url);
		
		// 获取网页资源
		return web.getPageSource();
	}
	
	/**
	 * 
	 * @Title: getBody
	 * @param url
	 * @return
	 * @throws Throwable
	 * @author xiaoa
	 */
	public static String doLogin(String url)throws Throwable{
		
		if(url == null || url.trim().equals("")){
			throw new RuntimeException(" url 不合法");
		}
		
		// 
		String  html_  =  "";
		
		if( web == null ){
			throw new RuntimeException("not init ");
		}
		
		//  加载网页
		web.get(url);
		
	    html_ = web.getPageSource();
		L.info("================  未登录 html = " + html_);

		
		// 获取网页资源
	    html_ = web.getPageSource();
		
		//  获取用户名
		WebElement   userName  =  web.findElement(By.className("user_name"));
		
		if(userName == null){
			throw new RuntimeException("  user_name 元素不存在");
		}
		userName.clear();
		userName.sendKeys("ljy");
		
		// 获取密码
		WebElement   passWord  =  web.findElement(By.className("passWord"));

		if(passWord == null){
			throw new RuntimeException("  user_name 元素不存在");
		}
		
		passWord.clear();
		passWord.sendKeys("123456");
		
		// 获取提交按钮
		WebElement   submit  =  web.findElement(By.id("sub"));
		
		if(submit == null){
			throw new RuntimeException("  submit 元素不存在");
		}
		
		submit.click();

		 //10s用于输入验证码
        Thread.sleep(100000);
        
		// 获取网页资源
	     html_ = web.getPageSource();
		
		L.info("================ 已登录 html = " + html_);
		
		// 获取cookie
		web.manage().getCookies();
		
		return html_;
		
	}
	
	
	public static void main(String[] args) throws Throwable {
		
		// 初始化mgr
		ChromeMgr.init("chromedriver_x64.exe");
//	
//		// 测试登录
//		String html = ChromeMgr.doLogin("http://back.dagolfla.com/back-stage/login.html");
//		
//		L.info("====================== return html = " + html);
		
		
		//  测试加载网页
		String html = ChromeMgr.get("https://s.taobao.com/search?q=%E6%B0%B4%E6%9D%AF&tab=all&promote=0&bcoffset=-3&s=44&ntoffset=-3");
		
		L.info("====================  浏览器加载出来的网页  html = " + html);
		
		// httpClient 加载出来的网页
		
		MyHttpUtils.doGet(URI.create("https://s.taobao.com/search?q=%E6%B0%B4%E6%9D%AF&tab=all&promote=0&bcoffset=-3&s=44&ntoffset=-3"));
		
		
		
	}
	
	public static  void close(){
		
		
	}
	

}
