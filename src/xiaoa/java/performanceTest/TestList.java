package xiaoa.java.performanceTest;

import xiaoa.java.utils.time.DateFormatUtils;

import java.util.*;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * 测试list
 * @author xiaoa
 * @date 2017年6月11日 下午8:57:00
 * @version V1.0
 *
 */
public class TestList {
	
	
	public static void main(String[] args) throws Throwable{
		
		
		System.out.println("start");
		
		int i = 1000;
		
		// ArrayList
     	testArrayList(10000 * i, new ArrayList<String>());
		
		System.out.println("============ 指定长度 ");
		// ArrayList
		testArrayList(10000 * i, new ArrayList<String>(10000 * i));
		
		// LinkedList
		testArrayList(10000 * i , new LinkedList<String>());
		
//		// CopyOnWriteArrayList
//		testArrayList(10000 * i , new CopyOnWriteArrayList<>());
		
		testQueue(10000 * i , new LinkedBlockingQueue<String>());
		
		
		testQueue(10000 * i , new ArrayBlockingQueue<String>(10000 * i));


		
		
	}
	
	/**
	 * 测试ArrayList
	 * @Title: testArrayList
	 * @param size
	 * @author xiaoa
	 */
	public static void testArrayList(long size , List<String> list){
		
		Date start = new Date();
		
		{
			// time
			long startTime = System.nanoTime();
			
			for (long i = 0 ; i < size ; i ++){
			
				long strStartTime = System.nanoTime();
				
				String str = new Random().nextLong() + "";
				
				startTime -= System.nanoTime()-  strStartTime;
				
				list.add(str);
				
			}
			
			long useNano = System.nanoTime() - startTime;
			
			System.out.println(" add  list = " + list.getClass().getSimpleName() + "  size = " + size + "  useNano = " + (useNano) + " time = " + (useNano / 1000000) );
			
		}
		
		{
			// time
			long startTime = System.nanoTime();
			
			for (String str : list);
			
	        long useNano = System.nanoTime() - startTime;
			
			System.out.println(" for start = " + DateFormatUtils.format_yyyyMMddhhssSSS(start) + "  end = " + DateFormatUtils.format_yyyyMMddhhssSSS(new Date()) + "   list = " + list.getClass().getSimpleName() + "  size = " + size + "  useNano = " + (useNano) + " time = " + (useNano / 1000000) );

			System.out.println("======================================================");
			
		}
		
		
		
	}
	
	
	public static void testQueue(long size , BlockingQueue<String> queue)throws Throwable{
		
		Date start = new Date();
		
		{
			// time
			long startTime = System.nanoTime();
			
			for (long i = 0 ; i < size ; i ++){
			
				long strStartTime = System.nanoTime();
				
				String str = new Random().nextInt(10) + "";
				
				startTime -= System.nanoTime()-  strStartTime;
				
				queue.add(str);
				
			}
			
			long useNano = System.nanoTime() - startTime;
			
			System.out.println(" add  list = " + queue.getClass().getSimpleName() + "  size = " + size + "  useNano = " + (useNano) + " time = " + (useNano / 1000000) );
			
		}
		
		{
			// time
			long startTime = System.nanoTime();
			
			for (;!queue.isEmpty();queue.take());
			
	        long useNano = System.nanoTime() - startTime;
			
			System.out.println(" for start = " + DateFormatUtils.format_yyyyMMddhhssSSS(start) + "  end = " + DateFormatUtils.format_yyyyMMddhhssSSS(new Date()) + "   list = " + queue.getClass().getSimpleName() + "  size = " + size + "  useNano = " + (useNano) + " time = " + (useNano / 1000000) );

			System.out.println("======================================================");
			
		}
		
		
		
	}
	
	

}
