package xiaoa.java.field;

import java.lang.reflect.Field;

/**
 * 字段工具类
 * @author xiaoa
 * @date 2017年10月8日 下午4:25:10
 * @version V1.0
 *
 */
public class FieldUtils {
	
	/**
	 * 获取所有字段
	 * @Title: getAllFields
	 * @param cla
	 * @return
	 * @author xiaoa
	 */
	public static Field[] getAllFields(Class<?> cla){
		
		Field[] fs = null;
		
		for (Class<?> tempCla = cla ; tempCla != null ; tempCla = tempCla.getSuperclass() ){
			
			Field[] claFs = tempCla.getDeclaredFields();
			
			if (claFs == null || claFs.length == 0){
				continue;
			}
			
			Field[] tempFs = new Field[(fs == null ? 0 : fs.length ) + claFs.length];
			
			if (fs != null){
				System.arraycopy(fs, 0, tempFs, 0, fs.length);
			}
			
			System.arraycopy(claFs,0 , tempFs, (fs == null ? 0 : fs.length  ), claFs.length);
			
			fs = tempFs;
		}
		
	    return fs.length == 0 ? null : fs;
	}

}
