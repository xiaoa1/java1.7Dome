package xiaoa.java.task;

import xiaoa.java.lang.test.ObjecstTest;
import xiaoa.java.log.L;

/**
 * 线程测试类   实现 runnable  接口    
 * @author lkc
 */
public class RunnableTest  implements Runnable{

	//  线程名字
	public String  threadName;
	
	//  线程休眠时间
	public  int  timeout=0;


	// 创建默认方法
	public RunnableTest(String  threadName ){
		this.threadName  = threadName;
	}
	
	@Override
	public void run() {
		try {
			
			//  开始休眠
			synchronized (RunnableTest.class){
				
				RunnableTest.class.wait(timeout);
				
				// 调用线程
				ObjecstTest.threadSync(threadName , timeout);
				
				System.out.println("线程名字:"+threadName);
				
			}
			
			
			
			//Thread.sleep(timeout);
			
		} catch (Throwable e) {
			// 打印异常
			L.info(e.getMessage());;
			
		}
		
	}

}
