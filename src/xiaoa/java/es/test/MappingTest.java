package xiaoa.java.es.test;

import com.alibaba.fastjson.JSON;

import xiaoa.java.es.bean.MappingBulider;
import xiaoa.java.es.bean.MappingConstant;

public class MappingTest {

	
	public static void main(String[] args) {
		
		
		MappingBulider  bulider = new MappingBulider();
		
		{
			MappingBulider.Type  type = new MappingBulider.Type();
			
			{ // ID
				MappingBulider.Properties  properties = new MappingBulider.Properties();
				properties.type  = MappingConstant.Properties.TYPE_STRING;
				properties.index = MappingConstant.Properties.INDEX_NOT_ANALYZED;
				properties.store =  true;
				type.properties.put("id", properties);

			}
			
			{ // pushTime
				MappingBulider.Properties  properties = new MappingBulider.Properties();
				properties.type  = MappingConstant.Properties.TYPE_STRING;
				properties.index = MappingConstant.Properties.INDEX_NOT_ANALYZED;
				properties.store =  true;
				type.properties.put("pushTime", properties);

			}
			
			{ // pushTime
				MappingBulider.Properties  properties = new MappingBulider.Properties();
				properties.type  = MappingConstant.Properties.TYPE_DATE;
				properties.index = MappingConstant.Properties.INDEX_NOT_ANALYZED;
				properties.store =  true;
				type.properties.put("indexTime", properties);

			}
			
			{ // indexTimeStr
				MappingBulider.Properties  properties = new MappingBulider.Properties();
				properties.type  = MappingConstant.Properties.TYPE_STRING;
				properties.index = MappingConstant.Properties.INDEX_NOT_ANALYZED;
				properties.store =  true;
				type.properties.put("indexTimeStr", properties);

			}
			
			{ // indexMonth
				MappingBulider.Properties  properties = new MappingBulider.Properties();
				properties.type  = MappingConstant.Properties.TYPE_STRING;
				properties.index = MappingConstant.Properties.INDEX_NOT_ANALYZED;
				properties.store =  true;
				type.properties.put("pushMonth", properties);
			}
			
			{ // indexDate
				MappingBulider.Properties  properties = new MappingBulider.Properties();
				properties.type  = MappingConstant.Properties.TYPE_STRING;
				properties.index = MappingConstant.Properties.INDEX_NOT_ANALYZED;
				properties.store =  true;
				type.properties.put("pushMonth", properties);
			}
			
			
			{ // indexDate
				MappingBulider.Properties  properties = new MappingBulider.Properties();
				properties.type  = MappingConstant.Properties.TYPE_STRING;
				properties.index = MappingConstant.Properties.INDEX_NOT_ANALYZED;
				properties.store =  true;
				type.properties.put("pushHour", properties);
			}
			

			{ // userName
				MappingBulider.Properties  properties = new MappingBulider.Properties();
				properties.type  = MappingConstant.Properties.TYPE_STRING;
				properties.index = MappingConstant.Properties.INDEX_NOT_ANALYZED;
				properties.store =  true;
				type.properties.put("userName", properties);
			}
			
			

			{ // userName
				MappingBulider.Properties  properties = new MappingBulider.Properties();
				properties.type  = MappingConstant.Properties.TYPE_STRING;
				properties.index = MappingConstant.Properties.INDEX_NOT_ANALYZED;
				properties.store =  true;
				type.properties.put("userNameAt", properties);
			}
			
			{ // worksList
				MappingBulider.Properties  properties = new MappingBulider.Properties();
				properties.type  = MappingConstant.Properties.TYPE_STRING;
				properties.index = MappingConstant.Properties.INDEX_NOT_ANALYZED;
				properties.store =  true;
				type.properties.put("worksList", properties);
			}
			
			
			
			
			{ // Title 
				MappingBulider.Properties  properties = new MappingBulider.Properties();
				properties.type  = MappingConstant.Properties.TYPE_STRING;
				properties.index = MappingConstant.Properties.INDEX_ANALYZED;
				properties.store =  true;
				properties.analyzer = "ik_max_word";
				properties.search_analyzer = "ik_max_word";
				type.properties.put("title", properties);

			}
			
			{ // Content 
				MappingBulider.Properties  properties = new MappingBulider.Properties();
				properties.type  = MappingConstant.Properties.TYPE_STRING;
				properties.index = MappingConstant.Properties.INDEX_ANALYZED;
				properties.store =  true;
				properties.analyzer = "ik_max_word";
				properties.search_analyzer = "ik_max_word";
				type.properties.put("content", properties);
			}
			
			{ // Keys 
				MappingBulider.Properties  properties = new MappingBulider.Properties();
				properties.type  = MappingConstant.Properties.TYPE_STRING;
				properties.index = MappingConstant.Properties.INDEX_ANALYZED;
				properties.store =  true;
				properties.analyzer = "ik_max_word";
				properties.search_analyzer = "ik_max_word";
				type.properties.put("keys", properties);
			}
			
			{ // WebpageUrl 
				MappingBulider.Properties  properties = new MappingBulider.Properties();
				properties.type  = MappingConstant.Properties.TYPE_STRING;
				properties.index = MappingConstant.Properties.INDEX_NOT_ANALYZED;
				properties.store =  true;
				type.properties.put("webUrl", properties);
			}
			
			bulider.mappings.put("content", type);
		}
		
		
		System.out.println(JSON.toJSONString(bulider.mappings));
		
		System.out.println(bulider.bulid());
		
		
	}
	
}
