package xiaoa.java.es.http;

import java.nio.charset.Charset;

import java.util.HashMap;
import java.util.Map;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import xiaoa.java.es.client.ClientUtils;
import xiaoa.java.netty.HttpClientUtils;
import xiaoa.java.netty.Log;

public class AddDocument {
	
	public static void main(String[] args)throws Throwable {
		
		HttpClientUtils.init();
		
		// 初始化es
		ClientUtils.initClient("192.168.218.133" , 9300  , "elasticsearch");
		
		
		for(int i = 10 ; i < 20 ; i ++ ){
			
			Map<String, String>  parmaMap = new HashMap<String, String>();
			
			parmaMap.put("from", (4600 + i * 500) + "");
			parmaMap.put("size", "500");
			
		    String dataJson = HttpClientUtils.doGet("http://search.es-test.wyq.cn/contentindex_3days,contentindex_test/_search", parmaMap, null, Charset.defaultCharset()).getBodyToString();
			
		    JSONObject jsonObj =   JSON.parseObject(dataJson);
		   
		    JSONArray  hits  = jsonObj.getJSONObject("hits").getJSONArray("hits");
		   
		    long startTime = System.currentTimeMillis();
		    
		    for (Object h : hits ){
			   
			   JSONObject   ho = (JSONObject)h;
			   String key = ho.getString("_id");
			   
			   String data = ho.get("_source").toString();
			   
			   Log.setDebug(true);
			   
			   ClientUtils.addDocToCache("test_node", "testWbIndex", key, data);
			   
			   System.out.println("添加成功==========" + key);
			   
		   }
		    
		    System.out.println(i + "  use = " + (System.currentTimeMillis() - startTime) );
		   
			
		}
		
	
		ClientUtils.fushDoc();
	   
	   System.out.println("完成");
	   
	   
		
		
	}
	
	

}
