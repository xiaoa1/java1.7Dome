package xiaoa.java.es.bean;

public class MappingConstant {

	/**
	 * string(指定分词器)
	date(默认使用UTC保持,也可以使用format指定格式)
	数值类型(byte,short,integer,long,float,double)
	boolean
	binary(存储在索引中的二进制数据的base64表示，比如图像，只存储不索引)
	ip(以数字形式简化IPV4地址的使用，可以被索引、排序并使用IP值做范围查询).
	 * @author xiaoa
	 * @date 2017年11月19日 上午9:44:38
	 * @version V1.0
	 *
	 */
	public static class Properties{
		
		
		// (指定分词器)
		public final static  String TYPE_STRING = "string";
		
		// 时间
		public final static  String TYPE_DATE = "date";

		// 二进制
		public final static  String TYPE_BINARY = "binary";
		
		// 分词
		public final static  String INDEX_ANALYZED = "analyzed";
		
		// 不分词
		public final static  String INDEX_NOT_ANALYZED = "not_analyzed";

		
	}
	
	
}
