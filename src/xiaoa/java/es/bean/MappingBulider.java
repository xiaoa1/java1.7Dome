package xiaoa.java.es.bean;

import java.util.HashMap;
import java.util.Map;

import com.alibaba.fastjson.JSON;

public class MappingBulider {
	
	/**
	 * 设置
	 */
	public Settings settings  = new Settings();
	
	public Map<String, Type>  mappings = new HashMap<>();
	
	
	public static class Settings{
		
		/**
		 * 分片数
		 */
		public int number_of_shards = 1;
		
		/**
		 * 副本数
		 */
		public int number_of_replicas = 0;
		
	}
	
	
	/**
	 * 类型
	 * @author xiaoa
	 * @date 2017年11月19日 上午9:52:37
	 * @version V1.0
	 *
	 */
	public static class Type extends HashMap<String, Object>{
		
		private static final long serialVersionUID = 1L;
		public  Map<String, Properties> properties = new HashMap<>();
		
		
		public Type() {
			put("properties", properties);
		}
		
	}
	
	
	
	public static class Properties{
		
		/**
		 * string(指定分词器)
		date(默认使用UTC保持,也可以使用format指定格式)
		数值类型(byte,short,integer,long,float,double)
		boolean
		binary(存储在索引中的二进制数据的base64表示，比如图像，只存储不索引)
		ip(以数字形式简化IPV4地址的使用，可以被索引、排序并使用IP值做范围查询).
		 */
		public String type;
		
		
		/**
		 * 	index
                   可选值为analyzed(默认)和no，如果是字段是字符串类型的，则可以是not_analyzed.
		 */
		public String index;
		
		/**
		 * store
可选值为yes或no，指定该字段的原始值是否被写入索引中，默认为no，即结果中不能返回该字段
		 */
		public boolean store = true;
		
		/**
		 * boost
                     默认为1，定义了文档中该字段的重要性，越高越重要
		 */
		public String boost;
		
		/**
		 * 如果一个字段为null值(空数组或者数组都是null值)的话不会被索引及搜索到，null_value参数可以显示替代null values为指定值，这样使得字段可以被搜索到。
		 */
		public String null_value;
		
		
		/**
		 * 指定该字段是否应该包括在_all字段里头，默认情况下都会包含。
		 */
		public String include_in_all;
		
		/**
		 * 分词器
		 */
		public String analyzer;
		
		/**
		 * 搜索 分词器
		 */
		public String search_analyzer;
		
		 
	}
	
	
	
	
	public String bulid(){
		
		return JSON.toJSONString(this);
		
	}
	

}
