package xiaoa.java.es.bean;


/**
 * 搜索参数
 * @author xiaoa
 * @date 2017年12月10日 下午12:57:29
 * @version V1.0
 *
 */
public class Params {
	
	/**
	 * 搜索
	 */
	private int searchEngineType;
	
	/**
	 * 事务id
	 */
	private String transactionId;
	
	
	/**
	 * 用户名
	 */
	private String userName;
	
	/***
	 * 统计字段
	 */
	public static final String STAT_FIELD_USERNAME = "userName";
	public static final String STAT_FIELD_USERNAME_AT = "userNameAt";

	private String statField;
	

	public String getStatField() {
		return statField;
	}

	public void setStatField(String statField) {
		this.statField = statField;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public int getSearchEngineType() {
		return searchEngineType;
	}

	public void setSearchEngineType(int searchEngineType) {
		this.searchEngineType = searchEngineType;
	}
	
}
