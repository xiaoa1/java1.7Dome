package xiaoa.java.es.bean;

import java.util.Date;
import java.util.List;
import java.util.Set;

public class Document {
	


    public static final String ORIGINTYPE_ALL = "all";
    public static final String ORIGINTYPE_WZ = "wz";// 网站
    public static final String ORIGINTYPE_LT = "lt";// 论坛
    public static final String ORIGINTYPE_BK = "bk";// 博客
    public static final String ORIGINTYPE_WB = "wb";// 微博
    public static final String ORIGINTYPE_TB = "tb";// 贴吧
    public static final String ORIGINTYPE_JW = "jw";// 境外
    public static final String ORIGINTYPE_MG = "mg";// 敏感信息
    public static final String ORIGINTYPE_WX = "wx";// 微信
    public static final String ORIGINTYPE_SP = "sp";// 视频
    public static final String ORIGINTYPE_XW = "xw";// 新闻
    public static final String ORIGINTYPE_BAOKAN = "baokan";// 报刊
    public static final String ORIGINTYPE_ZW = "zw";// 政务
    public static final String ORIGINTYPE_GT = "gt";// 跟贴
    public static final String ORIGINTYPE_APP = "app";// APP
    public static final String ORIGINTYPE_OTHER = "qt";
    protected String aliasTitle = "";// 别名标题
    /**
     * 作者名称
     */
    protected String author;// wz,wb'origin_author_name'|wb.comment'author_name'
    protected String authorRaw;// 作者(不分词)
    protected String originWebsiteName;// 来源网站名称（如新华社、红网）
    protected String originWebsiteUrl;// 来源网站网址
    /**
     * 评论数
     */
    protected Integer comments;// wz,wb
    protected Double commentsTemp;// 评论者数量标签
    protected Integer commentatorNumber;// 评论者数量
    /**
     * 分享数量
     */
    protected Integer shareNumber;
    protected Date lastEdittime;// 帖子最后编辑时间
    protected String webpageScreenshot;// 网站快照
    protected String webpageUrl;// 原文链接(网站、源微博);wz,wb'weibo_url'
    protected String captureWebsiteName;// 采集网站（如天涯、新浪）
    protected Integer captureWebsiteId;// 采集微博ID;wb
    protected String contentPlate;// 内容板块
    protected String tag;// 标签
    protected Integer collectionNumber;// 收藏数量;wz,wb
    protected Date lastCtime;// 最后评论时间;wz,wb
    /* wb only */
    protected Date sourcePublished; // 源微博发布时间
    protected String weiboId;// 微博ID
    protected String originAuthorId;// 原作者Id | comment'回复者Id,author_id'
    protected String sourceClient;// 来源终端（如小米手机，iPhone4）
    protected String sourceUrl;// 来源链接
    protected Date lastForwardTime;// 最后转发时间
    protected String forwarderId;// 转发微博Id
    protected String forwarderContent;// 转发微博内容
    protected String forwarderAuthorId;// 转发者Id
    protected String forwarderAuthorName;// 转发者名称
    protected String forwarderWeiboUrl;// 转发微博原文链接
    protected Date forwardTime;// 转发时间
    protected Integer forwardComments;// 转发后评论数
    protected Integer forwardForwardNumber;// 转发后转发数
    protected String sourceGeo;// 地理位置信息（原微博地理信息）|comment'source_geo'
    protected Integer weiboAccountId;// 采集微博账号ID
    protected String commentId;// 评论内容id
    protected String authorNick;// 回复人昵称
    protected String contentOrder;// 内容顺序（几楼，沙发。。。）
    protected String sameNum;// 相同数量
    protected boolean isAddPro = false;// 是否被加入简报
    protected String majorID;// 主内容索引ID，当创建评论索引时使用
    protected String captureWebsite;
    protected String authoracount;
    protected String originAuthorName;
    protected Integer praiseNum = 0;// 点赞数
    protected Integer answerNum = 0;// 回复数
    protected String websiteCode;// 网站来源code
    protected String profileImageUrl;// 网站logo、微博用户头像
    // ---------------wb分析字段-------------------------------------/
    protected String parentUserScreenName;// 上级id
    protected String userCustomFlag1;// 用户负面属性
    //已读未读标记(0:未读 1:已读)", required = true)
    protected String readFlag;// 用户已读未读属性
    //已读编辑", required = true)
    protected String readUser;// 用户已读未读属性
    protected String isNormalData;// 是否是噪音数据 1:正常数据 2:是噪音数据
    protected String webColumnName;// 二级域名（如天涯XX版块、新浪XX版块）
    protected String contentAddress;// 精准地域
    /* 来源类型originType常量 */
    protected String originType = ORIGINTYPE_WZ;// 来源类型
    protected Date captureTime;// 抓取时间
    protected Date updateTime;// 内容更新时间
    protected long contentId = 0;// 内容自增长ID
    protected String publishedString;
    protected int customHot1 = 0;// 自定义热度1[views+comments+forwardNumber]
    protected int repeatNum = 0;// 重复数
    protected int repeatPosition = 0; // 重复位置
    protected String repeatFirstID;// 重复索引首条ID
    protected String repeatLastID;// 重复索引最后一条ID
    // "1":疑似负面 "2":负面 3": 疑似负面和负面 "4":非负面
    protected String customFlag1;// 内容标记1[Normal-普通的，Positive-正面的，Negative-负面的]
    private Integer forwardNumber = 0;
    //微博粉丝数", required = true)
    private Integer fansNumber = 0;
    //微博关注数", required = true)
    private Integer friendsCount = 0;
    // 微博层级", required = true)
    private Integer Spare3;
    //父微博用户昵称", required = true)
    private String Spare5;
    //微博用户类型", required = true)
    private Integer verifiedType = -1;
    //微博阅读数", required = true)
    private Integer views = 0;
    //微博微博数", required = true)
    private Integer weiboNums = 0;
    //列表内排名", required = true)
    private int ranking = 0;
    //统计数量", required = true)
    private int num = 0;
    //统计热度(双精度)", required = true)
    private double total = 0;
    //优先级", required = true)
    private int priority = 0;
    //类型(普通:0,评论:1)", required = true)
    private int type = 0;
    //平台标志", required = true)
    private String platformTag;
    //用户ID或用户标签", required = true)
    private String userTag;
    // 简报ID", required = true)
    private String reportId;
    //目录标识", required = true)
    private String directory;
    //简报序列", required = true)
    private int sequence = 0;
    //类型标识 1：收藏夹 2：简报", required = true)
    private String featuresType;
    //编号", required = true)
    private int position = 0;
    //涉及关键词", required = true)
    private String keyword;
    //微博定位", required = true)
    private String locationAddress;
    //去重标识", required = true)
    private String noRepeat;
    //行业标签", required = true)
    private String secondTrade;
    //是否长微博", required = true)
    private boolean weiboHeadline;
    private String json;
    private String taskTicket;
    private String rootId;
    private String parentId;
    private String mid;
    private String unParticipleContent;
    private String clusterContent;
    private int repostsCount;
    private int commentsCount;
    private int attitudesCount;
    private int readsCount;
    private String url;
    private String urbanrank;
    private String location;
    private String platform;
    private String source;
    private String options;
    private int faceScore;
    private String uid;
    private String userScreenName;
    private String userProfileImageUrl;
    //性别", required = true)
    private String usergender;
    private String userVerified;
    private String userProvince;
    private String userCity;
    private String userLocation;
    private String userChineseHoroscope;
    private int userAge;
    private String userHoroscope;
    private Date userBirthday;
    private String userRemark;
    private String userVerifiedReason;
    private String userDescription;
    private String userAbility;
    private String navyFlg;
    private String activeFlg;
    private int userFriendsCount;
    private int userFollowersCount;

    // `weibo_content_id` bigint(17) DEFAULT NULL,
    private int userStatusesCount;
    private int tier;
    private String existNext;
    private String repostsFlg;
    private String topicId;
    private int seqNo;
    private String dataTypeWfx;
    //大头像", required = true)
    private String avatarLarge;
    private String hbaseId;
    private String remark;
    private Date indexTime;
    private Date published;
    //城市标签", required = true)
    private String labelCity;
    //股票标签", required = true)
    private String labelStock;
    //名人标签", required = true)
    private String labelEminentPerson;
    //品牌标签", required = true)
    private String labelBrand;
    //企业标签", required = true)
    private String labelCompany;
    //行业标签", required = true)
    private String labelTrade;
    //兴趣标签", required = true)
    private String labelInterest;
    //类型标签", required = true)
    private String labelType;
    // 继承类的新增字段
    private String id;
    private String dataType;
    private String title;
    private String titleTemp;
    private String titleHs;
    private Set<String> titleSet;
    private String keys;
    private String filterkeys;
    private String content;
    private String contentTemp;
    private String publishedHour;
    private String publishedDay;
    private String publishedMonth;
    private String publishedMinute;
    private String publishedWeek;
    private String publishedTenMinute;
    private String province;
    private String city;
    private String summary;
    // 新增加字段
    private String astEdittime;
    private String deluser;
    private String rskeys;
    private Integer biFollowersCount;// 粉丝数
    private Integer level;// 微博转发等级
    private String isRecommended;// 优选推荐标记 2:正常(含优选词语) 1:没有优选词语(默认0或未标记)
    private boolean isRead = false;// true :已读 false：未读
    private String emotion;    // 情绪

    // 内容属性 1:视频
    private String contentType;
    private List<String> pictureList;
    private List<String> videoList;

    // web 逻辑中使用，索引中不记录不生效
    private boolean negative;// 负面的
    private boolean favorite;// 收藏的
    // ***weibo分析新加字段*****/
    private boolean weiboIsDel;//微博是否被删除
    private String Classification;
    //权重(1:重点 2:普通重点 3:普通)", required = true)
    private String boostLevel;
    //短链内容", required = true)
    private String shortUrlContent;
    //短链列表", required = true)
    private List<String> shortUrlList;
    
    protected long realReadNum = 0;
    
    
    // 敏感等级字段
    //汽车敏感等级", required = true)
    protected String customFlagAuto;
    //教育", required = true)
    protected String customFlagEdu;
  
    //金融敏感等级", required = true)
    private String customFlagFinance;
    //能源环保敏感等级", required = true)
    private String customFlagEnergy ;
    //美容敏感等级", required = true)
    private String customFlagBeauty;
    //科技敏感等级", required = true)
    private String customFlagTech ;
    //生活敏感等级", required = true)
    private String customFlagLive ;
    //游戏敏感等级", required = true)
    private String customFlagGames ;
    //母婴敏感等级", required = true)
    private String customFlagBaby ;
    //时尚敏感等级", required = true)
    private String customFlagFashion ;
    //旅游敏感等级", required = true)
    private String customFlagTravel ;
    //文化敏感等级", required = true)
    private String customFlagCulture ;
    //政务敏感等级", required = true)
    private String customFlagGov ;
    //招标敏感等级", required = true)
    private String customFlagZhaobiao;
    //军事敏感等级", required = true)
    private String customFlagMil ;
    //房产敏感等级", required = true)
    private String customFlagHouse ;
    //彩票敏感等级", required = true)
    private String customFlagLottery ;
    //娱乐敏感等级", required = true)
    private String customFlagEnt;
    //医疗敏感等级", required = true)
    private String customFlagMedical ;
    //健康敏感等级", required = true)
    private String customFlagHealth ;
    //体育敏感等级", required = true)
    private String customFlagSports ;
    //司法敏感等级", required = true)
    private String customFlagSifa;

    
    
    
    
    public long getRealReadNum() {
		return realReadNum;
	}

	public void setRealReadNum(long realReadNum) {
		this.realReadNum = realReadNum;
	}

	//评论表：    评论id，评论者昵称，评论内容 ，点赞数，回复数，内容ID，用户ID
    //内容表：   内容id，标题，发布时间，富文本，作者，阅读数，点赞数，评论数，标签
    //用户表： 用户id，用户昵称，性别，生日，所在地，省，市
    public String getClassification() {
		return Classification;
	}

	public void setClassification(String classification) {
		Classification = classification;
	}

    public String getBoostLevel() {
        return boostLevel;
    }

    public void setBoostLevel(String boostLevel) {
        this.boostLevel = boostLevel;
    }

    public String getShortUrlContent() {
        return shortUrlContent;
    }

    public void setShortUrlContent(String shortUrlContent) {
        this.shortUrlContent = shortUrlContent;
    }

    public List<String> getShortUrlList() {
        return shortUrlList;
    }

    public void setShortUrlList(List<String> shortUrlList) {
        this.shortUrlList = shortUrlList;
    }


  
	public String getPublishedTenMinute() {
		return publishedTenMinute;
	}

	public void setPublishedTenMinute(String publishedTenMinute) {
		this.publishedTenMinute = publishedTenMinute;
	}

	public String getPublishedMinute() {
		return publishedMinute;
	}

	public void setPublishedMinute(String publishedMinute) {
		this.publishedMinute = publishedMinute;
	}

	public String getPublishedWeek() {
		return publishedWeek;
	}

	public void setPublishedWeek(String publishedWeek) {
		this.publishedWeek = publishedWeek;
	}

	public String getEmotion() {
		return emotion;
	}

	public void setEmotion(String emotion) {
		this.emotion = emotion;
	}

	public boolean isWeiboIsDel() {
		return weiboIsDel;
	}

	public void setWeiboIsDel(boolean weiboIsDel) {
		this.weiboIsDel = weiboIsDel;
	}

    public String getCustomFlagAuto() {
        return customFlagAuto;
    }

    public void setCustomFlagAuto(String customFlagAuto) {
        this.customFlagAuto = customFlagAuto;
    }

    public String getCustomFlagEdu() {
        return customFlagEdu;
    }

    public void setCustomFlagEdu(String customFlagEdu) {
        this.customFlagEdu = customFlagEdu;
    }

    public static String getOrigintypeAll() {
        return ORIGINTYPE_ALL;
    }

    public static String getOrigintypeWz() {
        return ORIGINTYPE_WZ;
    }

    public static String getOrigintypeLt() {
        return ORIGINTYPE_LT;
    }

    public static String getOrigintypeBk() {
        return ORIGINTYPE_BK;
    }

    public static String getOrigintypeWb() {
        return ORIGINTYPE_WB;
    }

    public static String getOrigintypeTb() {
        return ORIGINTYPE_TB;
    }

    public static String getOrigintypeJw() {
        return ORIGINTYPE_JW;
    }

    public static String getOrigintypeMg() {
        return ORIGINTYPE_MG;
    }

    public static String getOrigintypeWx() {
        return ORIGINTYPE_WX;
    }

    public static String getOrigintypeSp() {
        return ORIGINTYPE_SP;
    }

    public static String getOrigintypeXw() {
        return ORIGINTYPE_XW;
    }

    public static String getOrigintypeBaokan() {
        return ORIGINTYPE_BAOKAN;
    }

    public static String getOrigintypeZw() {
        return ORIGINTYPE_ZW;
    }

    public static String getOrigintypeGt() {
        return ORIGINTYPE_GT;
    }

    public static String getOrigintypeApp() {
        return ORIGINTYPE_APP;
    }

    public static String getOrigintypeOther() {
        return ORIGINTYPE_OTHER;
    }

    public Integer getForwardNumber() {
        return forwardNumber;
    }

    public void setForwardNumber(Integer forwardNumber) {
        this.forwardNumber = forwardNumber;
    }

    public Integer getFansNumber() {
        return fansNumber;
    }

    public void setFansNumber(Integer fansNumber) {
        this.fansNumber = fansNumber;
    }

    public Integer getFriendsCount() {
        return friendsCount;
    }

    public void setFriendsCount(Integer friendsCount) {
        this.friendsCount = friendsCount;
    }

    public Integer getSpare3() {
        return Spare3;
    }

    public void setSpare3(Integer spare3) {
        Spare3 = spare3;
    }

    public String getSpare5() {
        return Spare5;
    }

    public void setSpare5(String spare5) {
        Spare5 = spare5;
    }

    public Integer getVerifiedType() {
        return verifiedType;
    }

    public void setVerifiedType(Integer verifiedType) {
        this.verifiedType = verifiedType;
    }

    public Integer getViews() {
        return views;
    }

    public void setViews(Integer views) {
        this.views = views;
    }

    public Integer getWeiboNums() {
        return weiboNums;
    }

    public void setWeiboNums(Integer weiboNums) {
        this.weiboNums = weiboNums;
    }

    public int getRanking() {
        return ranking;
    }

    public void setRanking(int ranking) {
        this.ranking = ranking;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getPlatformTag() {
        return platformTag;
    }

    public void setPlatformTag(String platformTag) {
        this.platformTag = platformTag;
    }

    public String getUserTag() {
        return userTag;
    }

    public void setUserTag(String userTag) {
        this.userTag = userTag;
    }

    public String getReportId() {
        return reportId;
    }

    public void setReportId(String reportId) {
        this.reportId = reportId;
    }

    public String getDirectory() {
        return directory;
    }

    public void setDirectory(String directory) {
        this.directory = directory;
    }

    public int getSequence() {
        return sequence;
    }

    public void setSequence(int sequence) {
        this.sequence = sequence;
    }

    public String getFeaturesType() {
        return featuresType;
    }

    public void setFeaturesType(String featuresType) {
        this.featuresType = featuresType;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getJson() {
        return json;
    }

    public void setJson(String json) {
        this.json = json;
    }

    public String getTaskTicket() {
        return taskTicket;
    }

    public void setTaskTicket(String taskTicket) {
        this.taskTicket = taskTicket;
    }

    public String getRootId() {
        return rootId;
    }

    public void setRootId(String rootId) {
        this.rootId = rootId;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getMid() {
        return mid;
    }

    public void setMid(String mid) {
        this.mid = mid;
    }

    public String getUnParticipleContent() {
        return unParticipleContent;
    }

    public void setUnParticipleContent(String unParticipleContent) {
        this.unParticipleContent = unParticipleContent;
    }

    public String getClusterContent() {
        return clusterContent;
    }

    public void setClusterContent(String clusterContent) {
        this.clusterContent = clusterContent;
    }

    public int getRepostsCount() {
        return repostsCount;
    }

    public void setRepostsCount(int repostsCount) {
        this.repostsCount = repostsCount;
    }

    public int getCommentsCount() {
        return commentsCount;
    }

    public void setCommentsCount(int commentsCount) {
        this.commentsCount = commentsCount;
    }

    public int getAttitudesCount() {
        return attitudesCount;
    }

    public void setAttitudesCount(int attitudesCount) {
        this.attitudesCount = attitudesCount;
    }

    public int getReadsCount() {
        return readsCount;
    }

    public void setReadsCount(int readsCount) {
        this.readsCount = readsCount;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUrbanrank() {
        return urbanrank;
    }

    public void setUrbanrank(String urbanrank) {
        this.urbanrank = urbanrank;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getOptions() {
        return options;
    }

    public void setOptions(String options) {
        this.options = options;
    }

    public int getFaceScore() {
        return faceScore;
    }

    public void setFaceScore(int faceScore) {
        this.faceScore = faceScore;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getUserScreenName() {
        return userScreenName;
    }

    public void setUserScreenName(String userScreenName) {
        this.userScreenName = userScreenName;
    }

    public String getUserProfileImageUrl() {
        return userProfileImageUrl;
    }

    public void setUserProfileImageUrl(String userProfileImageUrl) {
        this.userProfileImageUrl = userProfileImageUrl;
    }

    public String getUsergender() {
        return usergender;
    }

    public void setUsergender(String usergender) {
        this.usergender = usergender;
    }

    public String getUserVerified() {
        return userVerified;
    }

    public void setUserVerified(String userVerified) {
        this.userVerified = userVerified;
    }

    public String getUserProvince() {
        return userProvince;
    }

    public void setUserProvince(String userProvince) {
        this.userProvince = userProvince;
    }

    public String getUserCity() {
        return userCity;
    }

    public void setUserCity(String userCity) {
        this.userCity = userCity;
    }

    public String getUserLocation() {
        return userLocation;
    }

    public void setUserLocation(String userLocation) {
        this.userLocation = userLocation;
    }

    public String getUserChineseHoroscope() {
        return userChineseHoroscope;
    }

    public void setUserChineseHoroscope(String userChineseHoroscope) {
        this.userChineseHoroscope = userChineseHoroscope;
    }

    public int getUserAge() {
        return userAge;
    }

    public void setUserAge(int userAge) {
        this.userAge = userAge;
    }

    public String getUserHoroscope() {
        return userHoroscope;
    }

    public void setUserHoroscope(String userHoroscope) {
        this.userHoroscope = userHoroscope;
    }

    public Date getUserBirthday() {
        return userBirthday;
    }

    public void setUserBirthday(Date userBirthday) {
        this.userBirthday = userBirthday;
    }

    public String getUserRemark() {
        return userRemark;
    }

    public void setUserRemark(String userRemark) {
        this.userRemark = userRemark;
    }

    public String getUserVerifiedReason() {
        return userVerifiedReason;
    }

    public void setUserVerifiedReason(String userVerifiedReason) {
        this.userVerifiedReason = userVerifiedReason;
    }

    public String getUserDescription() {
        return userDescription;
    }

    public void setUserDescription(String userDescription) {
        this.userDescription = userDescription;
    }

    public String getUserAbility() {
        return userAbility;
    }

    public void setUserAbility(String userAbility) {
        this.userAbility = userAbility;
    }

    public String getNavyFlg() {
        return navyFlg;
    }

    public void setNavyFlg(String navyFlg) {
        this.navyFlg = navyFlg;
    }

    public String getActiveFlg() {
        return activeFlg;
    }

    public void setActiveFlg(String activeFlg) {
        this.activeFlg = activeFlg;
    }

    public int getUserFriendsCount() {
        return userFriendsCount;
    }

    public void setUserFriendsCount(int userFriendsCount) {
        this.userFriendsCount = userFriendsCount;
    }

    public int getUserFollowersCount() {
        return userFollowersCount;
    }

    public void setUserFollowersCount(int userFollowersCount) {
        this.userFollowersCount = userFollowersCount;
    }

    public int getUserStatusesCount() {
        return userStatusesCount;
    }

    public void setUserStatusesCount(int userStatusesCount) {
        this.userStatusesCount = userStatusesCount;
    }

    public int getTier() {
        return tier;
    }

    public void setTier(int tier) {
        this.tier = tier;
    }

    public String getExistNext() {
        return existNext;
    }

    public void setExistNext(String existNext) {
        this.existNext = existNext;
    }

    public String getRepostsFlg() {
        return repostsFlg;
    }

    public void setRepostsFlg(String repostsFlg) {
        this.repostsFlg = repostsFlg;
    }

    public String getTopicId() {
        return topicId;
    }

    public void setTopicId(String topicId) {
        this.topicId = topicId;
    }

    public int getSeqNo() {
        return seqNo;
    }

    public void setSeqNo(int seqNo) {
        this.seqNo = seqNo;
    }

    public String getDataTypeWfx() {
        return dataTypeWfx;
    }

    public void setDataTypeWfx(String dataTypeWfx) {
        this.dataTypeWfx = dataTypeWfx;
    }

    public String getAvatarLarge() {
        return avatarLarge;
    }

    public void setAvatarLarge(String avatarLarge) {
        this.avatarLarge = avatarLarge;
    }

    public String getHbaseId() {
        return hbaseId;
    }

    public void setHbaseId(String hbaseId) {
        this.hbaseId = hbaseId;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Date getIndexTime() {
        return indexTime;
    }

    public void setIndexTime(Date indexTime) {
        this.indexTime = indexTime;
    }

    public Date getPublished() {
        return published;
    }

    public void setPublished(Date published) {
        this.published = published;
    }

    public String getLabelCity() {
        return labelCity;
    }

    public void setLabelCity(String labelCity) {
        this.labelCity = labelCity;
    }

    public String getLabelStock() {
        return labelStock;
    }

    public void setLabelStock(String labelStock) {
        this.labelStock = labelStock;
    }

    public String getLabelEminentPerson() {
        return labelEminentPerson;
    }

    public void setLabelEminentPerson(String labelEminentPerson) {
        this.labelEminentPerson = labelEminentPerson;
    }

    public String getLabelBrand() {
        return labelBrand;
    }

    public void setLabelBrand(String labelBrand) {
        this.labelBrand = labelBrand;
    }

    public String getLabelCompany() {
        return labelCompany;
    }

    public void setLabelCompany(String labelCompany) {
        this.labelCompany = labelCompany;
    }

    public String getLabelTrade() {
        return labelTrade;
    }

    public void setLabelTrade(String labelTrade) {
        this.labelTrade = labelTrade;
    }

    public String getLabelInterest() {
        return labelInterest;
    }

    public void setLabelInterest(String labelInterest) {
        this.labelInterest = labelInterest;
    }

    public String getLabelType() {
        return labelType;
    }

    public void setLabelType(String labelType) {
        this.labelType = labelType;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDataType() {
        return dataType;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitleTemp() {
        return titleTemp;
    }

    public void setTitleTemp(String titleTemp) {
        this.titleTemp = titleTemp;
    }

    public String getTitleHs() {
        return titleHs;
    }

    public void setTitleHs(String titleHs) {
        this.titleHs = titleHs;
    }

    public Set<String> getTitleSet() {
        return titleSet;
    }

    public void setTitleSet(Set<String> titleSet) {
        this.titleSet = titleSet;
    }

    public String getKeys() {
        return keys;
    }

    public void setKeys(String keys) {
        this.keys = keys;
    }

    public String getFilterkeys() {
        return filterkeys;
    }

    public void setFilterkeys(String filterkeys) {
        this.filterkeys = filterkeys;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getContentTemp() {
        return contentTemp;
    }

    public void setContentTemp(String contentTemp) {
        this.contentTemp = contentTemp;
    }

    public String getPublishedHour() {
        return publishedHour;
    }

    public void setPublishedHour(String publishedHour) {
        this.publishedHour = publishedHour;
    }

    public String getPublishedDay() {
        return publishedDay;
    }

    public void setPublishedDay(String publishedDay) {
        this.publishedDay = publishedDay;
    }

    public String getPublishedMonth() {
        return publishedMonth;
    }

    public void setPublishedMonth(String publishedMonth) {
        this.publishedMonth = publishedMonth;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getAstEdittime() {
        return astEdittime;
    }

    public void setAstEdittime(String astEdittime) {
        this.astEdittime = astEdittime;
    }

    public String getDeluser() {
        return deluser;
    }

    public void setDeluser(String deluser) {
        this.deluser = deluser;
    }

  

    public String getRskeys() {
        return rskeys;
    }

    public void setRskeys(String rskeys) {
        this.rskeys = rskeys;
    }

    public String getAliasTitle() {
        return aliasTitle;
    }

    public void setAliasTitle(String aliasTitle) {
        this.aliasTitle = aliasTitle;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getAuthorRaw() {
        return authorRaw;
    }

    public void setAuthorRaw(String authorRaw) {
        this.authorRaw = authorRaw;
    }

    public String getOriginWebsiteName() {
        return originWebsiteName;
    }

    public void setOriginWebsiteName(String originWebsiteName) {
        this.originWebsiteName = originWebsiteName;
    }

    public String getOriginWebsiteUrl() {
        return originWebsiteUrl;
    }

    public void setOriginWebsiteUrl(String originWebsiteUrl) {
        this.originWebsiteUrl = originWebsiteUrl;
    }

    public Integer getComments() {
        return comments;
    }

    public void setComments(Integer comments) {
        this.comments = comments;
    }

    public Integer getCommentatorNumber() {
        return commentatorNumber;
    }

    public void setCommentatorNumber(Integer commentatorNumber) {
        this.commentatorNumber = commentatorNumber;
    }

    public Integer getShareNumber() {
        return shareNumber;
    }

    public void setShareNumber(Integer shareNumber) {
        this.shareNumber = shareNumber;
    }

    public Date getLastEdittime() {
        return lastEdittime;
    }

    public void setLastEdittime(Date lastEdittime) {
        this.lastEdittime = lastEdittime;
    }

    public String getWebpageScreenshot() {
        return webpageScreenshot;
    }

    public void setWebpageScreenshot(String webpageScreenshot) {
        this.webpageScreenshot = webpageScreenshot;
    }

    public String getWebpageUrl() {
        return webpageUrl;
    }

    public void setWebpageUrl(String webpageUrl) {
        this.webpageUrl = webpageUrl;
    }

    public String getCaptureWebsiteName() {
        return captureWebsiteName;
    }

    public void setCaptureWebsiteName(String captureWebsiteName) {
        this.captureWebsiteName = captureWebsiteName;
    }

    public Integer getCaptureWebsiteId() {
        return captureWebsiteId;
    }

    public void setCaptureWebsiteId(Integer captureWebsiteId) {
        this.captureWebsiteId = captureWebsiteId;
    }

    public String getContentPlate() {
        return contentPlate;
    }

    public void setContentPlate(String contentPlate) {
        this.contentPlate = contentPlate;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public Integer getCollectionNumber() {
        return collectionNumber;
    }

    public void setCollectionNumber(Integer collectionNumber) {
        this.collectionNumber = collectionNumber;
    }

    public Date getLastCtime() {
        return lastCtime;
    }

    public void setLastCtime(Date lastCtime) {
        this.lastCtime = lastCtime;
    }

    public Date getSourcePublished() {
        return sourcePublished;
    }

    public void setSourcePublished(Date sourcePublished) {
        this.sourcePublished = sourcePublished;
    }

    public String getWeiboId() {
        return weiboId;
    }

    public void setWeiboId(String weiboId) {
        this.weiboId = weiboId;
    }

    public String getOriginAuthorId() {
        return originAuthorId;
    }

    public void setOriginAuthorId(String originAuthorId) {
        this.originAuthorId = originAuthorId;
    }

    public String getSourceClient() {
        return sourceClient;
    }

    public void setSourceClient(String sourceClient) {
        this.sourceClient = sourceClient;
    }

    public String getSourceUrl() {
        return sourceUrl;
    }

    public void setSourceUrl(String sourceUrl) {
        this.sourceUrl = sourceUrl;
    }

    public Date getLastForwardTime() {
        return lastForwardTime;
    }

    public void setLastForwardTime(Date lastForwardTime) {
        this.lastForwardTime = lastForwardTime;
    }

    public String getForwarderId() {
        return forwarderId;
    }

    public void setForwarderId(String forwarderId) {
        this.forwarderId = forwarderId;
    }

    public String getForwarderContent() {
        return forwarderContent;
    }

    public void setForwarderContent(String forwarderContent) {
        this.forwarderContent = forwarderContent;
    }

    public String getForwarderAuthorId() {
        return forwarderAuthorId;
    }

    public void setForwarderAuthorId(String forwarderAuthorId) {
        this.forwarderAuthorId = forwarderAuthorId;
    }

    public String getForwarderAuthorName() {
        return forwarderAuthorName;
    }

    public void setForwarderAuthorName(String forwarderAuthorName) {
        this.forwarderAuthorName = forwarderAuthorName;
    }

    public String getForwarderWeiboUrl() {
        return forwarderWeiboUrl;
    }

    public void setForwarderWeiboUrl(String forwarderWeiboUrl) {
        this.forwarderWeiboUrl = forwarderWeiboUrl;
    }

    public Date getForwardTime() {
        return forwardTime;
    }

    public void setForwardTime(Date forwardTime) {
        this.forwardTime = forwardTime;
    }

    public Integer getForwardComments() {
        return forwardComments;
    }

    public void setForwardComments(Integer forwardComments) {
        this.forwardComments = forwardComments;
    }

    public Integer getForwardForwardNumber() {
        return forwardForwardNumber;
    }

    public void setForwardForwardNumber(Integer forwardForwardNumber) {
        this.forwardForwardNumber = forwardForwardNumber;
    }

    public String getSourceGeo() {
        return sourceGeo;
    }

    public void setSourceGeo(String sourceGeo) {
        this.sourceGeo = sourceGeo;
    }

    public Integer getWeiboAccountId() {
        return weiboAccountId;
    }

    public void setWeiboAccountId(Integer weiboAccountId) {
        this.weiboAccountId = weiboAccountId;
    }

    public String getCommentId() {
        return commentId;
    }

    public void setCommentId(String commentId) {
        this.commentId = commentId;
    }

    public String getAuthorNick() {
        return authorNick;
    }

    public void setAuthorNick(String authorNick) {
        this.authorNick = authorNick;
    }

    public String getContentOrder() {
        return contentOrder;
    }

    public void setContentOrder(String contentOrder) {
        this.contentOrder = contentOrder;
    }

    public String getSameNum() {
        return sameNum;
    }

    public void setSameNum(String sameNum) {
        this.sameNum = sameNum;
    }

    public boolean isAddPro() {
        return isAddPro;
    }

    public void setAddPro(boolean isAddPro) {
        this.isAddPro = isAddPro;
    }

    public String getMajorID() {
        return majorID;
    }

    public void setMajorID(String majorID) {
        this.majorID = majorID;
    }

    public String getCaptureWebsite() {
        return captureWebsite;
    }

    public void setCaptureWebsite(String captureWebsite) {
        this.captureWebsite = captureWebsite;
    }

    public String getAuthoracount() {
        return authoracount;
    }

    public void setAuthoracount(String authoracount) {
        this.authoracount = authoracount;
    }

    public String getOriginAuthorName() {
        return originAuthorName;
    }

    public void setOriginAuthorName(String originAuthorName) {
        this.originAuthorName = originAuthorName;
    }

    public Integer getPraiseNum() {
        return praiseNum;
    }

    public void setPraiseNum(Integer praiseNum) {
        this.praiseNum = praiseNum;
    }

    public Integer getAnswerNum() {
        return answerNum;
    }

    public void setAnswerNum(Integer answerNum) {
        this.answerNum = answerNum;
    }

    public String getWebsiteCode() {
        return websiteCode;
    }

    public void setWebsiteCode(String websiteCode) {
        this.websiteCode = websiteCode;
    }

    public String getProfileImageUrl() {
        return profileImageUrl;
    }

    public void setProfileImageUrl(String profileImageUrl) {
        this.profileImageUrl = profileImageUrl;
    }

    public String getParentUserScreenName() {
        return parentUserScreenName;
    }

    public void setParentUserScreenName(String parentUserScreenName) {
        this.parentUserScreenName = parentUserScreenName;
    }

    public Integer getBiFollowersCount() {
        return biFollowersCount;
    }

    public void setBiFollowersCount(Integer biFollowersCount) {
        this.biFollowersCount = biFollowersCount;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public String getIsRecommended() {
        return isRecommended;
    }

    public void setIsRecommended(String isRecommended) {
        this.isRecommended = isRecommended;
    }

    public boolean isRead() {
        return isRead;
    }

    public void setRead(boolean isRead) {
        this.isRead = isRead;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public List<String> getPictureList() {
        return pictureList;
    }

    public void setPictureList(List<String> pictureList) {
        this.pictureList = pictureList;
    }

    public List<String> getVideoList() {
        return videoList;
    }

    public void setVideoList(List<String> videoList) {
        this.videoList = videoList;
    }

    public String getUserCustomFlag1() {
        return userCustomFlag1;
    }

    public void setUserCustomFlag1(String userCustomFlag1) {
        this.userCustomFlag1 = userCustomFlag1;
    }

    public String getReadFlag() {
        return readFlag;
    }

    public void setReadFlag(String readFlag) {
        this.readFlag = readFlag;
    }

    public String getReadUser() {
        return readUser;
    }

    public void setReadUser(String readUser) {
        this.readUser = readUser;
    }

    public String getIsNormalData() {
        return isNormalData;
    }

    public void setIsNormalData(String isNormalData) {
        this.isNormalData = isNormalData;
    }

    public String getWebColumnName() {
        return webColumnName;
    }

    public void setWebColumnName(String webColumnName) {
        this.webColumnName = webColumnName;
    }

    public String getContentAddress() {
        return contentAddress;
    }

    public void setContentAddress(String contentAddress) {
        this.contentAddress = contentAddress;
    }

    public String getOriginType() {
        return originType;
    }

    public void setOriginType(String originType) {
        this.originType = originType;
    }

    public Date getCaptureTime() {
        return captureTime;
    }

    public void setCaptureTime(Date captureTime) {
        this.captureTime = captureTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public long getContentId() {
        return contentId;
    }

    public void setContentId(long contentId) {
        this.contentId = contentId;
    }

    public String getPublishedString() {
        return publishedString;
    }

    public void setPublishedString(String publishedString) {
        this.publishedString = publishedString;
    }

    public int getCustomHot1() {
        return customHot1;
    }

    public void setCustomHot1(int customHot1) {
        this.customHot1 = customHot1;
    }

    public int getRepeatNum() {
        return repeatNum;
    }

    public void setRepeatNum(int repeatNum) {
        this.repeatNum = repeatNum;
    }

    public int getRepeatPosition() {
        return repeatPosition;
    }

    public void setRepeatPosition(int repeatPosition) {
        this.repeatPosition = repeatPosition;
    }

    public String getRepeatFirstID() {
        return repeatFirstID;
    }

    public void setRepeatFirstID(String repeatFirstID) {
        this.repeatFirstID = repeatFirstID;
    }

    public String getRepeatLastID() {
        return repeatLastID;
    }

    public void setRepeatLastID(String repeatLastID) {
        this.repeatLastID = repeatLastID;
    }

    public String getCustomFlag1() {
        return customFlag1;
    }

    public void setCustomFlag1(String customFlag1) {
        this.customFlag1 = customFlag1;
    }

    public boolean isNegative() {
        return negative;
    }

    public void setNegative(boolean negative) {
        this.negative = negative;
    }

    public boolean isFavorite() {
        return favorite;
    }

    public void setFavorite(boolean favorite) {
        this.favorite = favorite;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public String getNoRepeat() {
        return noRepeat;
    }

    public void setNoRepeat(String noRepeat) {
        this.noRepeat = noRepeat;
    }

    public String getLocationAddress() {
        return locationAddress;
    }

    public void setLocationAddress(String locationAddress) {
        this.locationAddress = locationAddress;
    }

    public String getSecondTrade() {
        return secondTrade;
    }

    public void setSecondTrade(String secondTrade) {
        this.secondTrade = secondTrade;
    }

    public boolean isWeiboHeadline() {
        return weiboHeadline;
    }

    public void setWeiboHeadline(boolean weiboHeadline) {
        this.weiboHeadline = weiboHeadline;
    }

	/**
	 * @return the commentsTemp
	 */
	public Double getCommentsTemp() {
		return commentsTemp;
	}

	/**
	 * @param commentsTemp the commentsTemp to set
	 */
	public void setCommentsTemp(Double commentsTemp) {
		this.commentsTemp = commentsTemp;
	}

	public String getCustomFlagFinance() {
		return customFlagFinance;
	}

	public void setCustomFlagFinance(String customFlagFinance) {
		this.customFlagFinance = customFlagFinance;
	}

	public String getCustomFlagEnergy() {
		return customFlagEnergy;
	}

	public void setCustomFlagEnergy(String customFlagEnergy) {
		this.customFlagEnergy = customFlagEnergy;
	}

	public String getCustomFlagBeauty() {
		return customFlagBeauty;
	}

	public void setCustomFlagBeauty(String customFlagBeauty) {
		this.customFlagBeauty = customFlagBeauty;
	}

	public String getCustomFlagTech() {
		return customFlagTech;
	}

	public void setCustomFlagTech(String customFlagTech) {
		this.customFlagTech = customFlagTech;
	}

	public String getCustomFlagLive() {
		return customFlagLive;
	}

	public void setCustomFlagLive(String customFlagLive) {
		this.customFlagLive = customFlagLive;
	}

	public String getCustomFlagGames() {
		return customFlagGames;
	}

	public void setCustomFlagGames(String customFlagGames) {
		this.customFlagGames = customFlagGames;
	}

	public String getCustomFlagBaby() {
		return customFlagBaby;
	}

	public void setCustomFlagBaby(String customFlagBaby) {
		this.customFlagBaby = customFlagBaby;
	}

	public String getCustomFlagFashion() {
		return customFlagFashion;
	}

	public void setCustomFlagFashion(String customFlagFashion) {
		this.customFlagFashion = customFlagFashion;
	}

	public String getCustomFlagTravel() {
		return customFlagTravel;
	}

	public void setCustomFlagTravel(String customFlagTravel) {
		this.customFlagTravel = customFlagTravel;
	}

	public String getCustomFlagCulture() {
		return customFlagCulture;
	}

	public void setCustomFlagCulture(String customFlagCulture) {
		this.customFlagCulture = customFlagCulture;
	}

	public String getCustomFlagGov() {
		return customFlagGov;
	}

	public void setCustomFlagGov(String customFlagGov) {
		this.customFlagGov = customFlagGov;
	}

	public String getCustomFlagZhaobiao() {
		return customFlagZhaobiao;
	}

	public void setCustomFlagZhaobiao(String customFlagZhaobiao) {
		this.customFlagZhaobiao = customFlagZhaobiao;
	}

	public String getCustomFlagMil() {
		return customFlagMil;
	}

	public void setCustomFlagMil(String customFlagMil) {
		this.customFlagMil = customFlagMil;
	}

	public String getCustomFlagHouse() {
		return customFlagHouse;
	}

	public void setCustomFlagHouse(String customFlagHouse) {
		this.customFlagHouse = customFlagHouse;
	}

	public String getCustomFlagLottery() {
		return customFlagLottery;
	}

	public void setCustomFlagLottery(String customFlagLottery) {
		this.customFlagLottery = customFlagLottery;
	}

	public String getCustomFlagEnt() {
		return customFlagEnt;
	}

	public void setCustomFlagEnt(String customFlagEnt) {
		this.customFlagEnt = customFlagEnt;
	}

	public String getCustomFlagMedical() {
		return customFlagMedical;
	}

	public void setCustomFlagMedical(String customFlagMedical) {
		this.customFlagMedical = customFlagMedical;
	}

	public String getCustomFlagHealth() {
		return customFlagHealth;
	}

	public void setCustomFlagHealth(String customFlagHealth) {
		this.customFlagHealth = customFlagHealth;
	}

	public String getCustomFlagSports() {
		return customFlagSports;
	}

	public void setCustomFlagSports(String customFlagSports) {
		this.customFlagSports = customFlagSports;
	}

	public String getCustomFlagSifa() {
		return customFlagSifa;
	}

	public void setCustomFlagSifa(String customFlagSifa) {
		this.customFlagSifa = customFlagSifa;
	}


	
	


}
