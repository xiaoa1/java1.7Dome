package xiaoa.java.es.utils;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import org.apache.lucene.index.Term;
import org.apache.lucene.search.BooleanClause.Occur;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.TermQuery;

import com.alibaba.fastjson.JSON;

import xiaoa.java.es.bean.Params;
import xiaoa.java.es.client.EsClient;

/**
 * es搜索工具
 * @author lkc
 * @date 2017年11月14日 下午9:16:38
 * @version V1.0
 *
 */
public class EsSearchUtils {
	
	
	/**
	 * 获取type
	 * @Title: getType
	 * @param params
	 * @return
	 * @author lkc
	 */
	public static  String getType(Params params){
		
		return "content";
	}
	
	/**
	 * 获取分片
	 * @Title: getSearchIndex
	 * @param params
	 * @return
	 * @author lkc
	 */
	public static String[] getSearchIndex(Params params) {

		
		List<String> indexList = new ArrayList<String>();
		
		indexList.add("mx");
		
		LinkedHashMap<String, Object>  logMap = new LinkedHashMap<>();
		
		logMap.put("indexs", JSON.toJSONString(indexList));
		
		EsClient.info("searchIndex", logMap);
		
		return indexList.toArray(new String[indexList.size()]);
	}
	/**
	 * 获取索引时间字段名字
	 * @Title: getIndexField
	 * @param params
	 * @return
	 * @author lkc
	 */
	public static String getIndexField(Params params){
		
		return "IndexTime";
		
	}
	
	
	/**
	 * 创建查询query
	 * @Title: createLuceneQuery
	 * @param params
	 * @return
	 * @author xiaoa
	 */
	public static Query createLuceneQuery(Params params){
		
		if (params == null){
			return null;
		}
		
		BooleanQuery.Builder  query = new BooleanQuery.Builder();
		
		if (params.getUserName() != null && !params.getUserName().isEmpty()){
			query.add(new TermQuery(new Term("userName", params.getUserName())), Occur.MUST);
		}
		
		return query.build();
		
	}
	
	
}
