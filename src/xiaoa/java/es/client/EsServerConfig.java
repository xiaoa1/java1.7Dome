package xiaoa.java.es.client;

/**
 *es 服务器配置
 * @author lkc
 * @date 2017年11月11日 下午9:56:16
 * @version V1.0
 *
 */
public class EsServerConfig {

	/**
	 * key
	 */
    private int key;
	
	/**
	 * 主机名
	 */
	private String host ;
	
	/**
	 * 端口
	 */
	private int port;
	
	/**
	 * 懒加载 
	 */
	private boolean lazy = true;
	
	/**
	 * 集群名字
	 */
	private String clusterName;
	
	public EsServerConfig(){}

	public EsServerConfig(String host , int port , String clusterName , int key ){
		this.host = host;
		this.port = port;
		this.clusterName = clusterName;
		this.key = key;
	}
	
	
	
	
	
	public boolean isLazy() {
		return lazy;
	}





	public void setLazy(boolean lazy) {
		this.lazy = lazy;
	}





	public int getKey() {
		return key;
	}




	public void setKey(int key) {
		this.key = key;
	}




	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public String getClusterName() {
		return clusterName;
	}

	public void setClusterName(String clusterName) {
		this.clusterName = clusterName;
	}

	/**
	 * 检查
	 * @Title: check
	 * @return
	 * @author lkc
	 */
	public String check(){
		if  (host == null || host.isEmpty()){
			return "host is empty";
		}
		

		if  (port <= 0){
			return "port is " + port;
		}
		
		if  (clusterName == null || clusterName.isEmpty()){
			return "clusterName is empty";
		}
		
		return null;
	}
	

}
