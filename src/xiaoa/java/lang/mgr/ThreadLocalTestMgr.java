package xiaoa.java.lang.mgr;

/**
 * 线程局部 (thread-local) 变量
 * @author xiaoa
 */
public class ThreadLocalTestMgr {
	
	// 申明一个ThreadLocal对象
	private static ThreadLocal<String>  local   =  null ;
	
	// 初始化ThreadLocal对象
	static {
		local  =  new ThreadLocal<String>();
	}
	
	/**
	 * 设置一个值
	 * @param str
	 */
	public static  void  set(String  str){
		local.set(str);
	}
	
	/**
	 * 返回一个值
	 */
	public static String  get(){
		return local.get();
	}

}
