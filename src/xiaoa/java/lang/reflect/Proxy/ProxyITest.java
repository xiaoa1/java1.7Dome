package xiaoa.java.lang.reflect.Proxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;

import xiaoa.java.InvocationHandlers.InvocationHandlerTest;
import xiaoa.java.interfaces.ProxyTest;

/**
 * java 动态代理测试类
 * @author lkc
 *
 */
public class ProxyITest {
	
	
	
	public static void main(String[] args) throws Throwable {
		
		
		// 创建一个代理实例的调用处理接口
		InvocationHandler   invo  =  new  InvocationHandlerTest();
		
		// 返回指定接口实例代理类
		Object   testobj   =  Proxy.newProxyInstance(ProxyTest.class.getClassLoader() , new Class[]{ProxyTest.class} , invo);  
		
		System.out.println(testobj instanceof  ProxyTest );
		
		ProxyTest   test   =    (ProxyTest)testobj;
		
		test.test(111, 11+"");
		
		
		
	}
	

}
