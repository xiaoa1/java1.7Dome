package xiaoa.java.lang.test;

import java.util.ArrayList;
import java.util.Queue;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * 队列测试
 * @author lkc
 *
 */
public class ThreadQueueTest<T>  extends  ArrayList<T> implements Queue<T>{
	
	//  class 版本
	private static final long serialVersionUID = 1L;

	//  创建一个锁
	Lock    lock   =   new ReentrantLock();
	
	//  为添加线程创建一个锁
	Condition  addLock  =    lock.newCondition();
	
	//  为get线程创建一个锁
	Condition  getLock  =    lock.newCondition();
	
	// 集合中最多有100个线程
	public final int maxSize =  100;
	
	// 当前取队列位置
	public  int getIndex   =  0;
	
	// 当前添加队列位置
	public  int addIndex   =  0;
	
	// 当前队列长度
	public   int count     =  0 ;

	
	/***
	 * 向线程池中添加一个对象
	 */
	@Override
	public boolean add(T e) {
		
		if(e==null ){
		     return false;
		}
		
		try {
			
			// 开启一个锁	
			lock.lock();
			
			// 如果池子已满  当前线程休眠
			if(count == maxSize){
				
				System.out.println("----------------  线程休眠:"+Thread.currentThread().getName());
			    addLock.await();
			}
			
			//  如果当前list长度不等于maxSize  则调用添加方法  
			if(size() != maxSize){
				super.add(e);
			}else{
				super.set(addIndex, e);
			}
			
			//  将addIndex 设置为下一个下标
			//  如果当前已经到了最大下标
			if(addIndex == maxSize-1 ){
				//  初始化为 0
				addIndex = 0;
			}else{
				addIndex ++ ;
			}
			
			//  当前队列长度加1
			count ++ ;
			
			// 唤醒get 方法
			getLock.signal();
			
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}finally {
			// 释放锁
			lock.unlock();
		}
		
		return true;
	}


	/**
	 * 获取队列中一个
	 */
	public T get(){
		
		// 声明返回值对象
		T  t  =  null ;
		// 开启一个锁
		lock.lock();
		
		try {
			// 如果当前队列中没有
			if(count == 0){
				System.out.println("----------------  处理线程休眠:"+Thread.currentThread().getName());

				// 当前线程休眠
			    getLock.await();
			}
				
			// 设置下一次要取的下标位置
			//  获取队列中  getIndex 下标的对象
		    t  =  super.get(getIndex);
			
			//  如果已经取到数组的最大下标
			if(getIndex == maxSize-1){
				
				// 下标指向 0 位置
				getIndex  = 0 ;
			}else{
				getIndex ++ ;
			}
			
			//  池子容量 -1
			count --;
			
			// 唤醒添加锁
			addLock.signal();
			
		
		} catch (InterruptedException e) {
			e.printStackTrace();
		}finally{
			// 释放锁
			lock.unlock();
		}

		// 返回值
		return t;
	}


	@Override
	public boolean offer(T e) {
		
		return false;
	}


	@Override
	public T remove() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public T poll() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public T element() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public T peek() {
		// TODO Auto-generated method stub
		return null;
	}

	
	
	


}
