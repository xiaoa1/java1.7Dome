package xiaoa.java.lang.test;

import java.util.HashMap;


import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;import jnr.ffi.Struct.int16_t;

public class MapTest {
	
	
	public static void main(String[] args) throws Throwable{
//		
		//  map长度
	    Long  mapSize  =  100000l;
    	
//    	Map<String, String >  map   =  new Hashtable<String , String>();
    	
    	//testMap(map, mapSize);
//    	//  
//    	testTheadMap(map, mapSize, 50l, 50l);
//		
//		lindkedHashMapTest();
	    
	    Map<String, Integer>  map = new HashMap<>();
	    
	    Integer[] ss = new Integer[1000];
	    
	    
	    for (int i  = 0 ; i < 1000 ; i ++){
	    	
	    	map.put("map_", i);
	    	
	    	ss[i] = i;
	    	
	    }
	    {
	    	  
		    long start = System.currentTimeMillis();
		    
		    for (int i =0 ; i < 100 * 10000 ; i ++){
		    	
		    	map.get("map_12");
		    	
		    }
		    
		    System.out.println("mapTime = " + (System.currentTimeMillis() - start));
	    }
    	
	    
	    {
	    	  long start = System.currentTimeMillis();
	  	    
	  	     for (int i =0 ; i < 100 * 10000 ; i ++){
	  	    	
	  	    	int dd = ss[i < 1000 ? i : 999];
	  	    	
	  	    }
	  	    
	  	    System.out.println("ssTime = " + (System.currentTimeMillis() - start));
	      	
	    }
    
    	
		
	}
	

    
    //  测试 map性能
    public static void testMap(Map<String, String>  map  , Long mapSize ){
    	
    	
//    	// put操作所耗费时间
//    	long  putTime  =   0;
//    	
//    	// for操作所耗费时间
//    	long  forTime  =   0;
//    	
//    	// 迭代器操作所耗费时间
//    	long  itTime  =   0;


    	
        //  获取开始时间戳
   // 	long  putBeginTime  =  System.currentTimeMillis();
    	
    	for(int count = 0 ; count <  mapSize ; count++ ){
    		
    		map.put("key_"+count, "value");
    	}
    	
    	
   // 	long  forBeginTime  =  System.currentTimeMillis();
    	
   // 	System.out.println( "put end   class = "  + map.getClass().getName() + "            put  = " + (putTime =  forBeginTime - putBeginTime )) ;

    	for(String key : map.keySet() ){
    		map.get(key);
    		//System.out.println("  for     key = " + key +"   value "+map.get(key));
    	}
    	
    //	long  forEndTime  =  System.currentTimeMillis();

    //	System.out.println( "class = "  + map.getClass().getName() + "            put  = " + (putTime =  forBeginTime - putBeginTime ) +"         for = " + (forTime = forEndTime - forBeginTime)) ;
    	
    }
    
    
    //  测试 map性能
    public static void testTheadMap(final Map<String, String>  map  , final Long mapSize  , final Long forThreadSize ,  final Long putThreadSize ){
    	
    	
    	// 
        final Map<String, Long>   timeMap  =  new HashMap<String , Long>();
        
        // put操作所耗费时间
        timeMap.put("putTime", 0l);
        
        // for操作所耗费时间
        timeMap.put("forTime", 0l);
        
        // 迭代器操作所耗费时间
        timeMap.put("itTime", 0l);
        
        // 循环开始时间
        timeMap.put("forBeginTime", 0l);
        
        // 循环结束时间
        timeMap.put("forEndTime", 0l);

        //  获取开始时间戳
        timeMap.put("putBeginTime", 0l);

    	
        
    	timeMap.put("forBeginTime", System.currentTimeMillis())  ;
    	
    	for(  int  putThreadSizeIndex  = 0 ;  putThreadSizeIndex < putThreadSize ;  putThreadSizeIndex ++  ){
    		
    		new  Thread(new  Runnable() {
				public void run() {

			    	for(int count = 0 ; count <  mapSize ; count++ ){
			    		
			    		map.put( Thread.currentThread().getName()+"key_"+count, "value");
			    	}
					
			    	// 填充时间
			    	timeMap.put("forBeginTime", System.currentTimeMillis())  ;

			    	System.out.println( "class = "  + map.getClass().getName() + "            put  = " + (timeMap.put("putTime" ,   timeMap.get("forBeginTime") -  timeMap.get("putBeginTime") ) ) ) ;
			    	System.out.print("putTime  = " + timeMap.get("putTime"));
			    	
				}
			} , "thread_"+putThreadSizeIndex).start();
    		
    	}
    	
    	
    	System.out.println("   put 线程创建完毕");
    	
     
    	
   
    }
    
    
    
    
    public  static void  lindkedHashMapTest()throws Throwable{
    	
    	Map<String , String >   linkedMap  =  new LinkedHashMap<String , String>();
    	
    	linkedMap.put("2", "2");
    	linkedMap.put("3", "3");
    	linkedMap.put("4", "4");
    	linkedMap.put("1", "1");
    	
    	linkedMap.get("1");
    	linkedMap.get("1");
    	linkedMap.get("1");
    	linkedMap.get("1");
    	
    	
    	linkedMap.get("2");
    	linkedMap.get("2");
    	linkedMap.get("2");

      	linkedMap.get("3");
    	linkedMap.get("3");
    	
      	linkedMap.get("4");
      	linkedMap.get("4");
      	linkedMap.get("4");
      	linkedMap.get("4");
      	linkedMap.get("4");
      	linkedMap.get("4");
      	linkedMap.get("4");
      	linkedMap.get("4");
      	linkedMap.get("4");
      	linkedMap.get("4");
      	linkedMap.get("4");
      	linkedMap.get("4");


      	
      	Iterator<String>   it  =   linkedMap.keySet().iterator();
      	
      	
      	
      	while(it.hasNext()){
      		
      		System.out.println("key =  " + it.next());
      	}
      	
      	
      	for(String key  : linkedMap.keySet()){
      		
      		System.out.println("key = "+key );
      		
      	}
      	
    	
    	
    	
    }
   
    
    
    
    
    
    
    

}
