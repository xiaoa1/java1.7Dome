package xiaoa.java.lang.test;

import java.util.ArrayList;
import java.util.List;

import xiaoa.java.log.L;

public class QueueTest {

	
	/**
	 * 
	 * @param args
	 */
	public static void main(String[] args) throws Throwable{
		
	
		//  正在处理线程 数量
	   final  Integer[] handleNum      = new Integer[]{0};
	    
	    //  正在休眠处理线程
	   final Integer[] handleSleepNum  = new Integer[]{0};
	    
		//  正在处理线程 数量
	   final Integer[] userNum         = new Integer[]{0};
	    
	    //  正在休眠处理线程
	   final Integer[] userSleepNum    = new Integer[]{0};
		
		// 创建队列list
		final ThreadQueueTest<Long>    list  =   new ThreadQueueTest<Long>();
		
	   // 创建一个添加  Runnable 对象
		Runnable   addRun  =   new Runnable() {
			
			//  创建一个公共的TimeKey
			public Long timeKey  =  0l ;
			
			//  增量
			public Integer  increment  = 1000;
			
			// 获取timeKey 范围
			public List<Long>  getTimeKey(){
				
				//  范围   0 下标为 开始返回   1 为结束范围
				List<Long>  range   =  new ArrayList<Long>();
				
				synchronized (this) {
					// 开始值
					range.add( timeKey);
					
					// 结束值
					range.add( timeKey= timeKey + increment);
					
					// 赋值下一次的结束值
					timeKey++;
				}
				
				return  range;
				
			}
			
			
			@Override
			public void run() {
				try {
				
					while(true){
						
						// 获取范围
						List<Long>  range  =  getTimeKey();
						
						for(Long  index  = range.get(0) ; index <= range.get(1) ; index ++){
							
							userNum[0] ++; 
							
							// 向队列数组中添加
							list.add(index);
							L.info(Thread.currentThread().getName()+"===============  add  "+index);
							
							// 线程休眠 100 秒     目的模仿多个用户   使用随机数  看起来比较随机
							int  minute  =  (int)(Math.random()*100);
							
							// 获取当前线程名字
							String name =    Thread.currentThread().getName();
							
							// 设置当前线程名字
							Thread.currentThread().setName(name+"    休眠时间："+minute);
							
							userNum[0] --; 
							
							userSleepNum[0]++;
							
							// 线程休眠
							Thread.sleep(1000*minute);
							
							userSleepNum[0]--;
							
						}
						
					}
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
		};
		
		//  循环模仿多个用户  用户量  100
		for(int index =0 ; index <10000 ; index ++){

			// 创建一个添加线程
			Thread  addThread  =  new Thread( addRun , "线程 : "+index);
			
			// 开启添加线程
			addThread.start();
			
		}
		
		
		
		//  主线程休息10秒后开始处理
		Thread.sleep(10*1000);
		
		
		Runnable   getRun   =   new Runnable() {
			@Override
			public void run() {
					try {
						// 向队列数组中获取
						
						while(true){
							
							handleNum[0] ++ ;
							
						    Long 	num =   list.get();
							System.out.println("===============  get  "+num);
							
							handleNum[0] -- ;
							
							handleSleepNum[0] ++ ;
							// 线程休眠 1 秒
							Thread.sleep(100);
							
							handleSleepNum[0] -- ;

							// 获取当前线程名字
							String name =    Thread.currentThread().getName();
							
							System.out.println("----  处理线程 ： "+name);
							
						}
						
						
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
				
				
			}
		};
		
		//  创建处理线程   多个处理线程  
		for(int index =0 ; index <100 ; index ++){

			// 创建一个添加线程
			Thread  getThread  =  new Thread( getRun , "线程 : "+index);
			
			// 开启添加线程
			getThread.start();
			
		}
		
	
		
	}
		
	
	
		
		
	
	
}
