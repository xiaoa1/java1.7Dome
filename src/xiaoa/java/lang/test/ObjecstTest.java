package xiaoa.java.lang.test;


import java.util.HashMap;

import java.util.Map;


import xiaoa.java.bean.Chinese;
import xiaoa.java.bean.Student;
import xiaoa.java.bean.User;
import xiaoa.java.interfaces.FinalUser;
import xiaoa.java.log.L;
import xiaoa.java.task.RunnableTest;

public class ObjecstTest {
	
	
	
	//  线程延时多久开启
    public 	static int  delayedTimeout  =   10000;
	
	
	/**
	 * clone  测试
	 */
	public static void  cloneTest()throws Throwable{
		
		//  创建一个用户对象
		User    user   =   new   User();
		
		// 填充参数
		user.age          =   12;
		user.describe     =   "这是一个男人";
		user.sex          =   FinalUser.SEX_MAN;
		
		
		//  采用Cloneable  
		User  clone_NewUser      =   (User)user.clone();
		
		System.out.println(clone_NewUser.toString());
		
	    //  采用Cloneable  
		User  clone_NewUser1     =   user.cloneSerialize();
		
		System.out.println(clone_NewUser1.toString());
		
		
		//  创建一个学生对象
		Student   sutdent    =   new  Student();
		sutdent.age          =   12;
		sutdent.describe     =   "这是一个男人";
		sutdent.sex          =   FinalUser.SEX_MAN;
		sutdent.name         =   "小a";
		
		
		//  创建一个语文对象
		Chinese   chinese   =   new   Chinese();
		
		chinese.good        =  (float) 96.6;
		chinese.comment     =   "考的不错，下次继续努力";
		
		//  将语文对象赋值给学生对象
		sutdent.chinese     =   chinese ;
		
		
		{
			//  浅层clone 对象
			Student   shallowClone  =  (Student)sutdent.clone(); 
			
			// 改变clone前对象内chinese 内的值
			sutdent.chinese.good    =  (float)59; 
			sutdent.chinese.comment =  "还没有及格啊，，仍需努力啊"; 
			
			System.out.println(shallowClone.toString());
			System.out.println("shallowClone.chinese.good  =   "+ shallowClone.chinese.good);
			System.out.println("shallowClone.chinese.comment  =   "+ shallowClone.chinese.comment);		

		}
		
		{
			
		    //  恢复之前对象
			sutdent.chinese.good        =  (float) 96.6;
			sutdent.chinese.comment     =   "考的不错，下次继续努力";
			
			//  深层clone 对象
			Student   serializableClone  =  (Student)sutdent.cloneSerialize(); 
			
			// 改变clone前对象内chinese 内的值
			sutdent.chinese.good    =  (float)59; 
			sutdent.chinese.comment =  "还没有及格啊，，仍需努力啊"; 
			
			System.out.println(serializableClone.toString());
			System.out.println("serializableClone.chinese.good     =   "+ serializableClone.chinese.good);
			System.out.println("serializableClone.chinese.comment  =   "+ serializableClone.chinese.comment);		
			
			
		}
	
		
		
		
	}
	
	
	
	
	/**
	 * hashCode 测试
	 */
	public static void hashCodeTest(){
		
		
		String  str1  = "小a11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111";
		
		String  str2  = "小a";
		
		
		String  str3  = "小a";

		
		L.info("str1.hashCode = "+str1.hashCode() +"     str2.hashCode = "+str2.hashCode() +"     st3.hashCode = "+str3.hashCode());

		
		L.info("str1.hashCode = "+str1.hashCode() +"     str2.hashCode = "+str2.hashCode() +"     st3.hashCode = "+str3.hashCode());
		L.info("str1.hashCode = "+str1.hashCode() +"     str2.hashCode = "+str2.hashCode() +"     st3.hashCode = "+str3.hashCode());
		L.info("str1.hashCode = "+str1.hashCode() +"     str2.hashCode = "+str2.hashCode() +"     st3.hashCode = "+str3.hashCode());

		
		
	}
	
	
	
	@Override
	protected void finalize() throws Throwable {
		// TODO Auto-generated method stub
		
		
		System.out.println(this.getClass());
		
		super.finalize();
	}


	
	/**
	 * 线程休眠测试
	 * @throws Throwable
	 */
	public  static   void  waitTest()throws Throwable{
		
		// 创建同步, 目的为了唤醒线程
		synchronized ( RunnableTest.class) {


			
			//  创建线程池
			Map< Thread, String>   threadPool   =   new  HashMap<Thread, String>();
					
		    int count =  1  ;
            
            while(count<100){
            	
            	  //  创建类
            	  RunnableTest    test2 =   new RunnableTest("线程--"+count);
            	  
                  //  创建线程
                  Thread      thread2   =   new  Thread(test2 , test2.threadName);
            	  
                  
            	  if(count%3==0)
            		  //  休眠10 秒
            	       test2.timeout = delayedTimeout + 100000;
            	  else
            		   test2.timeout  = delayedTimeout + 1000;
            	  
            	  //  将线程加入池子中
            	  threadPool.put(thread2, test2.threadName);
            	  
            	  
            	  // 开启线程
            	  thread2.start();
            	
                  System.out.println("count  =  "+count);
                  count++;
            }
            
            System.out.println("线程创建成功");
            
            System.out.println("主线程开始休眠");
            
            //  主线程休眠
            RunnableTest.class.wait(delayedTimeout+10000);
            
            System.out.println("开始唤醒线程");
            
            // 循环线程
            for( Thread   t1  : threadPool.keySet() ){
            	
            	//  如果当前线程处于休眠状态
            	if(t1.getState()  == Thread.State.TIMED_WAITING ){
            		
            		System.out.println("准备唤醒线程  ： "+t1.getName());
            		// 唤醒线程
            		//t1.notify();
            		RunnableTest.class.notify();
            		
            		System.out.println("唤醒线程  ： "+t1.getName()+"    成功");
            	}else{
            		
            		// 线程状态
            		System.out.println("  name  =  "+t1.getName()+"  线程状态 "+t1.getState() );
            	}
            	
            }
            
       
            
		}
		
            
            
            
            
		    
		
		
	}
	
	
	// 同步代码块
	public static  void  threadSync(String  threadName , int  timeout )throws Throwable{
		
		
		// 同步代码块
	    synchronized(ObjecstTest.class){
	    	
		     System.out.println("线程名字:"+threadName+"     休眠时间 :"+(timeout-delayedTimeout));
		     
		     // 获取 object 线程锁 休眠线程
		   
		}
	}
	
	
	
	
	
	
	



	public static void main(String[] args)throws Throwable {
		
		// clone 测试  
		//cloneTest();
		
		// hashCode 测试
		//hashCodeTest();
		
		
		// wait  测试
		waitTest();
	}
	
	

}
