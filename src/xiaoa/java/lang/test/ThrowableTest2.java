package xiaoa.java.lang.test;

public class ThrowableTest2 {
	
	public static  void test(Throwable  thr){
		
		 Throwable   runtime1  =  new RuntimeException("异常信息1");
		    
		// ThrowableTest.throwableTest(runtime1, "异常信息1");
			
		 Throwable   runtime2  =  new RuntimeException(runtime1);
			
		 ThrowableTest.throwableTest(runtime2, "异常信息2");
		 
		 // 获取异常链
		 ThrowableTest.throwableTest(runtime2.getCause(), "获取异常链");
		 
		 //ThrowableTest.throwableTest(thr, "传入信息");

	}
	

}
