package xiaoa.java.lang.test;

/**
 * 异常信息测试类
 * @author lkc
 *
 */
public class ThrowableTest {
	
	
	public static void throwableTest(Throwable  runtime1 ,String throwableName){
	    
		System.out.println("=======================   异常名：  " + throwableName);
		
		//   创建此 throwable 的本地化描述。
		System.out.println("getLocalizedMessage = " +runtime1.getLocalizedMessage());
		
		//   返回此 throwable 的详细消息字符串。
		System.out.println("getMessage = " +runtime1.getMessage());
		
		//   输出堆栈的跟踪信息
		StackTraceElement[]   stacks  =  runtime1.getStackTrace();
		
		System.out.println("开始打印节点信息");
		for( StackTraceElement  i :stacks){
			
			// 返回类的完全限定名，该类包含由该堆栈跟踪元素所表示的执行点。
			System.out.println("getClassName()   "  + i.getClassName());

			// 返回方法名
			System.out.println("getMethodName()   "  + i.getMethodName());
			
			
			// 返回源文件 ， 该文件包含由该堆栈跟踪元素所表示的执行点。
			System.out.println("getFileName()   "  + i.getFileName());
			
			// 返回行号
		    System.out.println("getLineNumber()   "  + i.getLineNumber());
		    
			
		}

		
		//    将此 throwable 及其追踪输出至标准错误流。
		System.out.println(" 将此 throwable 及其追踪输出至标准错误流。");
		runtime1.printStackTrace() ;
		
		
	}
	
	
	public static void  test(){
		
	    Throwable   runtime1  =  new RuntimeException("传入信息");
	    
	    ThrowableTest1.test(runtime1);
	
	}
	
	
	
	public static void main(String[] args) {
		test();
	}
	
	

}
