package xiaoa.java.lang.test;

import xiaoa.java.lang.mgr.ThreadLocalTestMgr;

public class ThreadLocalTest {
	
	public static void main(String[] args) {
		
		
		Runnable   run1   =  new Runnable() {
			
			@Override
			public void run() {
				
				try {
					ThreadLocalTestMgr.set("线程"+(int)(Math.random()*100));
					
					Thread.sleep(1000);

					System.out.println(ThreadLocalTestMgr.get());
					
				} catch (Throwable e) {
					e.printStackTrace();
				}
				
			}
		};
		
		
       Runnable   run2   =  new Runnable() {
			
			@Override
			public void run() {
				
				ThreadLocalTestMgr.set("线程2");
				
				System.out.println(ThreadLocalTestMgr.get());
				
			}
		};
		
		// 创建一个线程
		Thread  thread1   =  new  Thread(run1);
		
		// 启动一个线程
		thread1.start();
		
		
		// 创建一个线程
		Thread  thread2   =  new  Thread(run2);
		
		// 启动一个线程
		thread2.start();
		
		
	}
	

}
