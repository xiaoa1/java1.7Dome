package xiaoa.java.lang.concurrent;

import java.io.Serializable;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.AbstractQueuedSynchronizer;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;

/**
 * 测试   AbstractQueuedSynchronizer
 * @author xiaoa
 * @date 2017年2月15日 下午1:13:38
 * @version V1.0
 *
 */
public class TestAbstractQueuedSynchronizer implements Lock , Serializable {

	
	private static final long serialVersionUID = 1L;
	
	/**
	 *  自定义一个内部内 同步操作
	 * @author xiaoa
	 * @date 2017年2月15日 下午1:24:44
	 * @version V1.0
	 *
	 */
	public static class Sync extends AbstractQueuedSynchronizer{

		private static final long serialVersionUID = 1L;
		
		/**
		 * 判断否处于占用状态
		 */
		@Override
		public boolean isHeldExclusively(){
			
			return getState() == 1;
		}
		
		
		/**
		 * 尝试获取锁
		 */
		@Override
		public boolean tryAcquire(int acquire) {
			
			if(acquire != 1 ){
				throw new RuntimeException("参数有误");
			}
			
			// 获取锁成功
			if(compareAndSetState(0, acquire)){
		         setExclusiveOwnerThread(Thread.currentThread());
		         return true;
			}
			
			return false;
		}
		
		
		
	}
	

	@Override
	public void lock() {
		
	}

	@Override
	public void lockInterruptibly() throws InterruptedException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean tryLock() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean tryLock(long time, TimeUnit unit) throws InterruptedException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void unlock() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Condition newCondition() {
		// TODO Auto-generated method stub
		return null;
	}

}
