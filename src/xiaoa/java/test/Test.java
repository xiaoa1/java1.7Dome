package xiaoa.java.test;

import java.util.*;

import com.alibaba.fastjson.JSON;

/**
 * Created by Administrator on 2017/2/27.
 */
public class Test {

    public static void main(String[] args) throws  Throwable{

    	
    	System.out.println(0xFFFF);



    }
    
    public static void error(){
    	try {
			
			
    	    throw	new RuntimeException("错误");
			
		} catch (Exception e) {
             throw new RuntimeException(e);
		}
    }
    

    public static void test() throws  Throwable {
    	

    	try {
    		try {
    			
    			
    			error();				
			} catch (Exception e) {
                 throw new RuntimeException(e);
			}
		
    		
		} catch (Exception e) {
			
			String error = exceptionToString(e);

			System.out.println(error);
			
			e.printStackTrace();

		}
    }

    
    public static String exceptionToString(Throwable  e){
		
    	Map<String, List<String>>  exceMap = new LinkedHashMap<String, List<String>>();
    	
		// 拼接最上层错误信息
		List<String>  topList = new ArrayList<>();
		exceMap.put("Exception : " + e + "]" , topList);
		StackTraceElement[] ste = e.getStackTrace();
		for (int k = 0; k < ste.length; k++) {
			topList.add("at "+ ste[k].toString());
		}
		
		// 拼接原因
		Throwable cause = e;
		while ( ( cause = cause.getCause() ) != null){
			
			List<String>  causeList = new ArrayList<>();
			exceMap.put("cause by : " + cause , causeList);
			StackTraceElement[] st = cause.getStackTrace();
			for (int k = 0; k < st.length; k++) {
				causeList.add(" at " + st[k].toString());
			}
		}
		
		// 将错误信息转为json
		String json = JSON.toJSONString(exceMap);
		
		return json;
	}
	

}
