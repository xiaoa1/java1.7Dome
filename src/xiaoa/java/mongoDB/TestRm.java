package xiaoa.java.mongoDB;

import java.util.ArrayList;

import java.util.Arrays;
import java.util.List;

import org.bson.Document;
import org.bson.conversions.Bson;

import com.mongodb.BasicDBObject;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.model.Aggregates;
import com.mongodb.client.model.BsonField;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Sorts;

import xiaoa.java.log.L;
import xiaoa.java.spider.db.vo.FetchUrl;

public class TestRm {
	
	public static void main(String[] args)throws Throwable {
		
		// 初始化数据库
		DbMgr.init("xiaoa", 27017);
		DbMgr.initDatabase("fetchEs");
		
		long sumCount = 0;
		
		// 获取表 
		MongoCollection<Document>   coll = DbMgr.getMongoCollection(DbUtils.getCollectionName(FetchUrl.class));
		
		int page = 1 ;
		int pageSize = 100;
		
		long sysStartTime = System.currentTimeMillis();
		
		List<String>  urlList = findRepeatList(page, pageSize);
		
		while(urlList != null && urlList.size() > 0){
			
			long startTime = System.currentTimeMillis();
			
			Bson query = Filters.and(Filters.in("url", urlList) , Filters.eq("state", FetchUrl.STATE_WAIT));
			
			long count = coll.deleteMany(query).getDeletedCount();
			
			sumCount += count;
			
			L.info(startTime,  System.currentTimeMillis(),"delete count:" + count + " ");
			
			// 下一个批次
			page ++;
			urlList = findRepeatList(page, pageSize);
			
		}
		
		
		L.info(sysStartTime , System.currentTimeMillis() , "完成 count = " + sumCount);
	}

	/**
	 * 寻找url
	 * @Title: findRepeat
	 * @param page
	 * @param pageSize
	 * @return
	 * @author xiaoa
	 */
	public static List<String>  findRepeatList(int page , int pageSize)throws Throwable{
		
		  MongoCollection<Document>   mongoCollection = DbMgr.getMongoCollection(DbUtils.getCollectionName(FetchUrl.class));

		  // 通道
		  List<Bson>  pipline =  Arrays.asList(
							              Aggregates.group("$url",  new BsonField("count", new BasicDBObject("$sum", 1) ))
									     ,Aggregates.match(Filters.gt("count", 1))	
									     ,Aggregates.sort(Sorts.descending("count"))
									     ,Aggregates.skip((page - 1 ) * pageSize)
									     ,Aggregates.limit(pageSize)
						                 );
		  
		  MongoCursor<Document>  docIt =  mongoCollection.aggregate(pipline).iterator();
		  
		  List<String>  urlList = new ArrayList<>();
		  
		  while(docIt.hasNext()){
			  
			  Document doc = docIt.next();
			  
			  urlList.add(doc.getString("_id"));
			  
			  L.info("url = " + doc.getString("_id") + "  count = " + doc.getInteger("count"));
			  
		  }
		  
		  return urlList;
		
	}
	
	
}
