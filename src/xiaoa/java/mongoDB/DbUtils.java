package xiaoa.java.mongoDB;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.persistence.Id;

import org.bson.Document;

/**
 * db工具类
 * @author xiaoa
 * @date 2017年10月8日 上午8:07:05
 * @version V1.0
 *
 */
public class DbUtils {
	
	 /**
     * 对象转doc
     * @Title: objToDocument
     * @param obj
     * @return
     * @throws Throwable
     * @author xiaoa
     */
    public static Document objToDocument(Object obj )throws Throwable{
    	
        // 创建集合描述文件
        Document   mongoDocument = new Document();

        
        Field[] fs = xiaoa.java.field.FieldUtils.getAllFields( obj.getClass());
        
        for (Field field : fs){
        	
        	field.setAccessible(true);
        	
        	if (!isValidField(field)){
        		continue;
        	}

            Object value = field.get(obj);

            if (value != null){
                mongoDocument.append(field.getName() , value);
            }

        }
        
        return mongoDocument;
    }
    
    /**
     * 对象list转list
     * @Title: objToDocument
     * @param list
     * @return
     * @throws Throwable
     * @author xiaoa
     */
    public static List<Document> objToDocumentList(List<?> list)throws Throwable{
    	
        List<Document>  documentList = new ArrayList<>(list.size());
        
        for (Object  obj : list){
        	
        	if ( obj == null){
        		continue;
        	}
        	
        	documentList.add(objToDocument(obj));
        }
        
        return documentList;
    }
    
    
    /**
     * 判断是否是有效字段
     * @Title: isValidField
     * @param f
     * @return
     * @author xiaoa
     */
    private static boolean isValidField(Field f){
    	
    	return !Modifier.isStatic(f.getModifiers());
    }
    
    
    /**
     * document 对象转obj
     * @Title: docToObj
     * @param cla
     * @param doc
     * @return
     * @throws Throwable
     * @author xiaoa
     */
    public static <TT> TT  docToObj(Class<TT> cla , Document doc)throws Throwable{
    	
        // 定义一个对象
        TT  obj  =  null;
        
        Set<String>  keySet = doc.keySet();
      	// 获取属性
        Field[]  fs = xiaoa.java.field.FieldUtils.getAllFields(cla);

   
        for (Field f : fs ){
        	
        	if (!isValidField(f)){
        		continue;
        	}
        	
        	f.setAccessible(true);
        	
        	String key =  f.getName(); 
          
           if (obj == null){
               // 根据class创建一个实例
               obj = cla.newInstance();
           }
           
           // 如果是id
           if (f.getAnnotation(Id.class) != null){
        	   
               f.set(obj , doc.get("_id").toString());
               
           }else{

        	   if (keySet.contains(key)){
        		   // 设置值到 实例中
                   f.set(obj , doc.get(key));
        	   }
           }

        	
        }
        
     
        return obj;
    }
    
    
    
    /**
     * 获取
     * @Title: getCollectionName
     * @param cla
     * @return
     * @author xiaoa
     */
    public static String getCollectionName(Class<?> cla){
    	
    	return cla.getSimpleName();
    }

}
