package xiaoa.java.sql.oodb.pool;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.mchange.v2.c3p0.ComboPooledDataSource;

import xiaoa.java.log.L;
import xiaoa.java.sql.oodb.Create;

/**
 * 数据库链接池
 * @author lkc
 *
 */
public class SqlSessionPool {
	
	// 线程池
	private static Map<String, ComboPooledDataSource>   sessionPool  =  new  HashMap<String, ComboPooledDataSource>();
	
	// 表map  key为表路径  map 为tag
	private static Map<String, String>                    tableMap  =  new  HashMap<String,String>();

	

	/**
	 * 添加一个链接池
	 * @param tag
	 * @param driverClass
	 * @param url
	 * @param userName
	 * @param password
	 * @throws Throwable
	 */
	public static void add(String tag , String driverClass ,  String url , String userName , String password  )throws Throwable{
		add(tag, driverClass, url, userName, password , null);
	}

	
	/**
	 * 添加一个连接池
	 * @param tag
	 * @param url
	 * @param userName
	 * @param password
	 */
	public static void add(String tag , String driverClass ,  String url , String userName , String password  ,  PoolParameter poolParameter )throws Throwable{
		
		// 判断参数
		if(isEmptyString(tag) || isEmptyString(driverClass)  ||isEmptyString(url) || isEmptyString(userName) ||  isEmptyString(password)){
			L.info("==== add   tag " + tag + " driverClass =" + driverClass + " url =" + url + "   userName =" + userName + "  password =" + password  );
		    throw new RuntimeException("参数有误 : " + "==== add   tag " + tag + " url =" + url + "   userName =" + userName + "  password =" + password );
		}
		
		// 创建一个池对象
		ComboPooledDataSource    pool =  new ComboPooledDataSource();
		
		// 设置参数
		
		// 设置驱动路径
		pool.setDriverClass(driverClass);
		
		// 设置url
		pool.setJdbcUrl(url);
		
		// 设置用户名
		pool.setUser(userName);
		
		// 设置密码
		pool.setPassword(password);
		
		// 如果配置文件不为空
		if(poolParameter != null ){
			
			// 设置最大链接数
			pool.setMaxPoolSize(poolParameter.max);
			
			// 设置最小链接数
			pool.setMinPoolSize(poolParameter.min);
			
			// -定义在从数据库获取新连接失败后重复尝试的次数 Default: 30
			pool.setAcquireRetryAttempts(poolParameter.acquireRetryAttempts);
			
			// 当连接池中的连接耗尽的时候c3p0一次同时获取的连接数  Default: 3 
			pool.setAcquireIncrement(poolParameter.acquireIncrement);

			// 两次连接中间隔时间，单位毫秒。Default: 1000 
			pool.setAcquireRetryDelay(poolParameter.acquireRetryDelay);

			// -连接关闭时默认将所有未提交的操作回滚。Default: false
			pool.setAutoCommitOnClose(poolParameter.autoCommitOnClose);

		}
		
		// 将连接添加到池子中
	    sessionPool.put(tag, pool);
		
	}
	
	/**
	 * 获取一个数据库链接
	 * @param tag
	 * @return
	 * @throws Throwable
	 */
	public static  Connection  getConnection(Class<?> cla )throws Throwable{
		
		return getConnection(getTag(cla));
	}

	
	/**
	 * 获取一个数据库链接
	 * @param tag
	 * @return
	 * @throws Throwable
	 */
	public static  Connection  getConnection(String tag )throws Throwable{
		
		if(isEmptyString(tag)){
			throw new RuntimeException("参数有误！");
		}
		
		// 获取池子对象
		ComboPooledDataSource  pool  =  sessionPool.get(tag) ;
		if( pool == null ){
			throw new RuntimeException("该链接池不存在！");
		}
		
		// 返回链接
		return pool.getConnection();
		
	}
	
	
	/**
	 * 添加表
	 * @param tag
	 * @param cla
	 */
	public static void addTable(String tag , Class<?> cla)throws Throwable{
		
		if( isEmptyString(tag) || cla == null ){
			throw new RuntimeException("参数有误！");
		}
		
		tableMap.put(cla.getName(), tag);
	}
	
	/**
	 * 获取表所在tag
	 * @param tag
	 * @param cla
	 */
    public static String getTable(Class<?> cla){
		
		if(cla == null ){
			throw new RuntimeException("参数有误！");
		}
		
		return tableMap.get(cla.getName());
	}
	
	/**
	 * 池参数
	 * @author xiaoa
	 *
	 */
	public static class PoolParameter {
		
		// 最大连接数
		public  Integer max = 20;
		
		// 最小连接数
		public  Integer min = 1;
		
		// -定义在从数据库获取新连接失败后重复尝试的次数 Default: 30
		public  Integer acquireRetryAttempts = 30;
		
		// 当连接池中的连接耗尽的时候c3p0一次同时获取的连接数  Default: 3 
		public  Integer acquireIncrement = 3;

		// 两次连接中间隔时间，单位毫秒。Default: 1000 
	    public  Integer acquireRetryDelay  = 1000;
	    
	    //-连接关闭时默认将所有未提交的操作回滚。Default: false
        public  Boolean  autoCommitOnClose = false;

		
	}
	
	/**
	 * 获取表tag
	 * @param cla
	 * @return
	 */
	public static String  getTag(Class<?> cla){
		
		if(cla == null ){
			throw new RuntimeException("参数有误！");
		}
		
		return tableMap.get(cla.getName());
		
	}
	
	/**
	 * 获取所有表classList
	 * @return
	 * @throws Throwable
	 */
	public static List<Class<?>>  getTableClassList()throws Throwable{
		
		List<Class<?> > list  =  new ArrayList<Class<?> >();
		
		for(String  className : tableMap.keySet()){
			// 获取class
			Class<?>  cla =  Class.forName(className);
			list.add(cla);
		}
		
		return list;
		
	}
	
	
	/**
	 * 判断字符串是否为空
	 * @param value
	 * @return
	 */
	private static boolean isEmptyString(String value ){
		
		return value==null || value.trim().equals("");
		
	} 
	
	
	/**
	 * 初始化表
	 * @throws Throwable
	 */
	public static void initTable()throws Throwable{
		
		for(Class<?>  cla : SqlSessionPool.getTableClassList()){
			// 创建表
			Create.create(cla);
		}
	}
	

}
