package xiaoa.java.sql.oodb.mgr;

import java.sql.Connection;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import xiaoa.java.log.L;
import xiaoa.java.sql.oodb.pool.SqlSessionPool;


/**
 *数据库链接管理器
 * @author xiaoa
 */
public class SqlSessionMgr {
	
	//  线程  数据库链接管理器  session 对象和线程绑定
	private static ThreadLocal<Map<String, Connection>>   connlocal  =  null;
	
	// 线程  保存一个线程处理的config
	private static ThreadLocal<Config>   configlocal                 =  null;

	
	static{
		// 初始化管理器
		connlocal    =  new ThreadLocal<Map<String ,Connection>>();
		
		configlocal  =  new ThreadLocal<Config>();
		configlocal.set(new Config());
	}
	
	/**
	 * 开启事务
	 */
	public static void beginTransaction(){
		
		// 设置为开启事务
		getConfig().transaction  = true;
	}
	
    /**
     * 提交事务
     */
	public static void commitTransaction(){

		// 获取所有连接
		Map<String, Connection>   connMap  =  getConnMap();
		
		for(String key : connMap.keySet()){
			Connection  conn  =  connMap.get(key);
			
			try {
				if(conn != null && !conn.isClosed() ){
					conn.commit();
				}
			} catch (SQLException e) {
                L.info("=========commitTransaction  事务提交失败 " );
				e.printStackTrace();
			}
			
		}
		
	}
    
	/**
	 * 回滚事务
	 */
	public static void rollBackTransaction(){
		
		// 获取所有连接
		Map<String, Connection>   connMap  =  getConnMap();
		
		for(String key : connMap.keySet()){
			Connection  conn  =  connMap.get(key);
			
			try {
				if(conn != null && !conn.isClosed() ){
					conn.rollback();;
				}
			} catch (SQLException e) {
                L.info("=========rollBackTransaction  事务回滚失败 " );
				e.printStackTrace();
			}
			
		}
	}
	
	
	/**
	 * 获取一个数据库链接
	 * @param tag
	 * @return
	 * @throws Throwable
	 */
	public  static  Connection getConnection(String tag )throws Throwable{
		return getConnection(tag , getConfig().transaction);
	}
	
	/**
	 * 获取一个数据库链接
	 * @param tag
	 * @return
	 * @throws Throwable
	 */
	public  static  Connection getConnection(Class<?> cla )throws Throwable{
		
		// 获取对象tag
		String  tag  = SqlSessionPool.getTag(cla);
		
		return getConnection(tag, getConfig().transaction);
	}
	
	/**
	 * 获取一个链接
	 * @param tag
	 * @return
	 * @throws Throwable
	 */
	public  static  Connection getConnection(String tag ,  boolean autoCommit)throws Throwable{
		
		if(tag == null || tag.trim().equals("")){
			throw new RuntimeException("参数有误！");
		}
		
		//  获取数据库链接map
		Map<String, Connection>   connMap  =  getConnMap();
		
		// 如果没有当前tag 链接 或者链接已经关闭  
		if(connMap.get(tag) == null  || connMap.get(tag).isClosed()){

			// 获取当前数据库链接
			connMap.put(tag, SqlSessionPool.getConnection(tag));
		}
		
		// 获取链接
		Connection  conn  =  connMap.get(tag);
		
		// 设置是否自动提交
		if(autoCommit){
			conn.setAutoCommit(false);
		}
		
		return conn;
	}
	
	/**
	 *获取线程存储 connMap
	 * @return
	 */
	private static Map<String, Connection>  getConnMap(){
		// 从线程中获取数据库链接map
		Map<String, Connection>   connMap  =  connlocal.get();
		
		// 如果map对象还没有创建  创建一个map
		if(connMap == null ){
			connMap   =  new  HashMap<String, Connection>();
			connlocal.set(connMap);
		}
		
		return connMap;
		
	}
	
	/**
	 * 获取此次线程配置
	 * @return
	 */
	private static Config getConfig(){
		return configlocal.get();
	}
	
	
	/**
	 * 配置config    仅适用于线程范围内
	 * @author xiaoa
	 *
	 */
	private static class Config{
		
		// 是否开启事务
		public boolean  transaction  = false;
		
	}
	
	/**
	 * 获取一个系统key
	 * @return
	 */
	public static long  createKey(){
		
		return System.currentTimeMillis() + ((int)Math.random()*100000);
		
	} 
	
	
	
	

}
