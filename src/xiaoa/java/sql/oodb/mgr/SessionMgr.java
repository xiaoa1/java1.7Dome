package xiaoa.java.sql.oodb.mgr;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;
import xiaoa.java.log.L;

/**
 * 会话管理
 * @author xiaoa
 *
 */
public class SessionMgr {
	
	
	/**
	 * 执行更新语句
	 * @param sql
	 * @return
	 * @throws Throwable
	 */
	public static int executeUpdate(String sql , Class<?> cla)throws Throwable{
		
		if(sql == null  || sql.trim().equals("")){
			throw new RuntimeException("参数有误");
		}
		
		// 获取数据库链接
		Connection               conn =  SqlSessionMgr.getConnection(cla);
		
		return conn.createStatement().executeUpdate(sql);
	}
	
	 /**
	  * 执行预编译更新语句
	  * @param sql
	  * @param list
	  * @param cla
	  * @return
	  * @throws Throwable
	  */
	public  static int preparedExecuteUpdate(String sql , List<Object>  list ,  Class<?> cla)throws Throwable{
				if(sql == null || list == null || list.size() <= 0 ){
			L.info("======= sql =" + sql + " cla =" + cla);
			throw new RuntimeException("参数有误");
		}
		
		// 获取数据库链接
		Connection               conn =  SqlSessionMgr.getConnection(cla);
		
		// 预编译sql
		PreparedStatement    prepared =   conn.prepareStatement(sql);
		
		// 填充参数
		fillParam(list, prepared);
		
		// 执行
		return prepared.executeUpdate();
		
	}
	
	/**
	 * 执行预编译查询	
	 * @param sql
	 * @param list
	 * @param cla
	 * @return
	 * @throws Throwable
	 */
	public  static ResultSet preparedExecuteQuery(String sql , List<Object>  list ,  Class<?> cla)throws Throwable{
		
		if(sql == null || list == null || list.size() <= 0 ){
			L.info("======= sql =" + sql + " cla =" + cla);
		}
		
		// 获取数据库链接
		Connection               conn =  SqlSessionMgr.getConnection(cla);
		
		// 预编译sql
		PreparedStatement    prepared =   conn.prepareStatement(sql);
		
		// 填充参数
		fillParam(list, prepared);
		
		// 执行
		return prepared.executeQuery();			
	}

	/**
	 * 填充参数
	 * @param list
	 * @param prepared
	 */
	private static void fillParam(List<Object>  list ,PreparedStatement    prepared  )throws Throwable{
		
        for(int i = 1 ; i <= list.size() ; i ++){
			
			 Object  value = list.get(i-1);
			
			if( value instanceof Integer ){
				prepared.setInt(i,(Integer) value);
			
			}else if( value instanceof Long ){
				prepared.setLong(i,(Long) value);
				
			}else if( value instanceof Float ){
				prepared.setFloat(i,(Float) value);
				
			}else if( value instanceof Double ){
				prepared.setDouble(i,(Double) value);
				
			}else if( value instanceof BigDecimal ){
				prepared.setBigDecimal(i,(BigDecimal) value);
			
			}else if( value instanceof String ){
				prepared.setString(i,(String) value);
			}else{
				L.info("============  不认识类型");
			} 

		   // TODO　这里需要补充其他类型
			
		}
		
		
	}
		

}
