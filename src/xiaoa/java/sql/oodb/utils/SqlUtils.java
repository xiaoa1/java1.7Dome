package xiaoa.java.sql.oodb.utils;

import java.lang.reflect.Field;
import java.sql.Types;

import xiaoa.java.sql.oodb.annotation.FieldType;
import xiaoa.java.sql.oodb.annotation.Indexs;

/**
 * sql工具
 * @author xiaoa
 *
 */
public class SqlUtils {
	
	
	/**
	 * 获取注解列sql
	 * @param f
	 * @return
	 */
	public static String  getFieldTypeSql(Field  f){
		
		if(f == null || !f.isAnnotationPresent(FieldType.class)){
			return "";
		}
		
		// 获取注解
		FieldType  field    =  f.getAnnotation(FieldType.class);
		
		// 列名
		String  columnName  =  f.getName();
		
		// 类型
		String  type        =  getFieldType(field.type() , field.length());
		
		// sql
		StringBuilder   sql =  new StringBuilder();
		
		// 拼接sql
		sql.append(columnName).append(" ").append(type);
		
		//拼接注释
		sql.append(" COMMENT '").append(field.comment()).append("'  ");
		
		// 拼接索引   应该放在拼接行的最后
		switch (field.index()) {
			case Indexs.NO_INDEX:
				break;
				
			case Indexs.NORMAL_INDEX:
				sql.append(",").append("index (").append(columnName).append(")"); 	
				break;
						
			case Indexs.PRIMARY_KEY:
				sql.append(" PRIMARY KEY"); 	
				break;
	
			default:
				break;
		}
		
		
		return sql.toString();
		
		
	}
	
	/**
	 * 获取列类型字符串
	 * @param type
	 * @param length
	 * @return
	 */
	public static  String  getFieldType(int  type , int length){
		
		String  typeSql   =  "";
		
		switch (type) {
		
		case Types.BIGINT:
			
			if(length == -1){
				length = 20;
			}
			
			typeSql = "bigint("+length+")";
			
			break;
			
         case Types.INTEGER:
			
			if(length == -1){
				length = 7;
			}
			
			typeSql = "int("+length+")";
			
			break;	
			
         case Types.VARCHAR:
 			
 			if(length == -1){
 				length = 255;
 			}
 			
 			typeSql = "varchar("+length+")";
 			
 			break;		

		default:
			break;
		}
		
		return typeSql;
		
	} 
	
	
	
	
	

}
