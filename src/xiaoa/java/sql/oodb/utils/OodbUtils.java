package xiaoa.java.sql.oodb.utils;


import java.sql.SQLException;

import xiaoa.java.sql.oodb.pool.SqlSessionPool;

/**
 * O/R  utils
 * @author xiaoa
 *
 */
public class OodbUtils {
	

	// 检测表是否存在
	public static boolean hasTable(Class<?> cla){
		
		boolean   bu  =  false;
		
		// 获取表名
		String tableName  =  VoUtils.getTableName(cla);
		
		// 创建查询
		try {
			
			// 创建sql语句
			String sql   =  "select  count(1) from  " + tableName;
			
			// 执行查询，如果当前表不存在，则抛出异常
			SqlSessionPool.getConnection(cla).createStatement().executeQuery(sql);
			
			bu = true;
		} catch (SQLException e) {
			
			// 获取当前日志信息
			String  msg   =  e.getMessage();
			
			if(msg.lastIndexOf("Table") == 0 && msg.contains("doesn't exist")  ){
				
				System.out.println(tableName+"表不存在");
			}
			
		} catch (Throwable e) {
			e.printStackTrace();
		}
	
		return bu;
	}
		
		
	
	
}
