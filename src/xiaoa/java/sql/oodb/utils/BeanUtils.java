package xiaoa.java.sql.oodb.utils;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * beanUtils  
 * @author xiaoa
 *
 */
public class BeanUtils {
	
	
	/**
	 * 获取对象所有不为空属性值map
	 * @param o
	 * @return
	 * @throws Throwable
	 */
	public static Map<String, Object>  getBeanValue(Object  o)throws Throwable{
		
		return  getBeanValue(o, null);
	}

	
	/**
	 * 获取对象指定不为空属性值map
	 * @param o   对象
	 * @param nameList  筛选属性  
	 * @return
	 * @throws Throwable
	 */
	public static Map<String, Object>  getBeanValue(Object  o , List<String>  nameList)throws Throwable{
		
		if(o == null ){
			return new HashMap<String , Object>();
		}
		
		//  定义所有值的map
		Map<String, Object>   map  =  new  HashMap<String,Object>();
		
		// 获取所有属性值
		Field[]  fs  =  o.getClass().getFields();
		
		for(Field f : fs){
			
			// 设置可以读取私有属性
			f.setAccessible(true);
			
			// 获取属性值
			Object  value  = f.get(o);
			
			if(value !=  null ){
				
				// 如有没有有筛选
				if(nameList != null && nameList.size() != 0){
					
					// 如果筛选字段中包含该属性
					if(nameList.contains(f.getName())){
						map.put(f.getName(), value);
					}
					
				}else{
					
					map.put(f.getName(), value);
				}
				
			}
			
			
		}
		
	    return map;
 		
	}
	

}
