package xiaoa.java.sql.oodb.utils;


import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import xiaoa.java.sql.oodb.Config;
import xiaoa.java.sql.oodb.annotation.FieldType;
import xiaoa.java.sql.oodb.annotation.Table;
import xiaoa.java.sql.oodb.vo.VData;

/**
 * 表vo Utils
 * @author xiaoa
 *
 */
public class VoUtils {
	
	/**
	 * 获取表名
	 * @param cla
	 * @return
	 */
	public static String  getTableName(Class<?> cla){
		
		if(cla == null ){
			throw new RuntimeException("参数有误！");
		}
		
		// 获取表注解
		Table   tableAnn   =  cla.getAnnotation(Table.class);
		if(tableAnn == null ){
			throw new RuntimeException("该类不是一个数据库表！");
		}
		
		// 定义表名变量
		String  tableName  =  "";
		
		// 如果有设置表名
	    if(!tableAnn.tableName().equals("")){
	    	
	    	// 如果有
	    	tableName  = tableAnn.tableName();
	    	
	    }else{
	    	
	    	tableName  = Config.TABLE_PREFIX + cla.getSimpleName();
	    }
		
		return tableName;
		
	}
	
	
	/**
	 * 获取列名称
	 * @param cla
	 * @return
	 */
	public static List<String> getColumn(Class<?> cla){
		
		if(cla == null ){
			throw new RuntimeException("参数有误!");
		}
		
		// 获取属性
		List<Field>  fList        =   getFields(cla);
		
		List<String>  columList   =    new  LinkedList<String>();
		
		for(Field  f : fList){
			columList.add(f.getName());
		}
	
		return columList;
	}
	
	/**
	 * 获取列名称
	 * @param fList
	 * @return
	 */
	public static List<String> getColumn(List<Field>  fList){
			
			if(fList == null || fList.size() == 0 ){
				return new ArrayList<String>();
			}
			
			List<String>  columList   =    new  LinkedList<String>();
			
			for(Field  f : fList){
				columList.add(f.getName());
			}
		
			return columList;
	}
	
	/**
	 * 获取对象有效的属性
	 * @param fList
	 * @return
	 */
	public static List<String> getvalidColumn(List<Field>  fList , Object  obj)throws Throwable{
		
		if(fList == null || fList.size() == 0  || obj == null){
			return new ArrayList<String>();
		}
		
		List<String>  columList   =    new  LinkedList<String>();
		
		for(Field  f : fList){
			
			if(f.get(obj) != null){
				columList.add(f.getName());
			}
			
		}
	
		return columList;
	}
	
	/**
	 * 获取vo所有注解属性
	 * @param cla
	 * @return
	 */
	public static List<Field>  getFields(Class<?> cla){
		
		if(cla == null ){
			throw new RuntimeException("参数有误!");
		}
		
		// 获取所有属性field
		Field[]  fs         =  cla.getFields();
		
		// 返回注解属性
		List<Field>  list   =  new LinkedList<Field>();
		
		for(int  i = 0 ; i < fs.length ; i ++ ){
			
			Field f = fs[i];
			
			// 设置访问权限可是访问私有属性
			f.setAccessible(true);
			
			// 如果该属性设置了注解
			if(f.isAnnotationPresent(FieldType.class)){
				list.add(f);
			}
			
		}
		
		return  list;
		
	}
	
	/**
	 * 
	 * @param cla
	 * @return
	 */
     public static List<Field>  getClassFields(Class<?> cla){
		
		if(cla == null ){
			throw new RuntimeException("参数有误!");
		}
		
		// 获取所有属性field
		Field[]  fs         =  cla.getFields();
		
		// 返回注解属性
		List<Field>  list   =  new LinkedList<Field>();
		
		for(int  i = 0 ; i < fs.length ; i ++ ){
			
			Field f = fs[i];
			
			// 设置访问权限可是访问私有属性
			f.setAccessible(true);
		    
			list.add(f);
		}
		
		return  list;
		
	}
	
	
	/**
	 * 获取属性默认值
	 * @param f
	 * @return
	 */
	public static Object getDefaultColumValue(Field f){
		
		if(f == null ){
			throw new RuntimeException("参数有误!");
		}
		
		// 获取注解值
		FieldType   fn  =  f.getAnnotation(FieldType.class);
		
		return FieldTypeUtils.defaultValue(fn.defaultValue(), fn.type());
		
	}
	
	/**
	 * 获取属性值
	 * @param f
	 * @param obj
	 * @return
	 */
	public static Object getColumValue(Field f , Object obj)throws Throwable{
		
		if(obj == null ){
			return getDefaultColumValue(f);
		}
		
		Object value  =  f.get(obj);
		
		if(value != null){
			return value;
		}
		
		return getDefaultColumValue(f);

	}
	
	
	/**
	 * 获取vo主键名
	 * @param cla
	 * @return
	 */
	public static String getPrimaryKeyName(Class<?> cla){
		
		if(cla == null ){
			throw new RuntimeException("参数有误");
		}
		
		// TODO  这里需要补充
		return "timeKey";
	} 
	
	/**
	 * 获取主键value值
	 * @param obj
	 * @return
	 */
    public static Long getPrimaryKeyValue(Object  obj){
		
		if(obj == null ){
			throw new RuntimeException("参数有误");
		}
		
		Long key = 0l;
		
		// 如果该实例继承了VData
		if(obj instanceof VData){
			VData data  = (VData)obj;
			key         = data.timeKey;
		}
		
		// TODO  这里需要补充
		return key;
	} 
	

}
