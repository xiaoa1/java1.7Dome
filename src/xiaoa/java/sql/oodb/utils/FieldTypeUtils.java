package xiaoa.java.sql.oodb.utils;

import java.sql.Types;

/**
 * 将字符串转换为指定属性
 * @author xiaoa
 *
 */
public class FieldTypeUtils {
	
	/**
	 * 获取默认值
	 * @param value
	 * @param type
	 * @return
	 */
	public static Object defaultValue( String  value ,  int type ){
		
		// 如果是String类型
		if(type == Types.VARCHAR  || type == Types.LONGNVARCHAR || type == Types.NVARCHAR ){
			return value;
			
		}if(type == Types.INTEGER  ){
			if(value == null || value.trim().equals("")){
				return new Integer(0);
			}
			return new Integer(value);
			
		}else{
			return "";
		}
		
	}
	
	

}
