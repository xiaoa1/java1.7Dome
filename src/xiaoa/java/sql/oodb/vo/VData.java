package xiaoa.java.sql.oodb.vo;

import java.io.Serializable;
import java.sql.Types;

import xiaoa.java.sql.oodb.annotation.FieldType;
import xiaoa.java.sql.oodb.annotation.Indexs;


public class VData implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@FieldType(type = Types.BIGINT , comment = "主键" , index = Indexs.PRIMARY_KEY) 
	public Long timeKey;
	
}
