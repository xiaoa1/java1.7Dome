package xiaoa.java.sql.oodb;

import java.lang.reflect.Field;

import java.util.List;

import xiaoa.java.log.L;
import xiaoa.java.sql.oodb.annotation.Table;
import xiaoa.java.sql.oodb.mgr.SessionMgr;
import xiaoa.java.sql.oodb.utils.OodbUtils;
import xiaoa.java.sql.oodb.utils.SqlUtils;
import xiaoa.java.sql.oodb.utils.VoUtils;

public class Create {
	
	// 构造方法
	public Create() {
		
	}

    /**
     * 添加操作
     * @param t
     * @throws Throwable
     */
	public static void create(Class<?> cla)throws Throwable{
		
		if(cla == null ){
			throw new RuntimeException("参数有误");
		}
		
		// 判断该class 是否有表注解
		if(!cla.isAnnotationPresent(Table.class)){
			L.info("该类没有table注解");
			return ;
		}
		
		// 如果该表不存在
		if(!OodbUtils.hasTable(cla)){
			
			// 创建sql语句
			StringBuilder  sql  =  new StringBuilder("create table ").append(VoUtils.getTableName(cla)).append(" ( ");
			
			try {
				
				// 获取该对象所有属性
				List<Field>   fList  = VoUtils.getFields(cla);
				
				// 是否需要拼接 ,
				boolean  mosaic = false ;
				
				// 循环所有属性
				for( Field  f : fList){
					
					//  设置可以获取父类属性，以及private 属性
					f.setAccessible(true);
					
					// 获取sql
					String   fieldSql   =  SqlUtils.getFieldTypeSql(f);
					
					// 如果sql不为空
					if(fieldSql != null && !fieldSql.equals("")){
						
						if(mosaic){
							sql.append(",");
						}
						
						sql.append(fieldSql);
						
						// 第一次执行完后设置为true
						mosaic = true;
					}
					
				}
				
				// sql结尾
				sql.append(")");
				
				// 创建表执行语句
				SessionMgr.executeUpdate(sql.toString() , cla);
				
				L.info("创建表成功");
			
			} catch (Throwable e) {

				// 打印sql日志
				L.info("sql  =  " + sql);
				throw e;
			}
			
			
		}
		
	}

}
