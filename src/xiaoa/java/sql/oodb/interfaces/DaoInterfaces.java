package xiaoa.java.sql.oodb.interfaces;

import java.util.List;

/**
 * dao操作
 * @author xiaoa
 *
 */
public interface DaoInterfaces<T> {
	
	/**
	 * 添加一个对象  , 返回 成功数量
	 * @param data
	 * @return
	 * @throws Throwable
	 */
	public int insert(Object data)throws Throwable;
	
	/**
	 * 插入一组数据
	 * @param dataList
	 * @return
	 * @throws Throwable
	 */
	public int insert(List<Object> dataList)throws Throwable;
	
	
	/**
	 * 保存一个对象  , 返回 成功数量
	 * @param data
	 * @return
	 * @throws Throwable
	 */
	public int save(Object data)throws Throwable;
	
	/**
	 * 保存一组数据
	 * @param dataList
	 * @return
	 * @throws Throwable
	 */
	public int save(List<Object> dataList)throws Throwable;
	
	
	/**
	 * 判断一个对象是否存在
	 * @param timeKey
	 * @return
	 * @throws Throwable
	 */
	public boolean hasExists(Long timeKey)throws Throwable;
	

	/**
	 * 查询一组数据
	 * @param offset
	 * @param rows
	 * @return
	 * @throws Throwable
	 */
	public List<T> 	queryPage( int offset , int rows )throws Throwable;
	
	/**
	 * 查询一条
	 * @param offset
	 * @param rows
	 * @return
	 * @throws Throwable
	 */
	public T  queryOne( int offset , int rows )throws Throwable;
	
	/**
	 * 查询一组字段  
	 * @param offset
	 * @param rows
	 * @return
	 * @throws Throwable
	 */
	public List<T> queryPageField(int offset , int rows )throws Throwable;
	
	/**
	 * 统计数量
	 * @return
	 * @throws Throwable
	 */
    public int count()throws Throwable;

    /**
     * 计算一个字段和
     * @return
     * @throws Throwable
     */
    public Double sum()throws Throwable;
    
    

    /**
     * 计算一个字段最大值
     * @return
     * @throws Throwable
     */
    public Double max()throws Throwable;
    
    
    /**
     * 计算一个字段最小值
     * @return
     * @throws Throwable
     */
    public Double min()throws Throwable;
    
    /**
     * 设置sql
     * @param sql
     * @throws Throwable
     */
    public void seSql(StringBuilder  sql)throws Throwable;
    
	/**
	 * 设置参数
	 * @throws Throwable
	 */
    public void setParams(List<Object> params)throws Throwable;
    
    /**
     * 删除一个对象
     * @param timeKey
     * @throws Throwable
     */
    public void delete(Long timeKey)throws Throwable;
    
    
    /**
     * 条件删除一个对象
     * @param timeKey
     * @throws Throwable
     */
    public void delete( )throws Throwable;
    
    /**
     * 更新一个对象
     * @param obj
     * @throws Throwable
     */
    public void update(Object obj)throws Throwable;
    
    /**
     * 条件更新一个对象
     * @param obj
     * @throws Throwable
     */
    public void update( )throws Throwable;
    
    
}
