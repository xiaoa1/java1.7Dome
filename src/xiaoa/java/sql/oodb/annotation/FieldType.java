package xiaoa.java.sql.oodb.annotation;

import static java.lang.annotation.ElementType.CONSTRUCTOR;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.LOCAL_VARIABLE;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PACKAGE;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.ElementType.TYPE;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 数据库列注解
 * @author xiaoa
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(value={CONSTRUCTOR, FIELD, LOCAL_VARIABLE, METHOD, PACKAGE, PARAMETER, TYPE})
public @interface FieldType {
	
	/**
	 * 类型  参考 {@link java.sql.Types}
	 */
	 int   type() default -1  ;
	 
	 /**
	  *长度
	  */
	 int   length() default -1  ;
	 
	 /**
	  * 索引 参考  {@link xiaoa.java.sql.oodb.annotation.Indexs}
	  */
	 int  index() default 0;
	 
	 /**
	  * 备注
	  */
	 String comment() default "";
	 
	 /**
	  * 编码字符集
	  */
	 String charset() default "utf8";
	 
	 /**
	  * 默认值
	  */
	 String defaultValue() default "";
	

}
