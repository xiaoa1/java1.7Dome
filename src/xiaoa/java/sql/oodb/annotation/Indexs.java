package xiaoa.java.sql.oodb.annotation;

/**
 * 索引类型
 * @author xiaoa
 *
 */
public interface Indexs {
	
	/**
	 * 没有索引
	 */
	public static final int NO_INDEX = 0;
	
    /**
     * 普通索引 ，一般情况下用普通索引
     */
	public static final int NORMAL_INDEX = 1;
	
	/**
	 * 唯一索引
	 */
	public static final int UNIQUE_INDEX = 2;
	
	
	/**
	 * 主键key
	 */
	public static final int PRIMARY_KEY  = 3;
	
	
}
