package xiaoa.java.sql.oodb.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 表注解
 * @author xiaoa
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
public @interface Table {
	
	/**
	 * 备注
	 */
	String comment() default "";

	
	/**
	 * 主键key
	 */
	String primary() default "";
	
	/**
	 * 表名
	 */
	String tableName() default "";
	
}
