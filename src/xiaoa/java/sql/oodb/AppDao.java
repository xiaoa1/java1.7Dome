package xiaoa.java.sql.oodb;

import java.lang.reflect.Field;



import java.util.ArrayList;
import java.util.List;

import xiaoa.java.log.L;
import xiaoa.java.sql.oodb.interfaces.DaoInterfaces;
import xiaoa.java.sql.oodb.mgr.SessionMgr;
import xiaoa.java.sql.oodb.mgr.SqlSessionMgr;
import xiaoa.java.sql.oodb.utils.VoUtils;
import xiaoa.java.sql.oodb.vo.VData;

/**
 * appdao
 * @author xiaoa
 * @date 2017年1月12日 上午11:32:37
 * @version V1.0
 *
 * @param <T>
 */
public class AppDao<T> implements DaoInterfaces<T>  {
	
	// 表class
	public Class<?>  cla;
	
	// SQL语句
	private StringBuilder  sql    = new  StringBuilder();
	
	// 参数
	private List<Object> parmas  =  new ArrayList<Object>();
	
	// 构造方法
	public AppDao(Class<?> cla) throws Throwable{
		
		if(cla == null ){
			throw new RuntimeException("参数有误");
		}
		
		// 表class
		this.cla = cla;
		
	}

    /**
     * 添加操作
     * @param t
     * @throws Throwable
     */
	public int save(Object obj)throws Throwable{
		
		// 先删除该条记录
		Long  key = VoUtils.getPrimaryKeyValue(obj);
		delete(key);
		
		// 添加该条记录
		return insert(obj);
		
	}

	/**
	 * 添加一条记录
	 */
	@Override
	public int insert(Object obj) throws Throwable {
		
		if(obj == null ){
			throw new RuntimeException("参数有误！");
		}
		
		// 获取表属性
		List<Field>   columList  =   VoUtils.getFields(cla);
		
		if(columList == null || columList.size() == 0){
			L.info("该表没有任何可以添加字段");
			 return 0;
		}
		
		// 获取添加对象属性
		List<Field>   objColumList          =   VoUtils.getClassFields(obj.getClass());

		if(objColumList == null || objColumList.size() == 0){
			L.info("该对象没有任何可以添加字段");
			 return 0;
		}
		
		// 设置默认值
		if(obj instanceof VData){
			
			VData  data =  (VData)obj;
			
			if(data.timeKey == null || data.timeKey <= 0){
				data.timeKey = SqlSessionMgr.createKey();
			}
		}
		
		// 获取所有表名
		List<String>   columNameList       =   VoUtils.getColumn(columList);
		
		// 创建sql语句
		StringBuilder   sql                =   new StringBuilder();
		
		// value值sql
		StringBuilder   valueSql           =   new StringBuilder();
		
		//  value值list
		List<Object>    valueList          =   new ArrayList<Object>(columNameList.size());
		
		// 设置语句头
		sql.append(" insert into ").append(VoUtils.getTableName(cla)).append(" ( ");
		
		// 设置value
		valueSql.append(" (");
		
		// 是否需要拼接 ,
	    int  index  = 0 ;
		
		for(Field f : columList){
			
			String  fName  =   f.getName();
			
			if(index == 0){
				sql.append(fName);
				
				// 拼接value sql
				valueSql.append("?");
				
			}else{
				sql.append(",").append(fName);
				
				// 拼接value sql
				valueSql.append(",?");
				
			}
			
			// 获取属性值
		    Object    value =  VoUtils.getColumValue(f,obj);
			
			// 添加属性值
			valueList.add(value);

			index ++ ;
		}
		
		sql.append(" ) ");
		
		sql.append(" values ");
		
		valueSql.append(" )"); 
		
		// 执行修改数据库
		return SessionMgr.preparedExecuteUpdate(sql.append(valueSql).toString(), valueList, cla);
		
	}

	/**
	 * 批量插入一组数据
	 */
	@Override
	public int insert(List<Object> dataList) throws Throwable {
		
		if(dataList == null || dataList.size() == 0){
			return 0;
		}
		
		for(Object data :  dataList){
			insert(data);
		}
		return 0;
	}

	@Override
	public int save(List<Object> dataList) throws Throwable {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public boolean hasExists(Long timeKey) throws Throwable {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public List<T> queryPage(int offset, int rows) throws Throwable {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public T queryOne(int offset, int rows) throws Throwable {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<T> queryPageField(int offset, int rows) throws Throwable {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int count() throws Throwable {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Double sum() throws Throwable {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Double max() throws Throwable {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Double min() throws Throwable {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void seSql(StringBuilder sql) throws Throwable {
		
	}


	@Override
	public void setParams(List<Object> params) throws Throwable {
		// TODO Auto-generated method stub
		
	}
	

	/**
	 * 删除一条记录
	 * @param timeKey
	 * @throws Throwable
	 */
	@Override
	public void delete(Long timeKey) throws Throwable {
		
		// 拼接sql
		sql.append("delete from ");
		sql.append(VoUtils.getTableName(cla));
		sql.append("  where  ");
		sql.append(VoUtils.getPrimaryKeyName(cla));
		sql.append(" = ? ");
		
		// 添加参数
		parmas.add(timeKey);
		
		// 执行
		SessionMgr.preparedExecuteUpdate(sql.toString(), parmas, cla);
	}

	@Override
	public void delete() throws Throwable {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update(Object obj) throws Throwable {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update() throws Throwable {
		// TODO Auto-generated method stub
		
	}
	
	
	
	

}
