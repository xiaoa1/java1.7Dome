package xiaoa.java.sql;

import xiaoa.java.log.L;

import xiaoa.java.sql.oodb.AppDao;
import xiaoa.java.sql.oodb.mgr.SqlSessionMgr;
import xiaoa.java.sql.oodb.pool.SqlSessionPool;

/**
 * 测试类
 * @author xiaoa
 *
 */
public class Test {
	/**
	 * lkc
	 * @throws Throwable
	 */
	public static void initDB()throws Throwable{
		
		//  初始化链接池
		String  databaseTag  =  "test";
		String   url          = "jdbc:mysql://localhost:3306/javaSqlTest?useUnicode=true&characterEncoding=utf8&autoReconnect=true&rewriteBatchedStatements=TRUE";
		String   userName     = "root";
		String   userPassword = "root";
		SqlSessionPool.add(databaseTag, "com.mysql.jdbc.Driver", url, userName, userPassword);
		
		// 添加表
		SqlSessionPool.addTable(databaseTag, TestVo.class);
		
		// 初始化表
		SqlSessionPool.initTable();
		
	    L.info("========= 初始化数据库成功");
		
	}
	
	
	public static void main(String[] args)throws Throwable{
		initDB();
		
		// 开启一个事务
		SqlSessionMgr.beginTransaction();
		
		// 创建一个dao
		AppDao<TestVo>   dao  = new AppDao<TestVo>(TestVo.class);
		
		for (int i = 0; i < 10; i++) {
			TestVo  vo  = new TestVo();
			
			vo.testName  = "测试名字";
			
			dao.save(vo);
			
			L.info("=========  添加成功");
			
			if(i == 9){
				// 回滚事务
				SqlSessionMgr.rollBackTransaction();
			}
			
		}
		
		
		
	}
	

}
