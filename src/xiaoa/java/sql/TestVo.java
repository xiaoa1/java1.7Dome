package xiaoa.java.sql;

import java.sql.Types;

import xiaoa.java.sql.oodb.annotation.FieldType;
import xiaoa.java.sql.oodb.annotation.Table;
import xiaoa.java.sql.oodb.vo.VData;

@Table(comment = "测试表" )
public class TestVo  extends VData{

	private static final long serialVersionUID = 1L;

	@FieldType(type = Types.VARCHAR , comment = "名字")
	public String  testName;
}
