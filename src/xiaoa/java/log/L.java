package xiaoa.java.log;

import java.util.LinkedHashMap;

import org.apache.log4j.Logger;

import org.apache.log4j.PropertyConfigurator;

import com.alibaba.fastjson.JSON;

/**
 *  日志打印类
 * @author lkc
 *
 */
public class L {

	/**
	 * 创建一个logger
	 */
	public  final static Logger    l  = Logger.getLogger("java1.7Dome");
	
	static {
		
		// 加载配置文件
		PropertyConfigurator.configure("log4j.properties");
	}

	
	// 最后一次日志时间
	//private static long lastTime = 0;
	
	public static void info( long startTime , long endTime ,Object  os){
		info(os , "useTime : " + (endTime - startTime));
	}

	
	public static void showUsedTime(String fun,LinkedHashMap<String, Object>  logMap ,long startTime , long endTime ){
		info(fun + ":" ,JSON.toJSON(logMap) , "useTime : " + (endTime - startTime));
		
		
	}
	
	
	/**
	 * info
	 * @Title: info
	 * @param os
	 * @author xiaoa
	 */
	public static void info(Object...  os){
		
		// 距离上次时将
		
		// 获取线程名字
		String threadName = Thread.currentThread().getName();
		
		l.info( "threadName = " + threadName + ":" + merge(os));

	}
	
	
	/**
	 * debug
	 * @Title: debug
	 * @param os
	 * @author xiaoa
	 */
	public static void debug(Object...  os){
		l.debug(merge(os));
	}
	
	
	/**
	 * 将对象合并为字符串
	 * @Title: merge
	 * @param os
	 * @return
	 * @author xiaoa
	 */
	private static String merge(Object...  os){
		
		if(os == null ){
			return null;
		}
		
		
		StringBuilder  sb = new StringBuilder();
		
		for (Object o : os){
			
			if (o == null){
				continue;
			}
			
			if (o instanceof CharSequence){
				sb.append(o.toString());
			}else {
				sb.append(JSON.toJSONString(o));
			}
			
		}
		
		return sb.toString();
	} 
	
	
}
